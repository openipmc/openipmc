/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file fru_state_machine.c
 * 
 * @authors Alison Franca Queiroz da Costa
 * @authors Andre Muller Cascadan
 * 
 * @brief  Management task and functions for the operation of the Field Replaceable Unit (FRU) State Machine.
 *
 * @sa {@link fru_state_machine.h } 
 */

#include <stdbool.h>
#include <stdint.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#include "fru_state_machine.h"
#include "ipmc_ios.h"
#include "front_leds.h"
#include "ipmb_0.h"
#include "ipmi_msg_manager.h"
#include "sdr_manager.h"
#include "power_manager.h"
#include "fru_payload_ipmc_control.h"

// FSM change conditions queue.
QueueHandle_t queue_fru_transitions = NULL;


TaskHandle_t fru_state_machine_task_handle = NULL;

// Custom initialization routines (Inventory, SDRs, Power Profile...)
void ipmc_custom_initialization(void);


// Struct holding the state of the FRU state machine
static struct {
	fru_state_t previous_state;
	fru_state_t current_state;
	int deact_lock;
	int fru_lock;
	uint8_t cause_state_change;
} fsm_status = {M0, M0, 0, 0, CHANGE_CAUSE_NORMAL};

// Control queue timeout
#define LONG_TIMEOUT  5000
#define SHORT_TIMEOUT 500
#define NO_TIMEOUT    portMAX_DELAY
TickType_t timeout  = NO_TIMEOUT;
/*
 *  NOTE: LONG TIMEOUT is only used in M0 -> M1 (after boot), since Shelf Manager may be still booting as well.
 *        Shelf Manager takes many seconds to boot, so there is no reason to insist too much.
 */

#define SEND_OK   0
#define SEND_FAIL 1;

_Bool send_hot_swap_event_message = true;

// Static functions
static int  send_hot_swap_event_to_shmc(void);
static void evaluate_handle(void);


void fru_state_machine_task( void *pvParameters )
{

	fru_transition_t transition_event;
	
    // Wait for resources
    while ( ipmc_ios_ready() != pdTRUE )
      vTaskDelay( pdMS_TO_TICKS(100) );
	
    // Wait until ipmi_msg_router is ready
    while( ipmi_router_channel_is_ready(IPMI_ROUTER_CHANNEL_IPMB_0) != IPMI_MSG_ROUTER_RET_OK )
      vTaskDelay( pdMS_TO_TICKS(100));

    // Create the queue to receive the transition events
    queue_fru_transitions = xQueueCreate( 5, sizeof( fru_transition_t ) );
	
	// Run the custom initialization routines
	ipmc_custom_initialization();

	// Create the "Management Controller Device Locator Record", which identifies the IPMC
	// It uses custom Device ID information provided by the user in the previously called function "ipmc_custom_initialization()"
	create_management_controller_sdr();

	// change from M0 to M1 (Normal Insertion)
	transition_event = NORMAL_INSERTION;
	xQueueSendToBack(queue_fru_transitions, &transition_event, 0UL);

	// Check handle position. It activates the board in case closed.
	evaluate_handle();


	for( ;; )
	{

		/*
		 * This task just stays waiting for a transition event from the queue.
		 * When it arrives, task executes the transition processes and finally informs Shelf Manager
		 * about the new FRU state via "FRU hot swap event message"
		 * However, if Shelf Manager does not responds properly to the "FRU hot swap event message",
		 * a timeout is applied to repeat the message, which happens immediately after this "If"
		 */
		if( xQueueReceive(queue_fru_transitions, &transition_event, timeout) == pdTRUE )
		{

			switch(transition_event)
			{
				case NORMAL_INSERTION:
					switch(fsm_status.current_state)
					{
						case M0:
							fsm_status.current_state = M1;
							front_leds_set_blue_led_mode(BLUE_LED_ON);
							send_hot_swap_event_message = true;
							break;
						default:
							break;
					}
					break;

				case CLOSE_HANDLE:
					fsm_status.fru_lock = 0;

					switch(fsm_status.current_state)
					{
						case M1:
							fsm_status.previous_state = fsm_status.current_state;
							fsm_status.current_state = M2;
							fsm_status.cause_state_change = CHANGE_CAUSE_HANDLE_SWITCH;
							front_leds_set_blue_led_mode(BLUE_LED_LONG_BLINK);
							send_hot_swap_event_message = true;
							break;
						case M5:
							fsm_status.previous_state = fsm_status.current_state;
							fsm_status.current_state = M4;
							fsm_status.cause_state_change = CHANGE_CAUSE_HANDLE_SWITCH;
							front_leds_set_blue_led_mode(BLUE_LED_OFF);
							send_hot_swap_event_message = true;
							break;
						default:
							break;
					}
					break;
				
				case OPEN_HANDLE:
					fsm_status.deact_lock = 0;

					switch(fsm_status.current_state)
					{
						case M2:
							fsm_status.previous_state = fsm_status.current_state;
							fsm_status.current_state = M1;
							fsm_status.cause_state_change = CHANGE_CAUSE_HANDLE_SWITCH;
							front_leds_set_blue_led_mode(BLUE_LED_ON);
							send_hot_swap_event_message = true;
							break;
						case M3:
							fsm_status.previous_state = fsm_status.current_state;
							fsm_status.current_state = M6;
							fsm_status.cause_state_change = CHANGE_CAUSE_HANDLE_SWITCH;
							front_leds_set_blue_led_mode(BLUE_LED_SHORT_BLINK);
							send_hot_swap_event_message = true;
							break;
						case M4:
							fsm_status.previous_state = fsm_status.current_state;
							fsm_status.current_state = M5;
							fsm_status.cause_state_change = CHANGE_CAUSE_HANDLE_SWITCH;
							front_leds_set_blue_led_mode(BLUE_LED_SHORT_BLINK);
							send_hot_swap_event_message = true;
							break;
						default:
							break;
					}
					break;

				case SET_FRU_ACTIVATION:
					switch(fsm_status.current_state)
					{
						case M2:
							fsm_status.deact_lock = 1;
							fsm_status.previous_state = fsm_status.current_state;
							fsm_status.current_state = M3;
							fsm_status.cause_state_change = CHANGE_CAUSE_SET_FRU_ACTIVATION;
							front_leds_set_blue_led_mode(BLUE_LED_OFF);
							send_hot_swap_event_message = true;
							break;
						case M5:
							fsm_status.deact_lock = 1;
							fsm_status.previous_state = fsm_status.current_state;
							fsm_status.current_state = M4;
							fsm_status.cause_state_change = CHANGE_CAUSE_SET_FRU_ACTIVATION;
							front_leds_set_blue_led_mode(BLUE_LED_OFF);
							send_hot_swap_event_message = true;
							break;
						default:
							break;
					}
					break;

				case SET_FRU_DEACTIVATION:
					switch(fsm_status.current_state)
					{
						case M2:
							fsm_status.fru_lock = 1;
							fsm_status.previous_state = fsm_status.current_state;
							fsm_status.current_state = M1;
							fsm_status.cause_state_change = CHANGE_CAUSE_SET_FRU_ACTIVATION;
							front_leds_set_blue_led_mode(BLUE_LED_ON);
							send_hot_swap_event_message = true;
							break;
						case M3:
							fsm_status.fru_lock = 1;
							fsm_status.previous_state = fsm_status.current_state;
							fsm_status.current_state = M6;
							fsm_status.cause_state_change = CHANGE_CAUSE_PROGRAMMATIC_ACTION;
							front_leds_set_blue_led_mode(BLUE_LED_SHORT_BLINK);
							send_hot_swap_event_message = true;
							break;
						case M4:
							fsm_status.fru_lock = 1;
							fsm_status.previous_state = fsm_status.current_state;
							fsm_status.current_state = M6;
							fsm_status.cause_state_change = CHANGE_CAUSE_SET_FRU_ACTIVATION;
							front_leds_set_blue_led_mode(BLUE_LED_SHORT_BLINK);
							send_hot_swap_event_message = true;
							ipmc_pwr_start_payload_deactivation();
							break;
						case M5:
							fsm_status.fru_lock = 1;
							fsm_status.previous_state = fsm_status.current_state;
							fsm_status.current_state = M6;
							fsm_status.cause_state_change = CHANGE_CAUSE_SET_FRU_ACTIVATION;
							front_leds_set_blue_led_mode(BLUE_LED_SHORT_BLINK);
							send_hot_swap_event_message = true;
							ipmc_pwr_start_payload_deactivation();
							break;
						default:
							break;
					}
					break;

				case SET_POWER_LEVEL_ACTIVATION:
					switch(fsm_status.current_state)
					{
						case M3:
							ipmc_pwr_apply_authorized_power_level();
							fsm_status.previous_state = fsm_status.current_state;
							fsm_status.current_state = M4;
							fsm_status.cause_state_change = CHANGE_CAUSE_NORMAL;
							front_leds_set_blue_led_mode(BLUE_LED_OFF);
							send_hot_swap_event_message = true;
							break;
						case M4:
							// Just apply the Power Level. Stay in M4
							// This case happens when system change its Power Level among non-zero values.
							ipmc_pwr_apply_authorized_power_level();
							break;
						default:
							break;
					}
					break;

				case SET_POWER_LEVEL_DEACTIVATION:
					switch(fsm_status.current_state)
					{
						case M4:
							fsm_status.fru_lock = 1;
							fsm_status.previous_state = fsm_status.current_state;
							fsm_status.current_state = M6;
							fsm_status.cause_state_change = CHANGE_CAUSE_UNEXPECTED_DEACTIVATION;
							front_leds_set_blue_led_mode(BLUE_LED_SHORT_BLINK);
							send_hot_swap_event_message = true;
							ipmc_pwr_start_payload_deactivation();
							break;
						default:
							break;
					}
					break;

				case DEACTIVATE_PAYLOAD:
					switch(fsm_status.current_state)
					{
						case M6:
							ipmc_pwr_apply_deactivation();
							fsm_status.previous_state = fsm_status.current_state;
							fsm_status.current_state = M1;
							fsm_status.cause_state_change = CHANGE_CAUSE_NORMAL;
							front_leds_set_blue_led_mode(BLUE_LED_ON);
							send_hot_swap_event_message = true;
							break;
						default:
							break;
					}
					break;

				case DO_IPMC_COLD_RESET:
					impl_begin_ipmc_cold_reset();
					break;

				case DO_IPMC_WARM_RESET:
					impl_begin_ipmc_warm_reset();
					break;

				case DO_IPMC_MANUFACTURING_TEST:
					impl_start_ipmc_manufacturing_test();
					break;

				case DO_PAYLOAD_COLD_RESET:
					impl_begin_payload_cold_reset();
					break;

				case DO_PAYLOAD_WARM_RESET:
					impl_begin_payload_warm_reset();
					break;

				case DO_PAYLOAD_GRACEFUL_REBOOT:
					impl_begin_payload_graceful_reboot();
					break;

				case DO_PAYLOAD_DIAGNOSTIC_INT:
					impl_issue_payload_diagnostic_int();
					break;

				default:
					break;
			}
		}

		/*
		 * Send "FRU hot swap event message".
		 * This process repeats until Shelf Manager acknowledges properly (completion code 0x00)
		 */
		if( send_hot_swap_event_message == true )
		{
			if( send_hot_swap_event_to_shmc() == SEND_OK )
			{
				send_hot_swap_event_message = false;
				timeout = NO_TIMEOUT;
			}
			else
			{
				if( fsm_status.previous_state == M0 )
					timeout = LONG_TIMEOUT;
				else
					timeout = SHORT_TIMEOUT;
			}

		}
	}
}



/**
 * @brief Send Hot Swap Event
 * 
 * This function is responsible to send the hot swap event to Shelf Manager (SHMC) based on the
 * previous and current state of the FRU state machine and the cause of the state change.
 * 
 * This function is used in {@link fru_state_machine_task} to promote the state change.
 * 
 */
static int send_hot_swap_event_to_shmc(void)
{
	
	uint8_t cmd_body[7];
	cmd_body[0] = 0x04;
	cmd_body[1] = 0xf0;
	cmd_body[2] = 0x00;
	cmd_body[3] = 0x6f;
	cmd_body[4] = 0xA0 + fsm_status.current_state;
	cmd_body[5] = (fsm_status.cause_state_change << 4) | fsm_status.previous_state;
	cmd_body[6] = 0x00;
	uint8_t completion;
	uint8_t response[28];
	int response_len;
	int ret;
	
    ret = ipmi_msg_send_request(IPMI_ROUTER_CHANNEL_IPMB_0, // channel
                                4, // netfn
                                2, // event
                                cmd_body,
                                7, // data_req length
                                &completion,
                                response,
                                &response_len );

	if( ipmi_msg_debug_flag )
		ipmc_ios_printf("FRU from M%d to M%d\r\n", (int)fsm_status.previous_state, (int)fsm_status.current_state);

	if( (ret == IPMI_MSG_SEND_OK) && (completion == 0) )
		return SEND_OK;
	else
		return SEND_FAIL;
}



void fru_set_locked_bit(int value)
{
	if (value == 0)
		fsm_status.fru_lock = 0;
	else 
		fsm_status.fru_lock = 1;
	
	evaluate_handle();
}

void fru_set_deactivation_locked_bit(int value)
{
	if (value == 0)
		fsm_status.deact_lock = 0;
	else 
		fsm_status.deact_lock = 1;
	
	evaluate_handle();
}

int fru_get_locked_bit(void)
{
	return fsm_status.fru_lock;
}

int fru_get_deactivation_locked_bit(void)
{
	return fsm_status.deact_lock;
}


/**
 * @brief Evaluate Handle
 * 
 * This function is responsible to force a Handle event transition by checking its state and send it to {@link queue_fru_transitions}.
 * 
 */
static void evaluate_handle(void)
{
	
	fru_transition_t transition;
	
	if( ipmc_ios_read_handle() == 0 ) // If closed
	{
		if (fsm_status.fru_lock != 1)
		{
			transition = CLOSE_HANDLE;
			xQueueSendToBack(queue_fru_transitions, &transition, portMAX_DELAY);
		}
	}
	else
	{
		if (fsm_status.deact_lock != 1)
		{
			transition = OPEN_HANDLE;
			xQueueSendToBack(queue_fru_transitions, &transition, portMAX_DELAY);
		}
	}
		
}


sensor_reading_status_t fru_hot_swap_sensor_reading(sensor_reading_t* sensor_reading, sensor_thres_values_t* sensor_thres_values)
{
	// This sensor doesn't have associated threshold values so the sensor_thres_values parameter will be NULL.
	sensor_reading->raw_value = 0; // Not used

	sensor_reading->present_state = 1 << (int)fsm_status.current_state;

	return SENSOR_READING_OK;
}


void fru_enqueue_state_transition(fru_transition_t transition)
{
  // Wait for the queue to be instantiated by the inizialization task
  while ( queue_fru_transitions == NULL )
    vTaskDelay( pdMS_TO_TICKS(100) );

  xQueueSendToBack(queue_fru_transitions, &transition, portMAX_DELAY); // Send state change request to FSM
}



