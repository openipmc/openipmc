/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file sensors_manager.h
 *
 * @author Carlos Ruben Dell'Aquila
 *
 * @brief Management interface for System Event Log (SEL)
 *
 */

#ifndef SENSORS_MANAGER_H
#define SENSORS_MANAGER_H


/**
 * @brief Callback used by Get Sensor Reading Command (IPMI v1.5) to get sensor reading from cache.
 *
 * @param short unsigned int which is the record Id.
 * @param sensor_reading_t pointer with the sensor_reading value.
 *
 * This is a global sensor reading callback function.
 */
sensor_reading_status_t sensor_reading_callback(short unsigned int, sensor_reading_t *);


#endif // SENSORS_MANAGER_H
