/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_completion_code_defs.h
 *
 * @author Luigi Calligaris, Carlos Ruben Dell'Aquila
 *
 * @brief Definition of the various IPMI completion codes.
 *
 * Refer to IPMI specification v1.5
 *
 */


#ifndef IPMI_COMPLETION_CODE_DEFS_H
#define IPMI_COMPLETION_CODE_DEFS_H


//
// IPMI v1.5 specification, Table 5-2
//
// Generic completion codes
//

#define IPMI_COMPL_CODE_SUCCESS                                  (0x00)
#define IPMI_COMPL_CODE_BUSY                                     (0XC0)
#define IPMI_COMPL_CODE_INVALID_CMD                              (0XC1)
#define IPMI_COMPL_CODE_INVALID_CMD_FOR_THIS_LUN                 (0XC2)
#define IPMI_COMPL_CODE_TIMEOUT                                  (0XC3)
#define IPMI_COMPL_CODE_OUT_OF_SPACE                             (0XC4)
#define IPMI_COMPL_CODE_RESERV_CANCELED_OR_INVALID_RESERV_ID     (0XC5)
#define IPMI_COMPL_CODE_REQUEST_DATA_TRUNCATED                   (0XC6)
#define IPMI_COMPL_CODE_DATA_LENGTH_INVALID                      (0XC7)
#define IPMI_COMPL_CODE_DATA_FIELD_LENGTH_LIMIT_EXCEEDED         (0XC8)
#define IPMI_COMPL_CODE_PARAMETER_OUT_OF_RANGE                   (0XC9)
#define IPMI_COMPL_CODE_CANNOT_RETURN_NREQUESTED_DATA_BYTES      (0XCA)
#define IPMI_COMPL_CODE_REQUESTED_RECORD_DATA_SENSOR_NOT_PRESENT (0XCB)
#define IPMI_COMPL_CODE_INVALID_DATA_FIELD_IN_REQUEST            (0XCC)
#define IPMI_COMPL_CODE_ILLEGAL_CMD_FOR_THIS_SENSOR_RECORD       (0XCD)
#define IPMI_COMPL_CODE_CANNOT_RESPOND                           (0XCE)
#define IPMI_COMPL_CODE_CANNOT_EXEC_DUPLICATED_REQUEST           (0XCF)
#define IPMI_COMPL_CODE_CANNOT_RESPOND_DUE_TO_SDR_UPDATE         (0XD0)
#define IPMI_COMPL_CODE_CANNOT_RESPOND_DUE_TO_FW_UPDATE          (0XD1)
#define IPMI_COMPL_CODE_CANNOT_RESPOND_DUE_TO_CONTROLLER_INIT    (0XD2)
#define IPMI_COMPL_CODE_DESTINATION_UNAVAILABLE                  (0XD3)
#define IPMI_COMPL_CODE_CANNOT_EXEC_CMD_INSUFFICIENT_PRIVILEGES  (0XD4)
#define IPMI_COMPL_CODE_CANNOT_EXEC_CMD_NOT_SUPPORTED_IN_STATE   (0XD5)
#define IPMI_COMPL_CODE_UNSPECIFIED_ERROR                        (0XFF)


//
// IPMI v1.5 specification, Table 25-6
//
// Add SEL Entry completion codes
//

#define IPMI_COMPL_CODE_OP_NOT_SUPPORTED_FOR_THIS_RECORD_TYPE    (0X80)
#define IPMI_COMPL_CODE_CANNOT_EXEC_CMD_SEL_ERASE_IN_PROGRESS    (0X81)

//
// IPMI v2.0 specification, Table 22
//
// Set Session Privilege Level
//

#define IPMI_COMPL_CODE_PRIVILEGE_LEVEL_NOT_AVAILABLE            (0X80)
#define IPMI_COMPL_CODE_PRIVILEGE_LEVEL_EXCEEDS_LIMIT            (0X81)

//
// IPMI v2.0 specification, Table 22-24
//
// Close Session
//

#define IPMI_COMPL_CODE_INVALID_REQ_SESSION_ID                   (0X87)
#define IPMI_COMPL_CODE_INVALID_REQ_SESSION_HANDLE               (0X88)

//
// IPMI v2.0 specification, Table 24-
//
// Activate/Deactivate Payload
//

#define IPMI_COMPL_CODE_PAYLOAD_IN_ANOTHER_SESSION               (0x80)
#define IPMI_COMPL_CODE_PAYLOAD_TYPE_DISABLE                     (0x81)
#define IPMI_COMPL_CODE_PAYLOAD_ACTIVATION_LIMIT_REACHED         (0x82)
#define IPMI_COMPL_CODE_PAYLOAD_NO_ACTIVATION_WITH_ENCRYPTION    (0x83)
#define IPMI_COMPL_CODE_PAYLOAD_ACTIVATION_REQ_ENCRYPTION        (0X84)

#define IPMI_COMPL_CODE_PAYLOAD_IS_ALREADY_DEACTIVATE            (0X80)


#endif // IPMI_COMPLETION_CODE_DEFS_H
