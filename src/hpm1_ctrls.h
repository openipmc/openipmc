
#include <stdint.h>
#include <stdbool.h>

// Global capabilities flags to be used in 'hpm1_global_capabilities'
#define SELF_TEST                           0x01
#define AUTO_ROLLBACK                       0x02
#define MANUAL_ROLLBACK                     0x04
#define PAYLOAD_IS_AFFECTED_BY_UPGRADE      0x08
#define DEFERRED_UPGRADE                    0x10
#define IPMC_DEGRADED_DURING_UPGRADE        0x20
#define AUTO_ROLLBACK_OVERRIDE              0x40
#define UPGRADE_UNDESIRABLE                 0X80


// Return values for HPM1 callbacks
#define HPM1_CB_RETURN_OK              0
#define HPM1_CB_RETURN_CHECKSUM_ERROR  1
#define HPM1_CB_RETURN_ROLLBACK_ERROR  2
#define HPM1_CB_RETURN_COMPONENT_ERROR 3


typedef struct
{
  uint8_t upgrade_timeout;
  uint8_t selftest_timeout;
  uint8_t rollback_timeout;
  uint8_t inaccessibility_timeout;
} hpm1_timeouts_t;

/*
 * HPM.1 Storage parameter
 */
typedef struct
{
  int run_start_block;
  int backup_start_block;
  int temp_start_block;

} hpm1_component_storage_info_t;


typedef struct
{
	// 0: General Component Properties
	bool payload_cold_reset_is_required;
	bool deferred_activation_is_supported;
	bool firmware_comparison_is_supported;
	// NOTE:  "Preparation" support is always informed as "supported"
	bool rollback_is_supported;
	bool backup_cmd_is_required;

	// 1: Current version
	uint8_t current_firmware_revision_major;  ///< 7 bits (0 ~ 127)
	uint8_t current_firmware_revision_minor;  ///< BCD from 00 to 99. (Example: Ver. x.2.3 -> 0x23)
	uint8_t current_firmware_revision_aux[4]; ///< Any 4 bytes of data typically displayed in hex format

	// 2: Description string
	char description_string[12];               ///< e.g.: Name of the component. 11 characters maximum + 1 end-of-string (zero).

	// 3: Rollback firmware version (ignored if no backup is available)
	uint8_t rollback_firmware_revision_major;  ///< 7 bits (0 ~ 127)
	uint8_t rollback_firmware_revision_minor;  ///< BCD from 00 to 99. (Example: Ver. x.2.3 -> 0x23)
	uint8_t rollback_firmware_revision_aux[4]; ///< Any 4 bytes of data typically displayed in hex format

	// 4: Deferred firmware version (ignored if no pending activation is available)
	uint8_t deferred_firmware_revision_major;  ///< 7 bits (0 ~ 127)
	uint8_t deferred_firmware_revision_minor;  ///< BCD from 00 to 99. (Example: Ver. x.2.3 -> 0x23)
	uint8_t deferred_firmware_revision_aux[4]; ///< Any 4 bytes of data typically displayed in hex format

	// Other information about this component
	bool backup_is_available;
	bool deferred_is_pending_activation;

	// Storage parameters
	hpm1_component_storage_info_t storage_info;

	// Boot control parameter to be used by bootloader
	uint8_t boot_ctrl;

}hpm1_component_properties_t;


// Holds the global capabilities. See Global capabilities flags
extern uint8_t hpm1_global_capabilities;

extern hpm1_timeouts_t hpm1_timeouts;

extern hpm1_component_properties_t* hpm1_component_properties[8];


hpm1_component_properties_t* hpm1_add_component( uint8_t component_number );


// Hardware specific callbacks
void hpm1_cmd_initiate_backup_cb( uint8_t component_mask );
void hpm1_cmd_initiate_prepare_cb( uint8_t component_mask );
void hpm1_cmd_initiate_upload_for_upgrade_cb( uint8_t component_number );
void hpm1_cmd_initiate_upload_for_compare_cb( uint8_t component_number );
void hpm1_cmd_upload_cb( uint8_t component_number, uint8_t* block_data, uint8_t block_size  );
int  hpm1_cmd_upload_finish_cb( uint8_t component_number  );
void hpm1_cmd_activate_cb( uint8_t component_number );
int  hpm1_cmd_manual_rollback_cb ( void );
