/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file power_manager.h
 * 
 * @authors Andre Muller Cascadan
 * @authors Bruno Augusto Casu
 * 
 * @brief Power management interface.
 * 
 * This header file contains the definition of all the functions that are responsible for the power management of the Field Replaceable Unit (FRU).
 * Those functions are integrated with the IPMI command response functions and the state machine of the OpenIPMC,
 * supporting the power management and transitions in the Hot Swap functionality.
 * 
 * Power levels can also be set using the function described in this file.
 */

#ifndef IPMC_POWER_H
#define IPMC_POWER_H

#include <stdint.h>

/**
 * @{
 * @name FRU control capabilities
 */
// user implementation to inform which control capabilities are supported by the payload (all of them are optional)
#define DIAGNOSTIC_INTERRUPT_SUPPORTED  0x80
#define GRACEFUL_REBOOT_SUPPORTED       0x40
#define WARM_RESET_SUPPORTED            0x20
///@}

/**
 * Struct to hold the parameters for managing the power of the FRU, such as the number of power levels the system supports
 * and, for each level, the power draw to be multiplied by a factor.
 */
typedef struct
{
	uint8_t num_of_levels;
	uint8_t multiplier;
	uint8_t power_draw[21];
    
}power_envelope_t;

/**
 * @{
 * @name Power management functions
 */
/**
 * @brief Set the Power Levels suported by the payload.
 *
 * @param usr_envelope Struct defined by user which contains the number of power level supported by the FRU and for each level the power draw.
 *
 * This function is used to inform OpenIPMC about the power levels accepted by the payload (definition in PICMG v3.0). A list of up to 20 power levels can be declared.
 * 
 * @note This function must be called inside the implementation of {@link ipmc_custom_initialization()}
 */
void ipmc_pwr_setup_power_envelope(power_envelope_t usr_envelope);

/**
 * @brief Set which power level is preferred by the payload
 *
 * @param power_level Number of the power level to be set.
 * 
 * This function is used to inform IPMC which power level is the most suitable for the payload. This value is used during the power negotiation steps betwen IPMC and 
 * Shelf Manager.
 *  
 * @note This function must be called inside the implementation of {@link ipmc_custom_initialization()}
 */
void ipmc_pwr_set_desired_power_level(uint8_t power_level);

/**
 * @brief Get the power level which is currently assigned to the payload.
 * 
 * @return Current power level, within the defined range set in ipmc_pwr_setup_power_envelope().
 * Value 0 means deactivated.
 */
uint8_t ipc_pwr_get_current_power_level(void);

/**
 * @brief Callback function to apply a new power level on the payload.
 * 
 * @param new_power_level Number of the power level to be set in the payload.
 * 
 * This function is implemented by the user to manage the activation and deactivation of the power supply for the FRU payload. The management is based in the
 * power levels set in the ipmc_pwr_setup_power_envelope(), therefore the activation and power draw of the payload must respect the values set for each power level.
 * 
 * The switch power level function is called by the ipmc_pwr_apply_authorized_power_level() function, which responds to commands sent from Shelf Manager.
 * 
 * @note Shelf Manager is the entity that authorizes the deactivation, activation or switching between power levels for the FRU payload. Power levels should not be
 *       changed without authorization.
 */
void ipmc_pwr_switch_power_level_on_payload(uint8_t new_power_level);

/**
 * @brief Function to inform that a change in the power level is authorized.
 * 
 * @param power_level Number of the power level to be set.
 * 
 *  This function is integrated with the ipmi_cmd_set_power_level() response function. As the power negotiations are finished, Shelf Manager will send the
 *  "Set Power Level" command, with a specific power level value. This function is called with the power level authorized by Shelf Manager and sends the 
 *  event to the state machine queue, which will manage the transition between power levels.
 */
void ipmc_pwr_set_power_level_authorized_by_shmc(uint8_t power_level);

/**
 * @brief Function to retrieve information of the power levels for the FRU payload initialization process.
 * 
 * @param get_envelope Pointer to return the parameter in the envelop struct, defined in the ipmc_pwr_setup_power_envelope().
 * @param get_current_power_level Pointer to return the number of the current power level.
 * @param get_desired_power_level Pointer to return the desired power level.
 * 
 * This function is integrated with the ipmi_cmd_get_power_level() response function, and is used to return information to Shelf Manager regarding the initialization process of 
 * the payload, based in the user defined parameters for the power levels and power draw.
 */
void ipmc_pwr_get_power_properties(power_envelope_t* get_envelope, 
                                   uint8_t*          get_current_power_level, 
                                   uint8_t*          get_desired_power_level);

/**
 * @brief Function to effectively set the authorized power level.
 * 
 * The power apply function effectively sets the current power level with the authorized value. It is called by the OpenIPMC state machine, which manages the transition between power levels of the payload.
 * 
 * @note The function does not require any parameters because it uses the static variable {@link authorized_power_level}.
 */ 
void ipmc_pwr_apply_authorized_power_level(void);

/**
 * @brief Function to start the deactivation process of the payload.
 * 
 * This function is used to inform that a deactivation was requested, and the controller must start the deactivation process.
 * User should configure the ipmc_pwr_switch_power_level_on_payload() function so the deactivation is done properly as this function calls it 
 * with a desired power level zero (zero is standard level for power off, defined in PICMG v3.0).
 */
void ipmc_pwr_start_payload_deactivation(void);


/**
 * @brief This function is used to deactivate the payload (switch to power level zero).
 * 
 * The power apply deactivation function calls the user implemented function to transition the payload power to level zero (deactivated state).
 * The current power level is updated as the user function returns from the deactivation process.
 * 
 * @sa {@link ipmc_pwr_switch_power_level_on_payload()} for the user implemented power transition function.
 */
void ipmc_pwr_apply_deactivation(void);

///@}

/**
 * @{
 * @name Reset functions
 */
/**
 * @brief Function to evaluate which capabilities the FRU supports beyond the Cold Reset function
 * 
 * @return Return the mask with the supported capabilities.
 * 
 * The options are defined within the macros {@link DIAGNOSTIC_INTERRUPT_SUPPORTED}, {@link GRACEFUL_REBOOT_SUPPORTED} and {@link WARM_RESET_SUPPORTED}.
 * 
 * @note More info in PICMG v3.0 "FRU Control" command.   
 */
uint8_t get_fru_control_capabilities (void);


/**
 * @brief Function called in response of a "FRU Control" command issuing a Cold Reset of the Payload
 * 
 * The function informs the IPMC state machine that a COld Reset command was issued by Shelf Manager.
 */
void fru_control_issued_cold_reset (void);

/**
 * @brief Function called by the IPMC state machine to initialize the Cold Reset process.
 * 
 * User must implement the process for the payload Cold Reset.
 */
void payload_cold_reset (void);

///@}

#endif //IPMC_POWER_H
