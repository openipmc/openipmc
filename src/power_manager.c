/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file power_manager.c
 * 
 * @authors Andre Muller Cascadan
 * @authors Bruno Augusto Casu
 * 
 * @brief Power management interface.
 */

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "queue.h"

#include <stdint.h>
#include "fru_state_machine.h"
#include "power_manager.h"


/**
 * Struct used to hold power level and power draw information of the payload.
 */
static power_envelope_t power_envelope;

/**
 * Current power level of the payload (within the defined levels).
 */
static uint8_t current_power_level = 0;

/**
 * Desired power level to set the payload to (within the defined levels).
 */
static uint8_t desired_power_level = 0;

/**
 * Authorized power level sent by Shelf Manager.
 */
static uint8_t authorized_power_level;


void ipmc_pwr_setup_power_envelope(power_envelope_t usr_envelope)
{
	int i;
	
	power_envelope.num_of_levels = usr_envelope.num_of_levels;
	power_envelope.multiplier    = usr_envelope.multiplier;
	power_envelope.power_draw[0] = 0;
	
	for (i=1; i<=20; i++)
		power_envelope.power_draw[i] = usr_envelope.power_draw[i];
 
}



void ipmc_pwr_set_desired_power_level(uint8_t power_level)
{
	desired_power_level = power_level;
}


uint8_t ipc_pwr_get_current_power_level(void)
{
	return current_power_level;
}

// To be used on Get Power Level
void ipmc_pwr_get_power_properties(power_envelope_t* get_envelope, 
                                   uint8_t*          get_current_power_level,
                                   uint8_t*          get_desired_power_level)
{
	int i;
	
	get_envelope->num_of_levels = power_envelope.num_of_levels;
	get_envelope->multiplier    = power_envelope.multiplier;
	get_envelope->power_draw[0] = 0;
	
	for (i=1; i<=20; i++)
		get_envelope->power_draw[i] = power_envelope.power_draw[i];
	
	*get_current_power_level = current_power_level;
	*get_desired_power_level = desired_power_level;
}

void ipmc_pwr_set_power_level_authorized_by_shmc(uint8_t power_level)
{
	fru_transition_t fru_transition;
	
	
	
	// Power Manager uses the FRU State Machine to Execute the Power Level transition over the payload
	// This process can change the FRU state.
	
	if (power_level != 0)
	{
		// Transitions to non-zero Power Levels leads to state M4 (Activated)
		authorized_power_level = power_level;
		fru_transition = SET_POWER_LEVEL_ACTIVATION;
	}
	else
	{
		// When Power Level is requested to switch to zero, FRU moves to M6 (Deactivation in progress)
		authorized_power_level = 0;  // Invalidates non-zero authorization;
		fru_transition = SET_POWER_LEVEL_DEACTIVATION;
	}
	
	xQueueSendToBack(queue_fru_transitions, &fru_transition, portMAX_DELAY);
}



void ipmc_pwr_start_payload_deactivation(void)
{
	fru_transition_t fru_transition = DEACTIVATE_PAYLOAD;
	xQueueSendToBack(queue_fru_transitions, &fru_transition, portMAX_DELAY);
}

void ipmc_pwr_apply_authorized_power_level(void)
{
	if (current_power_level != authorized_power_level)
	{
		// The new Power Level must be switched on payload by this custom routine
		ipmc_pwr_switch_power_level_on_payload(authorized_power_level);
		
		current_power_level = authorized_power_level;
	}
	
}


void ipmc_pwr_apply_deactivation(void)
{
	// The new Power Level must be switched on payload by this custom routine
	ipmc_pwr_switch_power_level_on_payload(0);
	
	current_power_level = 0;
}

