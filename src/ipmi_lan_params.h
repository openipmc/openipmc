/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_lan_param.h
 *
 * @authors Carlos Ruben Dell'Aquila
 *
 * @brief Each Set and Get declaration for receives or responses a specific lan parameter payload according to 'Parameter Selector' of LAN Configuration Parameter command
 *
 * Response functions for Transport Network Function commands.
 *
 * This file contains the specific functions to manage the requests of Transport Network Function parameter selector (definition in PICMG v3.0 and IPMI v1.5).
 * Each function returns the response data bytes and the completion code.
 *
 * The specific functions are called by ipmi_cmd_set_lan_config_parameters() or ipmi_cmd_get_lan_config_parameters.
 */

#ifndef IPMI_LAN_PARAM_H
#define IPMI_LAN_PARAM_H

#include <stdint.h>
#include "ipmi_msg_router.h"


/**
 * @name Set LAN Parameter functions
 * @{
 */

/**
 * @brief Set IP Address
 *
 * @param uint8_t array with static IP address bytes.
 * @param int size amount of static IP address bytes.
 *
 * @return int '1' if static IP have been added successfully.
 *
 * Function to set the static IP address as a request of ipmi_cmd_set_lan_config_parameters() with parameter selector #3 (IP Address),
 * according to IPMI v1.5 spec, Table 19-4
 *
 */
extern int lan_param_set_ip_addr(uint8_t addr_bytes[], int size);


/**
 * @brief Set IP Address Source
 *
 * @param uint8_t pointer to bits [7:4] -reserved;  bits [3:0] - address source: 0x01 static address (manually configured), 0x02 address obtained by BMC running DHCP
 *
 * @return int '1' if the network configuration has been changed successfully.
 *
 * Function to set DHCP or Static Network configuration as a request of ipmi_cmd_set_lan_config_parameters() with parameter selector #4 (IP Address Source),
 * according to IPMI v1.5 spec, Table 19-4
 *
 */
extern int lan_param_set_ip_addr_source(uint8_t* byte_param);


/**
 * @brief Set MAC Address
 *
 * @param uint8_t array with the MAC Address bytes.
 * @param int size with amount of MAC Address bytes.
 *
 * @return int '1' if the MAC Address have been changed successfully.
 *
 * Function to set the MAC address as a request of ipmi_cmd_set_lan_config_parameters() with parameter selector #5 (MAC Address),
 * according to IPMI v1.5 spec, Table 19-4
 */
extern int lan_param_set_mac_addr(uint8_t addr_bytes[], int size);


/**
 * @brief Set Subnet Mask
 *
 * @param uint8_t array with static network Subnet ask bytes.
 * @param int size amount of static network Subnet Mask bytes.
 *
 * @return int '1' if static Subnet Mask have been added successfully.
 *
 * Function to set the static network Subnet Mask as a request of ipmi_cmd_set_lan_config_parameters() with parameter selector #6 (Subnet Mask),
 * according to IPMI v1.5 spec, Table 19-4
 *
 */
extern int lan_param_set_subnet_mask(uint8_t addr_bytes[], int size);


/**
 * @brief Set Default Gateway Address
 *
 * @param uint8_t array with static network gateway bytes.
 * @param int size amount of static gateway bytes.
 *
 * @return int '1' if static Gateway Address have been added successfully.
 *
 * Function to set the static Gateway Address as a request ipmi_cmd_set_lan_config_parameters() with parameter selector #12 (Default Gateway Address),
 * according to IPMI v1.5 spec, Table 19-4
 *
 */
extern int lan_param_set_default_gateway_addr(uint8_t addr_bytes[], int size);

///@}


/**
 * @name Get LAN Parameter functions
 * @{
 */

/**
 * @brief Get Set In Progress
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #0 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 *
 */
extern void lan_param_get_set_in_progress(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);


/**
 * @brief Get Authentication Type Support (Read Only)
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #1 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 *
 */
extern void lan_param_get_auth_type_support(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);

/**
 * @brief Get Authentication Type Enables
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #2 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 */
extern void lan_param_get_auth_type_enables(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);

/**
 * @brief Get IP Address
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #3 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 */
extern void lan_param_get_ip_addr(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );

/**
 * @brief Get IP Address Source
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #4 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 *
 */
extern void lan_param_get_ip_addr_source(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );



/**
 * @brief Get MAC Address
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #5 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 *
 */
extern void lan_param_get_mac_addr(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );


/**
 * @brief Get Subnet Mask
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #6 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 *
 */
extern void lan_param_get_subnet_mask(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code);



/**
 * @brief Get IPv4 Header Parameters
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #7 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 *
 */
extern void lan_param_get_ipv4_header_params(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );



/**
 * @brief Get BMC-generated ARP control (optional[2])
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #10 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 *
 */
extern void lan_param_get_bmc_generated_arp_control(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );


/**
 * @brief Get Gratuitous ARP interval (optional)
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #11 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 *
 */
extern void lan_param_get_gratuitous_arp_interval(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );



/**
 * @brief Get Default Gateway Address
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #12 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 *
 */
extern void lan_param_get_default_gateway_addr(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );


/**
 * @brief Get Default Gateway MAC Address
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void
 *
 * Retrieves bytes for the parameter selector #13 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 *
 */
extern void lan_param_get_default_gateway_mac_addr(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );


/**
 * @brief Get Backup Gateway Address
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #14 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 *
 */
extern void lan_param_get_bkp_gateway_addr(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );


/**
 * @brief Get Backup Gateway MAC Address
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void
 *
 * Retrieves bytes for the parameter selector #15 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 *
 */
extern void lan_param_get_bkp_gateway_mac_addr(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );


/**
 *  @brief Get Community String
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #16 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 *
 */
extern void lan_param_get_community_string(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );


/**
 * @brief Get 802.1q VLAN ID (12-bit)
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void  none
 *
 * Retrieves bytes for the parameter selector #20 corresponding to IPMI v2.0, Table 23-4, LAN Configuration.
 *
 */
extern void lan_param_get_802_1q_vlan_id(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );


/**
 * @brief Get 802.1q VLAN Priority
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void  none
 *
 * Retrieves bytes for the parameter selector #21 corresponding to IPMI v2.0, Table 23-4, LAN Configuration.
 *
 */
extern void lan_param_get_802_1q_vlan_priority(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );


/**
 * @brief Get RMCP+ Messaging Cipher Suite Entry Support (Read Only)
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #22 corresponding to IPMI v2.0, Table 23-4, LAN Configuration.
 *
 */
extern void lan_param_get_rmcp_plus_msh_cipher_suite_entry_support(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );


/**
 * @brief Get RMCP+ Messaging Cipher Suite Entries (Read Only)
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #23 corresponding to IPMI v1.5, Table 19-4, LAN Configuration.
 *
 */
extern void lan_param_get_rmcp_plus_msh_cipher_suite_entries(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );


/**
 * @brief GetRMCP+ Messaging Cipher Suite Privilege Levels
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #24 corresponding to IPMI v2.0, Table 23-4, LAN Configuration.
 *
 */
extern void lan_param_get_rmcp_plus_msh_cipher_suite_privilege_levels(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );


/**
 * @brief Get Bad Password Threshold (optional)
 *
 * @param ipmi_channel_enum_t channel that originates the request.
 * @param uint8_t array pointer to send back the response bytes.
 * @param int pointer to send back the response length.
 * @param uint8_t pointer to send back the completion code.
 *
 * return void none
 *
 * Retrieves bytes for the parameter selector #26 corresponding to IPMI v2.0, Table 23-4, LAN Configuration.
 *
 */
extern void lan_param_get_bad_password_threshold(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code );

///@}

#endif // IPMI_LAN_PARAM_H
