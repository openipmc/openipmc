/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/*
 *	@file	  rmcpp.h
 *	@brief  RMCP for IPMI over LAN client.
 *	@date	  YYYY / MM / DD - 2023 / 02 / 28
 *	@author	Antonio V. G. Bassi
 */

#ifndef RMCP_H
#define RMCP_H

/* Standard C */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/**
 * @name Macros to specified default RMCP parameters.
 * @{
 */
#define RMCP_VERSION              0x06 // Version asf 2.0
#define RMCP_DEFAULT_PORT         0x026F;
#define RMCP_RESERVED             0x00
#define RMCP_IANA_ENT_NUM         0x11BE
#define RMCP_SEQUENCE_NUMBER_IPMI 0xFF
///@}

/**
 * @name Macros to define the RMCP buffer length
 * @{
 */
#define RMCP_MAX_PLD_BUF_LENGTH           96U
#define RMCP_IPMI_SESSION_MAX_USRNAME_LEN 16U
#define RMCP_IPMI_SESSION_MAX_PWRD_LEN    16U
///@}

// Macros for ipmi session header data interp.

/**
 * @name Macros for IPMI SESSION header data interp.
 */
#define RMCP_IPMI_PLD_T_MSK              0x3F
#define RMCP_IPMI_PLD_T_MSG              0x00
#define RMCP_IPMI_PLD_T_SOL              0x01
#define RMCP_IPMI_PLD_T_OEM              0x02
#define RMCP_IPMI_PLD_T_OPEN_SESSION_RQ  0x10
#define RMCP_IPMI_PLD_T_OPEN_SESSION_RS  0x11
#define RMCP_IPMI_PLD_T_RAKP_MSG_1       0x12
#define RMCP_IPMI_PLD_T_RAKP_MSG_2       0x13
#define RMCP_IPMI_PLD_T_RAKP_MSG_3       0x14
#define RMCP_IPMI_PLD_T_RAKP_MSG_4       0x15
#define RMCP_IPMI_PLD_T_OEM_PLD_MSK      ( 0x20 | 0x27 )
///@}

/*
 * Enumeration with IP (Internet Protocol) version used in RMCP.
 *
 * It is used to describe the  RMCP remote agent networks parameters.
 *
 */
typedef enum
{
  IP_V4,
  IP_V6
} rmcp_ip_ver_t;

/*
 * Union with ipv4 and ipv6 data format
 *
 * It is used to save the RMPC remote agent IP address
 */
typedef union
{
  uint32_t ipv4;
  uint32_t ipv6[4];
} rmcp_ip_addr_t;


/*
 * Type struct for all network parameters.
 *
 * It is a type to centralize all network parameters of the RMCP remote agent.
 */
typedef struct
{
  rmcp_ip_ver_t ip_ver;
  rmcp_ip_addr_t ip_addr;
  uint16_t port;
} rmcp_ip_net_params_t;


/*
 * Enumeration to describe a notification event.
 */
typedef enum
{
  WATCHDOG_CANNOT_START,
  RMCP_PLUS_SESSION_TERMINATED_CLIENT_UNRESPONSIVE
} rmcp_event_report_t;



/*
 * Enumeration to notify when a callback is defined as NULL pointer.
 */
typedef enum
{
  RMCP_CALLBACK_ERROR_OK,
  RMCP_CALLBACK_ERROR_BLOCKING_RAND_GEN,
  RMCP_CALLBACK_ERROR_GET_GUID,
  RMCP_CALLBACK_ERROR_MALLOC,
  RMCP_CALLBACK_ERROR_MEM_FREE,
  RMCP_CALLBACKS_ERROR_SEND_UDP,
  RMCP_CALLBACK_ERROR_EVENT_REPORT,
  RMCP_QUEUE_OP_ERROR,
  RMCP_QUEUE_WATCHDOG_ERROR
} rmcp_init_ret_codet_t;

/* Callback function type definitions */
typedef void* (*malloc_t)(size_t size);
typedef void  (*mem_free_t)(void* mem);
typedef int   (*blocking_rand_gen_t)(uint8_t *bufp, uint16_t bufSize, uint32_t msTimeout);
typedef void  (*get_guid_t)(uint8_t** buf, uint16_t* len);
typedef int   (*rmcp_udp_send_packet_t)(rmcp_ip_net_params_t net_params, uint8_t *payload, size_t length);
typedef void  (*rmcp_event_send_t)(rmcp_event_report_t event);

/* Group of callbacks type definition */
typedef struct
{
  malloc_t malloc;
  mem_free_t mem_free;
  blocking_rand_gen_t blocking_rand_gen;
  get_guid_t get_guid;
  rmcp_udp_send_packet_t rmcp_udp_send_packet;
  rmcp_event_send_t rmcp_event_send;
}rmcp_callbacks_t;


/* RMCP types definitions */

/*
 * RMCP Lanplus (RMCP+ v2.0) packet definition
 *
 */
typedef struct
{
  // RMCP Header
  uint8_t version;
  uint8_t rsvd;
  uint8_t rmcp_sequence;
  uint8_t msg_class;

  // IPMI Header
  uint8_t auth_t;
  uint8_t payload_t;
  uint8_t session_id[4U];
  uint8_t ipmi_sequence[4U];
  uint8_t payload_length[2U];

  uint8_t payload_buffer[RMCP_MAX_PLD_BUF_LENGTH];
}rmcp_lanplus_t;


/*
 * RMCP OEM Lanplus packet definition
 *
 */
typedef struct
{
  // RMCP Header
  uint8_t version;
  uint8_t rsvd;
  uint8_t rmcp_sequence;
  uint8_t msg_class;

  // IPMI Header
  uint8_t auth_t;
  uint8_t payload_t;
  uint8_t oem_iana[4U];
  uint8_t oem_payload_id[2U];
  uint8_t session_id[4U];
  uint8_t ipmi_sequence[4U];
  uint8_t payload_length[2U];

  uint8_t payload_buffer[RMCP_MAX_PLD_BUF_LENGTH];
}rmcp_oem_lanplus_t;


/*
 * RMCP Lan (RMCP spec. v1.5) packet definition
 *
 */
typedef struct
{
  // RMCP header
  uint8_t version;
  uint8_t rsvd;
  uint8_t rmcp_sequence;
  uint8_t msg_class;

  // IPMI Header
  uint8_t auth_t;
  uint8_t ipmi_sequence[4U];
  uint8_t session_id[4U];
  uint8_t payload_length;

  uint8_t payload_buffer[RMCP_MAX_PLD_BUF_LENGTH];
}rmcp_lan_t;

typedef struct
{
  // RMCP header
  uint8_t version;
  uint8_t rsvd;
  uint8_t rmcp_sequence;
  uint8_t msg_class;

  // IPMI Header
  uint8_t auth_t;
  uint8_t ipmi_sequence[4U];
  uint8_t session_id[4U];
  uint8_t msg_auth_code[16U];
  uint8_t payload_length;

  uint8_t payload_buffer[RMCP_MAX_PLD_BUF_LENGTH];
}rmcp_lan_auth_t;


/*
 * Union to describe a general RMCP packet.
 */
typedef union
{
  rmcp_lan_t          lan;
  rmcp_lan_auth_t     lan_w_auth;
  rmcp_lanplus_t      lanplus;
  rmcp_oem_lanplus_t  oem_lanplus;
}rmcp_t;


/*
 * Enumeration with privilege levels form RMCP session.
 */
typedef enum
{
  RMCP_IPMI_PRIV_MAX_RQST = 0x00,
  RMCP_IPMI_PRIV_CALLBACK = 0x01,
  RMCP_IPMI_PRIV_USER_LVL = 0x02,
  RMCP_IPMI_PRIV_OPERATOR = 0x03,
  RMCP_IPMI_PRIV_ADMN_LVL = 0x04,
  RMCP_IPMI_PRIV_OEM_LVL  = 0x05,
  RMCP_IPMI_PRIV_TOT_LVLS = 0x06
}rmcp_ipmi_priv_t;


/*
 * Enumeration with RMCP channel status
 */
typedef enum
{
  RMCP_IPMI_CH_FREE     = 0,
  RMCP_IPMI_CH_BUSY     = 1,
  RMCP_IPMI_CH_LOCKED   = 2,
  RMCP_IPMI_CH_INVAL    = 3,
}rmcp_ipmi_ch_status_t;


/*
 * Enumeration with RMCP software status
 */
typedef enum rmcp_status
{
  RMCP_STATUS_OK                    = 0x00,
  RMCP_STATUS_NORSRCS               = 0x01,
  RMCP_STATUS_INVID                 = 0x02,
  RMCP_STATUS_INVPLD                = 0x03,
  RMCP_STATUS_INVAUTH               = 0x04,
  RMCP_STATUS_INVINTGR              = 0x05,
  RMCP_STATUS_NOAUTHPLD             = 0x06,
  RMCP_STATUS_NOINTGRPLD            = 0x07,
  RMCP_STATUS_INCTVID               = 0x08,
  RMCP_STATUS_INVROLE               = 0x09,
  RMCP_STATUS_UNAUTHROLE            = 0x0A,
  RMCP_STATUS_NOROLERSRCS           = 0x0B,
  RMCP_STATUS_INVNAMELEN            = 0x0C,
  RMCP_STATUS_UNAUTHNAME            = 0x0D,
  RMCP_STATUS_UNAUTHGUID            = 0x0E,
  RMCP_STATUS_INVINTGRVAL           = 0x0F,
  RMCP_STATUS_INVCONFALGO           = 0x10,
  RMCP_STATUS_NOCIPHER              = 0x11,
  RMCP_STATUS_INVPARAM              = 0x12,
  RMCP_STATUS_NUMOFCODES            = 0x13,
  RMCP_STATUS_ERROR_IN_RAND_NUM_GEN = 0x14
}rmcp_status_code_t;

/* ipmi session management data */


/*
 * Type to describe an user in RMCP session
 */
typedef struct usr_entry
{
  rmcp_ipmi_priv_t user_privilege_code;
  uint8_t          username[RMCP_IPMI_SESSION_MAX_USRNAME_LEN + 1];
  uint8_t          password[RMCP_IPMI_SESSION_MAX_PWRD_LEN + 1];
  struct usr_entry *next;
}rmcp_ipmi_user_entry_t;


/*
 * Type to describe a session_handle
 */
typedef struct ipmi_session_handle_t
{
  rmcp_ipmi_user_entry_t  *user;              // User that owns the IPMI session.
  uint16_t                last_pkt_size;      // Size of the last received packet.
  uint8_t                 *last_pkt;          // Last received packet.
  rmcp_ipmi_priv_t        privilege;          // Session privilege code.
  uint8_t                 payload_inst_mask;  // Payload Instance Mask (!).
  uint32_t                remote_console_id;  // Remote console session id, generated in an open session request command.
  uint32_t                id;                 // IPMI session id, generated by us in an open session response message.
  uint32_t                last_seq_num;       // Last received sequence number.
  rmcp_ip_net_params_t    client_net_params;  // RMCP client networks parameters (IP address and port)

}rmcp_ipmi_session_handle_t;

/*
 *  (!) Payload Instance Mask values
 *
 *  +---+---+---+---+---+---+---+---+
 *  | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 |
 *  +---+---+---+---+---+---+---+---+
 *  0 - Session is active
 *  1 - Serial Over LAN (SOL)
 *  2 - Can be allocated.
 *  3 - Can be allocated.
 *  4 - Can be allocated.
 *  5 - Can be allocated.
 *  6 - Can be allocated.
 *  7 - Can be allocated.
 */

extern const size_t ipmb_max_size;
extern const size_t rmcp_header_size;
extern const size_t rmcp_ipmi_auth_header_size;
extern const size_t rmcp_ipmi_unauth_header_size;
extern const size_t rmcp_lanplus_ipmi_oem_header_size;
extern const size_t rmcp_lanplus_ipmi_header_size;


typedef enum
{
  RMCP_NO_SESSION,
  RMCP_OPENING_SESSION,
  RMCP_ERROR_RAND_NUM_FOR_SESSION,
  RMCP_RAKP_2,
  RMCP_RAKP_4,
  RMCP_ONGOING_SESSION
} rmcp_session_status_t;


/*
 * Type to describe the channel properties.
 */
typedef struct
{
  uint32_t auth_cap;
  rmcp_ipmi_priv_t privilege_level;
} rmcp_channel_config_t;


/****** Functions to interact with OpenIPMC-SW ******/

/**
 * @brief RMCP Set Session Invalid
 *
 * @param int invalid flag.
 * @return void, none.
 *
 * It is used to notify to RMCP software the result of IPMI Payload 'Close Session' command from external agent.
 */
extern void rmcp_set_session_invalid(int invalid);

/**
 * @brief RMCP Get Channel Authentication Capabilities
 *
 * @param void, none.
 * @return uint32_t authentication capabilities.
 *
 * 32-bit field for channel authentication capabilities
 *
 * 0      ...      7|8         ...        15|16        ...        23|24       ...      31|
 * +----------------+-----------------------+-----------------------+--------------------+
 * | Channel number | Auth. Type Support[1] | Auth. Type Support[2] | Extd. Capabilities |
 * +----------------+-----------------------+-----------------------+--------------------+
 *
 * The function to get the authentication capabilities and is used for the IPMI Payload
 * "Get Channel Authentication Capabilities" command.
 */
extern uint32_t rmcp_get_channel_auth_cap(void);

/**
 * @brief IPMI RMCP Session Handle Set
 *
 * @param rmcp_ipmi_session_handle_t double pointer. It is used to get the address to the pointer that hold the session memory address.
 * @return void, none.
 *
 * The function is used to update the ipmi session data by several IPMI commands from an external agent.
 *
 */
extern void ipmi_rmcp_session_handle_set(rmcp_ipmi_session_handle_t **ipmi_session );

/**
 * @brief RMCP Get Channel Privilege
 *
 * @param void
 * @return rmcp_ipmi_priv_t with the privilege level for the RMCP channel.
 *
 * It is used for the IPMI Payload "Set Session Privilege Level" command.
 */
extern rmcp_ipmi_priv_t rmcp_get_channel_privilege(void);

/**
 * @brief RMCP Set Session status according to the process used to start a new session
 *
 * @param rmcp_session_status_t to set the status mode.
 * @return void
 *
 */
extern void rmcp_set_session_status(rmcp_session_status_t status);

/**
 * @brief RMCP Get Session status
 *
 * @param rmcp_session_status_t pointer to send back the actual status.
 */
extern void rmcp_get_session_status(rmcp_session_status_t *status);

/**
 * @brief RMCP Callbacks Get to be used for IPMI Payload "Activate Payload" command.
 *
 * @param rmcp_callbacks_t double pointer to the variable that holds the address of the callbacks in
 *        specific implementation side.
 * @return void.
 *
 * The function is used during the "Activate Payload" command that start the SOL communication. The last needs the function
 * form implementation specific to send UDP packets.
 */
void rmcp_callbacks_get(rmcp_callbacks_t** callbacks);

/**
 * @brief RMCP Get Local Network params
 *
 * @param  void
 * @return rmcp_ip_net_params_t with hardware network param.
 *
 * It is used for the IPMI Payload "Activate Payload" command.
 */
extern rmcp_ip_net_params_t rmcp_get_local_net_params(void);


/****** Functions to interact with RMCP implementation ******/

/**
 * @brief RMCP CALLBACKS SET for the specific implementation
 *
 * @param rmcp_callbacks_t pointer to a variable with the group
 * @rmcp_init_ret_codet_t
 *
 * The function is used to set the callbacks for the system specific (UPD Stack, memory management and hardware)
 * and also the queues use for RMCP Task.
 */
extern rmcp_init_ret_codet_t rmcp_init(rmcp_callbacks_t* callbacks);

/**
 * @brief RMCP Set Local Net params from the implementation to RMCP software
 *
 * @param  rmcp_ip_net_params_t variable with the network parameters (IP version, IP Address and port)
 * @return void
 *
 * It is used to send from the system implementation to the RMCP software. The the network information is
 * used to
 */
extern void rmcp_set_local_net_params(rmcp_ip_net_params_t net_params);


/**
 * @brief RMCP UDP Receiver
 *
 * @param rmcp_ip_net_params_t network params
 * @param uint8_t pointer to received data
 * @param uint16_t length of the received data
 * @return void.
 *
 * The received UDP packet is sent to RMCP software using this function.
 */
extern void rmcp_udp_recv(rmcp_ip_net_params_t net_params, uint8_t* payload, uint16_t length);

/**
 * @brief Get a copy a the RMCP session to be used for other module like CLI
 *
 * @param rmcp_ipmi_session_handle_t
 *
 * The function return the actual session status to be used for other firmware module.
 */
extern int rmcp_get_a_copy_session_status(rmcp_ipmi_session_handle_t *h);

/**
 * @FreeRTOS task to perform RMCP operation
 *
 * @param void pointer to get the FreeRTOS task operation.
 * @return void
 *
 * The task is used to decouples
 *
 */
extern void rmcp_op_task(void *pvParameters);

#endif // RMCP_H
