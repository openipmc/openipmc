/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

#ifndef OPENIPMC_IPMC_SELF_TEST_RESULT_H__
#define OPENIPMC_IPMC_SELF_TEST_RESULT_H__

void ipmc_self_test_set_results(uint8_t* completion, uint8_t* byte2, uint8_t* byte3);
void ipmc_self_test_get_results(uint8_t* completion, uint8_t* byte2, uint8_t* byte3);

#endif /* OPENIPMC_IPMC_SELF_TEST_RESULT_H__ */
