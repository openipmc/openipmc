/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */

/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 *
 * @file picmg_address_info.h
 *
 * @authors Carlos Ruben Dell'Aquila
 *
 * @brief  Management interfaces to get FRU and Shelf-Manager (ShM) address from ShM device.
 *
 */

#ifndef PICMG_ADDRESS_INFO_H_
#define PICMG_ADDRESS_INFO_H_

#include <stdio.h>

/** Error codes for GET_INFO_ADDRESS command */
enum {
	/** No error, everything OK. */
	PICMG_ADDRESS_INFO_ERR_OK = 0,
	/** Timeout while waiting IPMI data */
	PICMG_ADDRESS_INFO_ERR_IPMI_TIMEOUT = -1,
	/** Error in IPMI communication */
	PICMG_ADDRESS_INFO_ERR_IPMI_ERROR = -2
};

/** Error code for GET_SHELF_ADDRESS command */
enum {
	/** No error, everything OK. */
	PICMG_GET_SHELF_ADDRESS_ERR_OK = 0,
	/** Timeout while waiting IPMI data */
	PICMG_GET_SHELF_ADDRESS_ERR_IPMI_TIMEOUT = -1,
	/** Error in IPMI communication */
	PICMG_GET_SHELF_ADDRESS_ERR_IPMI_ERROR = -2
};



/** Type definitions */
typedef struct {
	uint8_t hardware_address;
	uint8_t ipmb_0_address;
	uint8_t fru_device_id;
	uint8_t site_number;
	uint8_t site_type;
} picmg_address_info_data_t;

typedef struct {
	uint8_t type_code;
	uint8_t len;
	uint8_t data[22];
} picmg_shelf_address_info_data_t;



/**
 * @brief Function to get FRU's address information from Shelf-Manager.
 *
 * @param picmg_address_info_data_t pointer where the retrieve data command will be saved.
 *
 * @return Return the status of the command ({@link PICMG_ADDRESS_INFO_ERR_OK }, {@link PICMG_ADDRESS_INFO_ERR_IPMI_TIMEOUT }, {@link PICMG_ADDRESS_INFO_ERR_IPMI_ERROR})
 *
 * This function is based on PICMG 3.0 R3 AdvancedTCA Base Specification and execute a command to get information about FRU address. There are different types address that are include in retrieved data.
 *
 * @note This function send IPMI message to Shelf-Manager by calling the ipmi_msg_send_request_ipmb0 function from openipmc library.
 */
int picmg_get_address_info( picmg_address_info_data_t * );

/**
 * @brief Function to get Shelf address information from Shelf-Manager.
 *
 * @param picmg_shelf_address_info_data_t pointer where the retrieve data command will be saved.
 *
 * @return Return the status of the command ({@link PICMG_GET_SHELF_ADDRESS_ERR_OK }, {@link PICMG_GET_SHELF_ADDRESS_ERR_IPMI_TIMEOUT}, {@link PICMG_GET_SHELF_ADDRESS_ERR_IPMI_ERROR}).
 *
 * This function is based on PICMG 3.0 R3 AdvancedTCA Base Specification and execute a command to get information about Shelf address.
 *
 * @note This function send IPMI message to Shelf-Manager by calling the ipmi_msg_send_request_ipmb0 function from openipmc library.
 *
 */
int picmg_get_shelf_address_info (picmg_shelf_address_info_data_t * data);

#endif // PICMG_ADDRESS_INFO_H_
