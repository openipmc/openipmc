/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_lan_param.c
 *
 * @authors Carlos Ruben Dell'Aquila
 *
 * @brief Each Set and Get functions for receives or responses a specific lan parameter payload according to 'Parameter Selector' of LAN Configuration Parameter command
 *
 * Response functions for Transport Network Function commands.
 *
 * This file contains the specific functions to manage the requests of Transport Network Function parameter selector (definition in PICMG v3.0 and IPMI v1.5).
 * Each function returns the response data bytes and the completion code.
 *
 * The specific functions are called by ipmi_cmd_set_lan_config_parameters() or ipmi_cmd_get_lan_config_parameters.
 */

#include "ipmi_lan_params.h"
#include "ipmi_completion_code_defs.h"
#include "ipmi_msg_router.h"
#include "ipmc_ios.h"


/**
 * Set IP Address
 */
int lan_param_set_ip_addr(uint8_t addr_bytes[], int size)
{
  return ipmc_ios_set_ip_addr(addr_bytes,size);
}

/**
 * Set IP Address Source
 */
int lan_param_set_ip_addr_source(uint8_t* byte_param)
{
  const uint8_t source = byte_param[0] & 0x0F;
  int ret;

  switch(source)
  {
    case 0x01: // static address (manually configured)
      ret = ipmc_ios_set_ip_source_static();
      break;
    case 0x02: // address obtained by OpenIPMC running DHCP
      ret = ipmc_ios_set_ip_source_dhcp();
      break;
    default:
      ret = -1;
     break;
    }

  return ret;
}

/**
 * Set MAC Address
 */
int lan_param_set_mac_addr(uint8_t addr_bytes[], int size)
{
  return ipmc_ios_set_mac_addr(addr_bytes,size);
}


/**
 * Set Subnet Mask
 */
int lan_param_set_subnet_mask(uint8_t addr_bytes[], int size)
{
  return ipmc_ios_set_netmask(addr_bytes,size);
}


/**
 * Set Default Gateway Address
 */
int lan_param_set_default_gateway_addr(uint8_t addr_bytes[], int size)
{
  return ipmc_ios_set_gatw_addr(addr_bytes,size);
}


/**
 * Get Set In Progress
 */
void lan_param_get_set_in_progress(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
  response_bytes[0] = 0x00; // Set completed.

  *res_len += 1;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}


/**
 * Get Authentication Type Support (Read Only)
 */
void lan_param_get_auth_type_support(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
  response_bytes[0] = 0x01; // none authentication type enabled for this channel (bitfield)

  *res_len += 1;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}


/**
 * Get Authentication Type Enables
 */
void lan_param_get_auth_type_enables(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
  response_bytes[0] = 0x01; // Authentication type returned for maximum requested privilege = Callback level.
  response_bytes[1] = 0x01; // Authentication type returned for maximum requested privilege = User level.
  response_bytes[2] = 0x01; // Authentication type returned for maximum requested privilege = Operator level.
  response_bytes[3] = 0x01; // Authentication type returned for maximum requested privilege = Administrator level.
  response_bytes[4] = 0x01; // Authentication type returned for maximum requested privilege = OEM level.

  *res_len += 5;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}


/**
 * Get IP Address
 */
void lan_param_get_ip_addr(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  const uint8_t req_size = 4;
  uint8_t* res_ip;
  uint8_t res_ip_size;

  ipmc_ios_get_ip_addr(res_ip,&res_ip_size,req_size);

  if ( req_size == res_ip_size )
  {
    response_bytes[0] = res_ip[0];
    response_bytes[1] = res_ip[1];
    response_bytes[2] = res_ip[2];
    response_bytes[3] = res_ip[3];

    *res_len += 4;
    *completion_code = IPMI_COMPL_CODE_SUCCESS;
  }
  else
  {
    *res_len += 0;
    *completion_code = IPMI_COMPL_CODE_UNSPECIFIED_ERROR;
  }
  return;
}

/**
 * Get IP Address Source
 */
void lan_param_get_ip_addr_source(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  const int dhcp_status = ipmi_ios_dhcp_is_enable();

  if( 1 == dhcp_status )
  { // dhcp enable
    response_bytes[0] = 0x02;
  }
  else
  { // static address (manually configured)
    response_bytes[0] = 0x01;
  }
  *res_len += 1;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}


/**
 * Get MAC Address
 */
void lan_param_get_mac_addr(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  const uint8_t req_size = 6;
  uint8_t res_mac_size;

  ipmc_ios_get_mac_address(response_bytes,&res_mac_size,req_size);

  if ( req_size == res_mac_size )
  {
    *res_len += 6;
    *completion_code = IPMI_COMPL_CODE_SUCCESS;
  }
  else
  {
    *res_len += 0;
    *completion_code = IPMI_COMPL_CODE_UNSPECIFIED_ERROR;
   }
  return;
}

/**
 * Get Subnet Mask
 */
void lan_param_get_subnet_mask(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
  const uint8_t req_size = 4;
  uint8_t res_netmask_size;

  ipmc_ios_get_netmask(response_bytes,&res_netmask_size,req_size);

  if ( req_size == res_netmask_size )
  {
    *res_len += 4;
    *completion_code = IPMI_COMPL_CODE_SUCCESS;
  }
  else
  {
    *res_len += 0;
    *completion_code = IPMI_COMPL_CODE_UNSPECIFIED_ERROR;
  }
  return;
}


/**
 * Get IPv4 Header Parameters
 */
void lan_param_get_ipv4_header_params(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  // all defaults values are used.
  response_bytes[0] = 0x40; // Time-to-live
  response_bytes[1] = 0x40; // Flags.
  response_bytes[2] = 0x10; // minimize dalay

  *res_len += 3;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}


/**
 * Get BMC-generated ARP control (optional[2])
 */
void lan_param_get_bmc_generated_arp_control(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  response_bytes[0] = 0x00; // all BMC-generated ARP responses and Gratuitous ARPs are disabled.

  *res_len += 1;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}

/**
 * Get Gratuitous ARP interval (optional)
 */
void lan_param_get_gratuitous_arp_interval(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  response_bytes[0] = 0x04; // Gratuitous ARP interval in 500 millisecond increments. 0-based. Default values is 2 sec.

  *res_len += 1;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}

/**
 * Get Default Gateway Address
 */
void lan_param_get_default_gateway_addr(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  const uint8_t req_size = 4;
  uint8_t res_gatw_size;

  ipmc_ios_gatw_addr(response_bytes,&res_gatw_size,req_size);

  if ( req_size == res_gatw_size )
  {
    *res_len += 4;
    *completion_code = IPMI_COMPL_CODE_SUCCESS;
  }
  else
  {
    *res_len += 0;
    *completion_code = IPMI_COMPL_CODE_UNSPECIFIED_ERROR;
  }
  return;
}

/**
 * Get Default Gateway MAC Address
 */
void lan_param_get_default_gateway_mac_addr(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  response_bytes[0] = 0x00;
  response_bytes[1] = 0x00;
  response_bytes[2] = 0x00;
  response_bytes[3] = 0x00;
  response_bytes[4] = 0x00;
  response_bytes[5] = 0x00;

  *res_len += 6;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}

/**
 * Get Backup Gateway Address
 */
void lan_param_get_bkp_gateway_addr(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  response_bytes[0] = 0x00;
  response_bytes[1] = 0x00;
  response_bytes[2] = 0x00;
  response_bytes[3] = 0x00;

  *res_len += 4;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}

/**
 * Get Backup Gateway MAC Address
 */
void lan_param_get_bkp_gateway_mac_addr(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  response_bytes[0] = 0x00;
  response_bytes[1] = 0x00;
  response_bytes[2] = 0x00;
  response_bytes[3] = 0x00;
  response_bytes[4] = 0x00;
  response_bytes[5] = 0x00;

  *res_len += 6;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}

/**
 *  Get Community String
 */
void lan_param_get_community_string(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
	// TODO: 15
}

/**
 * Get 802.1q VLAN ID (12-bit)
 */
void lan_param_get_802_1q_vlan_id(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  response_bytes[0] = 0x00; // Least significant 8-bits of the VLAN ID. 0x00 if VLAN ID not used.
  response_bytes[1] = 0x00; // VLAN ID enable bit 7: 0b = disabled, 1b = enable.

  *res_len += 2;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}


/**
 * Get 802.1q VLAN Priority
 */
void lan_param_get_802_1q_vlan_priority(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  response_bytes[0] = 0x00; // Ignored when VLAN ID enable is 0b (disabled). By default, this should be set 0x00.

  *res_len += 1;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}


/**
 * Get RMCP+ Messaging Cipher Suite Entry Support (Read Only)
 */
void lan_param_get_rmcp_plus_msh_cipher_suite_entry_support(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  response_bytes[0] = 0x01; // Number of Cipher Suite entries. The implementation supports just one, RAKP-NONE.

  *res_len += 1;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}


/**
 * Get RMCP+ Messaging Cipher Suite Entries (Read Only)
 */
void lan_param_get_rmcp_plus_msh_cipher_suite_entries(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  response_bytes[0] = 0x00; // Cipher Suite ID entry #0: RAKP-NONE

  *res_len += 1;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}

/**
 *  Get RMCP+ Messaging Cipher Suite Privilege Levels
 */
void lan_param_get_rmcp_plus_msh_cipher_suite_privilege_levels(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  response_bytes[0] = 0x00;
  response_bytes[1] = 0x00;

  *res_len += 2;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}

/**
 *  Get Bad Password Threshold (optional)
 */
void lan_param_get_bad_password_threshold(ipmi_channel_enum_t channel_origin, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  response_bytes[0] = 0x00; // do not generate an event message when the user is disable.
  response_bytes[1] = 0x00; // bad password threshold number. there is no limit on bad passwords.

  response_bytes[2] = 0x00; // attempt count reset interval is disable. (2-bytes)
  response_bytes[3] = 0x00;

  response_bytes[4] = 0x00; // user lockout interval is disable. (2-bytes)
  response_bytes[5] = 0x00;

  *res_len += 6;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}







