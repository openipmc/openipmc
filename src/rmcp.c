/*************************************************************************************/
/*                                                                                   */
/* This Source Code Form is subject to the terms of the Mozilla Public               */
/* License, v. 2.0. If a copy of the MPL was not distributed with this               */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                          */
/*                                                                                   */
/*************************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file rmcp.c
 *
 * @author Antionio V. G. Bassi
 *
 * @date YYYY / MM / DD - 2023 / 02 / 28
 *
 * @brief Remote Management and Control Protocol - RMCP source code.
 *        This software was designed to be compliant wirh IPMI v2.0 specification.
 *
 */

/* Standard C */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

/* OpenIPMC-SW header */
#include "ipmi_msg_router.h"
#include "sol.h"
#include "rmcp.h"

#define RMCP_MSG_CLASS_ASF  0x06
#define RMCP_MSG_CLASS_IPMI 0x07
#define RMCP_MSG_CLASS_OEM  0x08

#define RMCP_ASF_MSG_TYPE_PING   0x80
#define RMCP_ASF_MSG_TYPE_PONG   0x40
#define RMCP_ASF_SUPPORTED_ENTS  0x81
#define RMCP_ASF_SUPPORTED_INTS  0x00

#define RMCP_SESSION_T_IPMIV15  0x00
#define RMCP_SESSION_T_IPMIV20  0x01

/* Macros for ipmi session properties */
#define RMCP_IPMI_SESSION_AUTH_T_MSK          0x2F
#define RMCP_IPMI_SESSION_AUTH_T_NONE         0x00
#define RMCP_IPMI_SESSION_AUTH_T_MD2          0x01
#define RMCP_IPMI_SESSION_AUTH_T_MD5          0x02
#define RMCP_IPMI_SESSION_AUTH_T_RSVD         0x03
#define RMCP_IPMI_SESSION_AUTH_T_PWRD         0x04
#define RMCP_IPMI_SESSION_AUTH_T_OEM          0x05
#define RMCP_IPMI_SESSION_AUTH_T_FMT_LANPLUS  0x06
#define RMCP_IPMI_SESSION_EXT_AUTH_CPBLTS     0x80

#define RMCP_IPMI_SESSION_T_IPMIV15_NO_AUTH ( RMCP_IPMI_SESSION_AUTH_T_NONE )
#define RMCP_IPMI_SESSION_T_IPMIV20         ( RMCP_IPMI_SESSION_AUTH_T_FMT_LANPLUS )

/* Macros for ipmi session authentication properties */
#define RMCP_IPMI_SESSION_EXT_LSB           (0b00000001)
#define RMCP_IPMI_SESSION_EXT_2KEY_LOGIN    (RMCP_IPMI_SESSION_EXT_LSB << 5UL)
#define RMCP_IPMI_SESSION_EXT_AUTH_PER_MSG  (RMCP_IPMI_SESSION_EXT_LSB << 4UL)
#define RMCP_IPMI_SESSION_EXT_USRLVL_AUTH   (RMCP_IPMI_SESSION_EXT_LSB << 3UL)
#define RMCP_IPMI_SESSION_EXT_NULL_USR_DIS  (RMCP_IPMI_SESSION_EXT_LSB << 2UL)
#define RMCP_IPMI_SESSION_EXT_NULL_USR_EN   (RMCP_IPMI_SESSION_EXT_LSB << 1UL)
#define RMCP_IPMI_SESSION_EXT_ANON_LOGIN    (RMCP_IPMI_SESSION_EXT_LSB << 0UL)

#define RMCP_IPMI_NETFN_Msk      0xFC
#define RMCP_IPMI_LUN_Msk        0x03

#define RMCP_IPMI_NETFN_LSB_Pos (2UL)
#define RMCP_IPMI_NETFN_LSB_Msk (1UL << RMCP_IPMI_NETFN_LSB_Pos)
#define RMCP_IPMI_NETFN_LSB     (RMCP_IPMI_NETFN_LSB_Msk)

/*
 * Give us an 'H' (0x48)
 * Give us an 'i' (0x69)
 * Give us a  'g' (0x67)
 * Give us a  'g' (0x67)
 * Give us a  's' (0x73)
 *
 * And what have we got?
 */
#define S   (1000UL)
#define MIN (60*S)

#define DEFAULT_RMCP_SESSION_ADMIN_USERNAME  {0x48, 0x69, 0x67, 0x67, 0x73}
#define DEFAULT_RMCP_SESSION_ADMIN_PASSWORD  {0x3C, 0x50, 0x57, 0x44, 0x3E}
#define DEFAULT_RMCP_SESSION_IDLE_MS_TIMEOUT (30*S)

#ifndef OpenIPMC_RMCP_SESSION_ADMIN_USERNAME_STRING
#define OpenIPMC_RMCP_SESSION_ADMIN_USERNAME_STRING   DEFAULT_RMCP_SESSION_ADMIN_USERNAME
#endif /* OpenIPMC_RMCP_SESSION_ADMIN_USERNAME_STRING */

#ifndef OpenIPMC_RMCP_SESSION_ADMIN_PASSWORD_STRING
#define OpenIPMC_RMCP_SESSION_ADMIN_PASSWORD_STRING   DEFAULT_RMCP_SESSION_ADMIN_PASSWORD
#endif /* OpenIPMC_RMCP_SESSION_ADMIN_USERNAME_STRING */

#ifndef OpenIPMC_RMCP_SESSION_IDLE_MS_TIMEOUT
#define OpenIPMC_RMCP_SESSION_IDLE_MS_TIMEOUT (DEFAULT_RMCP_SESSION_IDLE_MS_TIMEOUT)
#endif  /*OpenIPMC_RMCP_SESSION_IDLE_MS_TIMEOUT*/

#ifndef OPENIPMC_RMCP_OP_QUEUE_DEPH
#define OPENIPMC_RMCP_OP_QUEUE_DEPH 3
#endif  /* OpenIPMC_RMCP_OP_QUEUE_DEPH */

#define OPENIPMC_RMCP_OP_DATA_SIZE  212
#define OPENIPMC_RMCP_OP_QUEUE_ARRAY_SIZE (OPENIPMC_RMCP_OP_QUEUE_DEPH*OPENIPMC_RMCP_OP_DATA_SIZE)

/* RMCP Session status */
static rmcp_session_status_t rmcp_session_status = RMCP_NO_SESSION;

/* Local types for RMCP Operation Task */

typedef enum
{
  LAN_IPMI_MSG,
  LANPLUS_IPMI_MSG_OUTSIDE_SESSION,
  LANPLUS_IPMI_MSG_IN_SESSION,
  LANPLUS_IPMI_MSG_REPEAT_PACKET,
  LANPLUS_OPEN_SESSION,
  LANPLUS_RAKP_EXCHANGE,
} rmcp_op_cod_t;

typedef struct
{
  rmcp_ip_net_params_t net_params;
  rmcp_op_cod_t      type;
  rmcp_t             pkt;
  uint16_t           pkt_len;
  ipmi_msg_t         ipmi_in_msg;
} rmcp_op_data_t;


uint32_t rmcp_get_channel_auth_cap(void);

TaskHandle_t rmcp_op_task_handle = NULL;

/* Variables for RMCP Operation Task */
__attribute__((section(".rmcp_buffer")))
static uint8_t        rmcp_op_queue_array[OPENIPMC_RMCP_OP_QUEUE_ARRAY_SIZE]={0};
static StaticQueue_t  rmcp_queue_data_static;
static QueueHandle_t  rmcp_op_queue       = NULL;
static QueueHandle_t  rmcp_watchdog_queue = NULL;

static rmcp_op_data_t rmcp_op_in;
static rmcp_op_data_t rmcp_op_out;

static ipmi_msg_t output_msg;

/* Message constants for packet processing */
const size_t ipmb_max_size                     = 32UL;
const size_t rmcp_header_size                  = 4UL;
const size_t rmcp_lan_auth_ipmi_header_size    = 26UL;
const size_t rmcp_lan_ipmi_header_size         = 10UL;
const size_t rmcp_lanplus_ipmi_oem_header_size = 20UL;
const size_t rmcp_lanplus_ipmi_header_size     = 12UL;


/* IPMI Channel and session information */

/* This is the first user entry with administrator privileges */
// TODO: Set a custom setting to redefine the built-in admin user.
static rmcp_ipmi_user_entry_t rmcp_stdadmin_usr =
{
  .user_privilege_code = RMCP_IPMI_PRIV_ADMN_LVL,
  .username = OpenIPMC_RMCP_SESSION_ADMIN_USERNAME_STRING,
  .password = OpenIPMC_RMCP_SESSION_ADMIN_PASSWORD_STRING,
  .next     = NULL
};

static rmcp_ipmi_user_entry_t *rmcp_ipmi_user_entry_head = &rmcp_stdadmin_usr;
static const rmcp_ipmi_priv_t channel_privilege = RMCP_IPMI_PRIV_ADMN_LVL;


/* 32-bit field for channel authentication capabilities
 *
 * 0      ...      7|8         ...        15|16        ...        23|24       ...      31|
 * +----------------+-----------------------+-----------------------+--------------------+
 * | Channel number | Auth. Type Support[1] | Auth. Type Support[2] | Extd. Capabilities |
 * +----------------+-----------------------+-----------------------+--------------------+
 */
static const rmcp_channel_config_t rmcp_channel_config = {.auth_cap = 0x01811802,
                                                          .privilege_level = RMCP_IPMI_PRIV_ADMN_LVL };
static int ipmi_session_invalid = 0UL;
static uint8_t *rmcp_last_packet_buffer = NULL;
static rmcp_ipmi_session_handle_t *ipmi_session = NULL;

static rmcp_ip_net_params_t local_net_params = {0};
static rmcp_callbacks_t* rmcp_callbacks = NULL;



void rmcp_session_handle_set(rmcp_ipmi_session_handle_t *session )
{
  ipmi_session = session;
  return;
}

void rmcp_session_handle_get(rmcp_ipmi_session_handle_t** session)
{
  *session =  ipmi_session;
  return;
}

int rmcp_get_a_copy_session_status(rmcp_ipmi_session_handle_t *h)
{
  int err = 0;
  if((NULL != h)&&(NULL != ipmi_session))
  {
    memcpy((void *)h, (void *)ipmi_session, sizeof(rmcp_ipmi_session_handle_t));
    err = 1;
  }
  return err;
}

int rmcp_get_session_invalid()
{
  return ipmi_session_invalid;
}

void rmcp_set_session_invalid(int invalid)
{
  ipmi_session_invalid = invalid;
}

uint32_t rmcp_get_channel_auth_cap(void)
{
  return rmcp_channel_config.auth_cap;
}

rmcp_ipmi_priv_t rmcp_get_channel_privilege()
{
  return rmcp_channel_config.privilege_level;
}

void rmcp_set_session_status(rmcp_session_status_t status)
{
  rmcp_session_status = status;
}

void rmcp_get_session_status(rmcp_session_status_t *status)
{
  *status = rmcp_session_status;
}

void rmcp_set_local_net_params(rmcp_ip_net_params_t net_params)
{
  local_net_params = net_params;
}

rmcp_ip_net_params_t rmcp_get_local_net_params(void)
{
  return local_net_params;
}

void rmcp_callbacks_get(rmcp_callbacks_t** callbacks)
{
  *callbacks = rmcp_callbacks;
}

rmcp_init_ret_codet_t rmcp_init(rmcp_callbacks_t* callbacks)
{
  rmcp_callbacks = callbacks;

  if(&(rmcp_callbacks->blocking_rand_gen) == NULL)
    return RMCP_CALLBACK_ERROR_BLOCKING_RAND_GEN;

  if(&(rmcp_callbacks->get_guid) == NULL)
    return RMCP_CALLBACK_ERROR_GET_GUID;

  if(&(rmcp_callbacks->malloc) == NULL)
    return RMCP_CALLBACK_ERROR_MALLOC;

  if(&(rmcp_callbacks->mem_free) == NULL)
    return RMCP_CALLBACK_ERROR_MEM_FREE;

  if(&(rmcp_callbacks->rmcp_udp_send_packet) == NULL)
    return RMCP_CALLBACKS_ERROR_SEND_UDP;

  if(&(rmcp_callbacks->rmcp_event_send) == NULL)
    return RMCP_CALLBACK_ERROR_EVENT_REPORT;

  // RMCP Operation Queue creation
  rmcp_op_queue = xQueueCreateStatic(OPENIPMC_RMCP_OP_QUEUE_DEPH,sizeof(rmcp_op_data_t),rmcp_op_queue_array,&rmcp_queue_data_static);

  if(NULL == rmcp_op_queue)
    return RMCP_QUEUE_OP_ERROR;

  // RMCP Watchdog Queue creation
  rmcp_watchdog_queue = xQueueCreate(1UL,sizeof(uint32_t));

  if(NULL == rmcp_watchdog_queue)
    return RMCP_QUEUE_WATCHDOG_ERROR;

  rmcp_session_status = RMCP_NO_SESSION;

  return RMCP_CALLBACK_ERROR_OK;
}

static int __rmcp_FreeRTOS_pat_watchdog(void)
{
  uint32_t tickref = osKernelGetTickCount();
  return (pdTRUE == xQueueOverwrite(rmcp_watchdog_queue,&tickref));
}

static void rmcp_terminate_ipmi_session(void)
{
  if( (NULL != ipmi_session) && (ipmi_session_invalid) )
  {
    // Clear allocated resources for IPMI session
    rmcp_callbacks->mem_free((void*)ipmi_session->last_pkt);
    ipmi_session->user = NULL;
    ipmi_session->last_pkt_size = 0UL;
    ipmi_session->last_pkt = NULL;
    ipmi_session->id = 0UL;
    ipmi_session->remote_console_id = 0UL;
    ipmi_session->last_seq_num = 0UL;
    ipmi_session->payload_inst_mask = 0U;
    rmcp_callbacks->mem_free((void *) ipmi_session);
    ipmi_session = NULL;
    ipmi_session_invalid = 0UL;
    rmcp_session_status = RMCP_NO_SESSION;
  }
  return;
}

static uint32_t rmcp_generate_session_id(void)
{
  const uint32_t timeout = 5UL;
  const uint32_t bufSize = 16UL;
  uint32_t id[4] = {0};
  rmcp_callbacks->blocking_rand_gen((void *)id, bufSize, timeout);
  return (~(id[0] + id[1] + id[2] + id[3]) + 1UL);
}

static void rmcp_gen_rand_number(uint8_t *buf)
{
  const uint32_t timeout = 5UL;
  const uint16_t bufSize = 16U;
  rmcp_callbacks->blocking_rand_gen(buf, bufSize, timeout);
  return;
}


static rmcp_status_code_t rmcp_rqstd_priv_lookup(uint8_t rqstd_priv_code,
                                                 uint8_t username_len,
                                                 uint8_t *username)
{
  rmcp_status_code_t      err = RMCP_STATUS_INVNAMELEN;
  rmcp_ipmi_user_entry_t *usr = rmcp_ipmi_user_entry_head;

  // Check receive username length
  if( RMCP_IPMI_SESSION_MAX_USRNAME_LEN > username_len )
  {
    err = RMCP_STATUS_UNAUTHNAME;
    // Look for username entries
    while( NULL != usr )
    {
      if( ( 0 == memcmp((void *)usr->username, username, username_len) ) )
      {
        // Valid entry found
        rqstd_priv_code &= 0x0F;
        ipmi_session->user = usr;
        err = (usr->user_privilege_code < rqstd_priv_code)?(RMCP_STATUS_UNAUTHROLE):(RMCP_STATUS_OK);
        break;
      }
      usr = usr->next;
    }
  }
  return err;
}

static rmcp_status_code_t rmcp_lanplus_rakp_exchange(rmcp_ip_net_params_t net_params, uint8_t rakp_msg_step, uint8_t *payload)
{
  const uint16_t hmac_sha1_key_len   = 20U;
  const uint16_t rakp_2_msg_base_len = 40U;
  const uint16_t rakp_4_msg_base_len = 8U;
  const uint16_t rakp_err_msg_len    = 8U;

  uint8_t  *guid_buf;
  uint16_t gui_len;

  uint32_t session_id  = 0;
  uint8_t  rakp_next_step = 0;
  rmcp_status_code_t err = RMCP_STATUS_INVAUTH;

  /* For each RAKP message step, we perform security checks
   * to validate the authenticity of the message followed
   * by the required operation. If any check fails, an error
   * message is generated.
   */
  switch(rakp_msg_step)
  {
    case RMCP_IPMI_PLD_T_RAKP_MSG_1:
    {
      rakp_next_step = RMCP_IPMI_PLD_T_RAKP_MSG_2;

      // Extract ipmi session ID
      session_id = *((uint32_t *)&payload[4]);

      // Check if session id is active
      if( session_id == ipmi_session->id )
      {
        err = rmcp_rqstd_priv_lookup(payload[24], payload[27], &payload[28]);
      }
      break;
    }
    case RMCP_IPMI_PLD_T_RAKP_MSG_3:
    {
      rakp_next_step = RMCP_IPMI_PLD_T_RAKP_MSG_4;
      err = payload[1];

      // Did the remote console report an error?
      if( RMCP_STATUS_OK != err )
      {
        /* The remote console has generated an error.
         * We are not required to reply, instead we
         * wait for the remote console to retry the
         * RAKP exchange from step 1.
         */
        return err;
      }

      /* We received a successful operation from the remote console.
       * However is the referred session a valid one?
       */
      session_id = *((uint32_t *)&payload[4]);
      err = ( session_id == ipmi_session->id ) ? RMCP_STATUS_OK : RMCP_STATUS_INVID;
      break;
    }
    default:
      err = RMCP_STATUS_INVPLD;
  }

  /*
   * Now we build the correspondent message based on
   * the generated error and the RAKP exchange process step.
   */

  rmcp_op_in.type       = LANPLUS_RAKP_EXCHANGE;
  rmcp_op_in.net_params = net_params;

  rmcp_op_in.pkt.lanplus.version        = RMCP_VERSION;
  rmcp_op_in.pkt.lanplus.rsvd           = 0x00;
  rmcp_op_in.pkt.lanplus.rmcp_sequence  = RMCP_SEQUENCE_NUMBER_IPMI;
  rmcp_op_in.pkt.lanplus.msg_class      = RMCP_MSG_CLASS_IPMI;

  *((uint32_t *)rmcp_op_in.pkt.lanplus.session_id)    = 0UL;
  *((uint32_t *)rmcp_op_in.pkt.lanplus.ipmi_sequence) = 0UL;

  // IPMIv2.0 Header - Payload type field
  if( ( RMCP_STATUS_OK == err ) )
  {
    if( RMCP_IPMI_PLD_T_RAKP_MSG_2 == rakp_next_step )
    {
      // Build RAKP message 2
      rmcp_op_in.pkt.lanplus.auth_t = RMCP_IPMI_SESSION_AUTH_T_FMT_LANPLUS;
      rmcp_op_in.pkt.lanplus.payload_t = rakp_next_step;
      *((uint16_t *)&rmcp_op_in.pkt.lanplus.payload_length) = (rakp_2_msg_base_len + hmac_sha1_key_len);

      // RAKP Message 2
      rmcp_op_in.pkt.lanplus.payload_buffer[0] = payload[0];
      rmcp_op_in.pkt.lanplus.payload_buffer[1] = err;
      *((uint32_t *)&rmcp_op_in.pkt.lanplus.payload_buffer[4]) = ipmi_session->remote_console_id;

      // Fetch random number from implementation callback.
      rmcp_gen_rand_number(&rmcp_op_in.pkt.lanplus.payload_buffer[8]);

      // Get the GUID from buffer
      rmcp_callbacks->get_guid(&guid_buf,&gui_len);

      if ( 16U == gui_len)
      {
        rmcp_op_in.pkt.lanplus.payload_buffer[24] = guid_buf[0];
        rmcp_op_in.pkt.lanplus.payload_buffer[25] = guid_buf[1];
        rmcp_op_in.pkt.lanplus.payload_buffer[26] = guid_buf[2];
        rmcp_op_in.pkt.lanplus.payload_buffer[27] = guid_buf[3];
        rmcp_op_in.pkt.lanplus.payload_buffer[28] = guid_buf[4];
        rmcp_op_in.pkt.lanplus.payload_buffer[29] = guid_buf[5];
        rmcp_op_in.pkt.lanplus.payload_buffer[30] = guid_buf[6];
        rmcp_op_in.pkt.lanplus.payload_buffer[31] = guid_buf[7];
        rmcp_op_in.pkt.lanplus.payload_buffer[32] = guid_buf[8];
        rmcp_op_in.pkt.lanplus.payload_buffer[33] = guid_buf[9];
        rmcp_op_in.pkt.lanplus.payload_buffer[34] = guid_buf[10];
        rmcp_op_in.pkt.lanplus.payload_buffer[35] = guid_buf[11];
        rmcp_op_in.pkt.lanplus.payload_buffer[36] = guid_buf[12];
        rmcp_op_in.pkt.lanplus.payload_buffer[37] = guid_buf[13];
        rmcp_op_in.pkt.lanplus.payload_buffer[38] = guid_buf[14];
        rmcp_op_in.pkt.lanplus.payload_buffer[39] = guid_buf[15];

        rmcp_op_in.pkt_len = rmcp_header_size + rmcp_lanplus_ipmi_header_size + rakp_2_msg_base_len + hmac_sha1_key_len;

        rmcp_session_status = RMCP_RAKP_2;

      }
      else
      {
    	return RMCP_STATUS_INVAUTH;
      }
    }
    else
    {
      // Build RAKP message 4
      rmcp_op_in.pkt.lanplus.auth_t = RMCP_IPMI_SESSION_AUTH_T_FMT_LANPLUS;
      rmcp_op_in.pkt.lanplus.payload_t = rakp_next_step;
      *((uint16_t *)&rmcp_op_in.pkt.lanplus.payload_length) = (rakp_4_msg_base_len + hmac_sha1_key_len);

      rmcp_op_in.pkt.lanplus.payload_buffer[0] = payload[0];
      rmcp_op_in.pkt.lanplus.payload_buffer[1] = err;
      *((uint32_t *)&rmcp_op_in.pkt.lanplus.payload_buffer[4]) = ipmi_session->remote_console_id;

      rmcp_op_in.pkt_len = rmcp_header_size + rmcp_lanplus_ipmi_header_size + rakp_2_msg_base_len + hmac_sha1_key_len;

      rmcp_session_status = RMCP_RAKP_4;
    }
  }
  else
  {
    // Build error message
    rmcp_op_in.pkt.lanplus.auth_t = RMCP_IPMI_SESSION_AUTH_T_FMT_LANPLUS;
    rmcp_op_in.pkt.lanplus.payload_t = rakp_next_step;
    *((uint16_t *)&rmcp_op_in.pkt.lanplus.payload_length) = rakp_err_msg_len;

    rmcp_op_in.pkt.lanplus.payload_buffer[0] = payload[0];
    rmcp_op_in.pkt.lanplus.payload_buffer[1] = err;
    rmcp_op_in.pkt.lanplus.payload_buffer[4] = ipmi_session->remote_console_id;

    // Key Exchange has failed, notify invalid session and kill it.
    ipmi_session_invalid = 1;
    rmcp_terminate_ipmi_session();

    rmcp_op_in.pkt_len = rmcp_header_size + rmcp_lanplus_ipmi_header_size + rakp_err_msg_len;
  }

  // Send packet operation
  xQueueSendToBack(rmcp_op_queue,&rmcp_op_in,0UL);

  return err;
}

static rmcp_status_code_t rmcp_lanplus_open_session(rmcp_ip_net_params_t net_params, uint8_t *payload)
{
  const uint16_t open_session_rs_len = 36U;
  const uint16_t open_session_rs_err_len = 8U;

  rmcp_status_code_t err = RMCP_STATUS_NORSRCS;

  memset((void*)&rmcp_op_in,0,sizeof(rmcp_op_data_t));

  // Load Operation code and network parameters
  rmcp_op_in.net_params = net_params;
  rmcp_op_in.type       = LANPLUS_OPEN_SESSION;

  // Initialise response buffer and load first header
  rmcp_op_in.pkt.lanplus.version        = RMCP_VERSION;
  rmcp_op_in.pkt.lanplus.rsvd           = 0x00;
  rmcp_op_in.pkt.lanplus.rmcp_sequence  = RMCP_SEQUENCE_NUMBER_IPMI;
  rmcp_op_in.pkt.lanplus.msg_class      = RMCP_MSG_CLASS_IPMI;

  *((uint32_t *)rmcp_op_in.pkt.lanplus.session_id)    = 0UL;
  *((uint32_t *)rmcp_op_in.pkt.lanplus.ipmi_sequence) = 0UL;


  if(( RMCP_NO_SESSION != rmcp_session_status ) && (RMCP_ONGOING_SESSION != rmcp_session_status ))
  {
    if(ipmi_session->payload_inst_mask & RMCP_IPMI_PLD_T_SOL)
    {
      sol_deactivate();
    }
	ipmi_session_invalid = 1;
    rmcp_terminate_ipmi_session();
  }

  if( NULL == ipmi_session)
  {
    err = RMCP_STATUS_NOROLERSRCS;
    ipmi_session = (rmcp_ipmi_session_handle_t*) rmcp_callbacks->malloc(sizeof(rmcp_ipmi_session_handle_t));
    rmcp_last_packet_buffer = (uint8_t *) rmcp_callbacks->malloc((size_t) RMCP_MAX_PLD_BUF_LENGTH);

    if(( NULL != ipmi_session) && (NULL != rmcp_last_packet_buffer))
    {
      rmcp_session_status = RMCP_OPENING_SESSION;

      // Allocation successful
      err = RMCP_STATUS_OK;
      memset((void *)rmcp_last_packet_buffer, 0, RMCP_MAX_PLD_BUF_LENGTH);
      memset((void *)ipmi_session, 0, sizeof(rmcp_ipmi_session_handle_t));
      ipmi_session->last_pkt = rmcp_last_packet_buffer;
      ipmi_session->last_pkt_size = 0UL;
      ipmi_session->remote_console_id = *((uint32_t *)&payload[4]);
      ipmi_session->id = rmcp_generate_session_id();

      if( 0 == ipmi_session->id )
        return RMCP_STATUS_ERROR_IN_RAND_NUM_GEN;

      ipmi_session->last_seq_num = 0UL;
      ipmi_session->payload_inst_mask = 0U;
      ipmi_session->client_net_params = net_params;

      rmcp_op_in.pkt.lanplus.auth_t    = RMCP_IPMI_SESSION_AUTH_T_FMT_LANPLUS;
      rmcp_op_in.pkt.lanplus.payload_t = RMCP_IPMI_PLD_T_OPEN_SESSION_RS;
      *((uint32_t *)&rmcp_op_in.pkt.lanplus.session_id)     = ipmi_session->id;
      *((uint16_t *)&rmcp_op_in.pkt.lanplus.payload_length) = open_session_rs_len;

      rmcp_op_in.pkt.lanplus.payload_buffer[0] = payload[0];
      rmcp_op_in.pkt.lanplus.payload_buffer[1] = err;
      rmcp_op_in.pkt.lanplus.payload_buffer[2] = channel_privilege;
      rmcp_op_in.pkt.lanplus.payload_buffer[3] = 0x00;
      rmcp_op_in.pkt.lanplus.payload_buffer[4] = payload[4];
      rmcp_op_in.pkt.lanplus.payload_buffer[5] = payload[5];
      rmcp_op_in.pkt.lanplus.payload_buffer[6] = payload[6];
      rmcp_op_in.pkt.lanplus.payload_buffer[7] = payload[7];
      *((uint32_t *)&rmcp_op_in.pkt.lanplus.payload_buffer[8]) = ipmi_session->id;

      // Authentication payload: Just byte 15, rest are zeroes
      rmcp_op_in.pkt.lanplus.payload_buffer[15] = 0x08;

      // Integrity payload: Just byte 20, rest are zeroes
      rmcp_op_in.pkt.lanplus.payload_buffer[20] = 0x01;

      // Confidentiality payload: Just byte 28, rest are zeroes
      rmcp_op_in.pkt.lanplus.payload_buffer[28] = 0x02;

      rmcp_op_in.pkt_len = rmcp_header_size + rmcp_lanplus_ipmi_header_size + open_session_rs_len;
    }
    else
    {
      // Allocation for a session has failed!
      rmcp_op_in.pkt.lanplus.auth_t = RMCP_IPMI_SESSION_AUTH_T_FMT_LANPLUS;
      rmcp_op_in.pkt.lanplus.payload_t = RMCP_IPMI_PLD_T_OPEN_SESSION_RS;
      *((uint16_t *)&rmcp_op_in.pkt.lanplus.payload_length) = open_session_rs_err_len;

      rmcp_op_in.pkt.lanplus.payload_buffer[0] = payload[0];
      rmcp_op_in.pkt.lanplus.payload_buffer[1] = err;
      rmcp_op_in.pkt.lanplus.payload_buffer[2] = 0x00;
      rmcp_op_in.pkt.lanplus.payload_buffer[3] = 0x00;
      rmcp_op_in.pkt.lanplus.payload_buffer[4] = payload[4];
      rmcp_op_in.pkt.lanplus.payload_buffer[5] = payload[5];
      rmcp_op_in.pkt.lanplus.payload_buffer[6] = payload[6];
      rmcp_op_in.pkt.lanplus.payload_buffer[7] = payload[7];

      rmcp_op_in.pkt_len = rmcp_header_size + rmcp_lanplus_ipmi_header_size + open_session_rs_err_len;

      rmcp_session_status = RMCP_NO_SESSION;
    }
  } /* if( NULL == ipmi_session) */
  else
  {
    // A session is already active, we do not have resources for another.
    rmcp_op_in.pkt.lanplus.auth_t = RMCP_IPMI_SESSION_AUTH_T_FMT_LANPLUS;
    rmcp_op_in.pkt.lanplus.payload_t = RMCP_IPMI_PLD_T_OPEN_SESSION_RS;
    *((uint16_t *)&rmcp_op_in.pkt.lanplus.payload_length) = open_session_rs_err_len;

    rmcp_op_in.pkt.lanplus.payload_buffer[0] = payload[0];
    rmcp_op_in.pkt.lanplus.payload_buffer[1] = err;
    rmcp_op_in.pkt.lanplus.payload_buffer[2] = 0x00;
    rmcp_op_in.pkt.lanplus.payload_buffer[3] = 0x00;
    rmcp_op_in.pkt.lanplus.payload_buffer[4] = payload[4];
    rmcp_op_in.pkt.lanplus.payload_buffer[5] = payload[5];
    rmcp_op_in.pkt.lanplus.payload_buffer[6] = payload[6];
    rmcp_op_in.pkt.lanplus.payload_buffer[7] = payload[7];

    rmcp_op_in.pkt_len = rmcp_header_size + rmcp_lanplus_ipmi_header_size + open_session_rs_err_len;

  }

  // Send packet operation
  xQueueSendToBack(rmcp_op_queue,&rmcp_op_in,0UL);

  return err;
}

/**
 * @note IPMI message formats:
 *
 * request:
 *            (even netfn)                                                (0 or more)
 *  +--------+----------------+----------+--------+----------------+-----+------------+----------+
 *  | rsAddr | netfn[*rs*lun] | checksum | rqAddr | rqSeq[*rq*Lun] | cmd | data bytes | checksum |
 *  +--------+----------------+----------+--------+----------------+-----+------------+----------+
 *
 * response:
 *             (odd netfn)                                                 (0 or more)
 *  +--------+----------------+----------+--------+----------------+-----+---------------+----------+
 *  | rqAddr | netfn[*rq*lun] | checksum | rsAddr | rqSeq[*rs*Lun] | cmd | response data | checksum |
 *  +--------+----------------+----------+--------+----------------+-----+---------------+----------+
 */
static ipmi_router_ret_code_t rmcp_ipmi_msg_process(ipmi_msg_t *input_msg, ipmi_msg_t *output_msg)
{
  ipmi_router_ret_code_t ret;

  // Lazy initialization of channel input.
  ret = ipmi_router_channel_input_enable(IPMI_ROUTER_CHANNEL_RMCP);
  if( IPMI_MSG_ROUTER_RET_OK != ret )
    return ret;

  // Lazy initialization of channel output.
  ret = ipmi_router_channel_output_enable(IPMI_ROUTER_CHANNEL_RMCP);
  if( IPMI_MSG_ROUTER_RET_OK != ret )
    return ret;

  // Define if it is  a request or response.
  if( ( input_msg->data[1] & (1<<2) ) == 0x00 )
  {
    /* The input data is a request */

    // - Post the request
    ret = ipmi_router_queue_req_post(input_msg);

    // - Wait for the response.
    ret = ipmi_router_queue_out_get(IPMI_ROUTER_CHANNEL_RMCP,output_msg);
  }
  else
  {
    /* The input data is a response */

    // - Post the response
    ret = ipmi_router_queue_res_post(input_msg);
  }
  return ret;
}


static rmcp_status_code_t rmcp_lanplus_solve_ipmi(rmcp_ip_net_params_t net_params, size_t msg_length, void *msg)
{
  rmcp_status_code_t err = RMCP_STATUS_INVPLD;
  uint32_t session_seq_num = 0UL;
  uint32_t ipmi_session_id = 0UL;

  uint8_t *ipmi = (((uint8_t *)msg) + rmcp_header_size);

  ipmi_session_id = *((uint32_t *)&ipmi[2]);
  session_seq_num = *((uint32_t *)&ipmi[6]);

  switch(ipmi[1] & RMCP_IPMI_PLD_T_MSK)
  {
    case RMCP_IPMI_PLD_T_MSG:
    {
      if(0UL == ipmi_session_id)
      { // message are sent 'outside' of a session.

        rmcp_op_in.net_params = net_params;
        rmcp_op_in.type       = LANPLUS_IPMI_MSG_OUTSIDE_SESSION;

        rmcp_op_in.pkt.lanplus.version       = RMCP_VERSION;
        rmcp_op_in.pkt.lanplus.rsvd          = 0x00;
        rmcp_op_in.pkt.lanplus.rmcp_sequence = RMCP_SEQUENCE_NUMBER_IPMI;
        rmcp_op_in.pkt.lanplus.msg_class     = RMCP_MSG_CLASS_IPMI;

        rmcp_op_in.pkt.lanplus.auth_t    = RMCP_IPMI_SESSION_AUTH_T_FMT_LANPLUS;
        rmcp_op_in.pkt.lanplus.payload_t = RMCP_IPMI_PLD_T_MSG;
        rmcp_op_in.pkt_len               = rmcp_header_size + rmcp_lanplus_ipmi_header_size;

        *((uint32_t *)rmcp_op_in.pkt.lanplus.session_id)    = 0UL;
        *((uint32_t *)rmcp_op_in.pkt.lanplus.ipmi_sequence) = 0UL;

        rmcp_op_in.ipmi_in_msg.channel = IPMI_ROUTER_CHANNEL_RMCP;
        rmcp_op_in.ipmi_in_msg.length  = (*((uint16_t *)&ipmi[10]));

        memcpy(&rmcp_op_in.ipmi_in_msg.data,&ipmi[12],rmcp_op_in.ipmi_in_msg.length);

        // Send packet operation
        xQueueSendToBack(rmcp_op_queue,&rmcp_op_in,0UL);


      } /* if(0UL == ipmi_session_id) */

      else if (NULL != ipmi_session)
      {
        err = RMCP_STATUS_INVID;

        if( ipmi_session_id == ipmi_session->id)
        {
          err = RMCP_STATUS_NORSRCS;

          // Is this packet subsequent from the previous?
          if(session_seq_num > ipmi_session->last_seq_num)
          {
            rmcp_op_in.type       = LANPLUS_IPMI_MSG_IN_SESSION;
            rmcp_op_in.net_params = net_params;

            rmcp_op_in.pkt.lanplus.version        = RMCP_VERSION;
            rmcp_op_in.pkt.lanplus.rsvd           = 0X00;
            rmcp_op_in.pkt.lanplus.rmcp_sequence  = RMCP_SEQUENCE_NUMBER_IPMI;
            rmcp_op_in.pkt.lanplus.msg_class      = RMCP_MSG_CLASS_IPMI;

            rmcp_op_in.pkt.lanplus.auth_t    = RMCP_IPMI_SESSION_AUTH_T_FMT_LANPLUS;
            rmcp_op_in.pkt.lanplus.payload_t = RMCP_IPMI_PLD_T_MSG;
            rmcp_op_in.pkt_len               = rmcp_header_size + rmcp_lanplus_ipmi_header_size;

            *((uint32_t *)rmcp_op_in.pkt.lanplus.session_id)    = ipmi_session->remote_console_id;
            *((uint32_t *)rmcp_op_in.pkt.lanplus.ipmi_sequence) = session_seq_num;

            rmcp_op_in.ipmi_in_msg.channel = IPMI_ROUTER_CHANNEL_RMCP;
            rmcp_op_in.ipmi_in_msg.length  = (*((uint16_t *)&ipmi[10]));

            memcpy(rmcp_op_in.ipmi_in_msg.data,&ipmi[12],rmcp_op_in.ipmi_in_msg.length);

            ipmi_session->last_seq_num  = session_seq_num;

            // Send packet operation
            xQueueSendToBack(rmcp_op_queue,&rmcp_op_in,0UL);

          } /* if(session_seq_num == (ipmi_session->last_seq_num + 1)) */
          else if( session_seq_num == ipmi_session->last_seq_num)
          {
            // This packet has the same sequence number as the previous, retry sending the previous packet.
            rmcp_op_in.type       = LANPLUS_IPMI_MSG_REPEAT_PACKET;
            rmcp_op_in.net_params = net_params;

            // Send packet operation
            xQueueSendToBack(rmcp_op_queue,&rmcp_op_in,0UL);
          }
        } /* if(ipmi_session_id == ipmi_session->id) */

      } /* if(NULL != ipmi_session) */
	  break;
    }/* RMCP_IPMI_PLD_T_MSG */

    case RMCP_IPMI_PLD_T_SOL:
    {
      if((ipmi_session->payload_inst_mask & RMCP_IPMI_PLD_T_SOL) && (ipmi_session_id == ipmi_session->id))
      {
        ipmi_session->last_seq_num = session_seq_num;
        sol_enqueue_payload((uint8_t *)msg);
      } /* if(ipmi_session_id == ipmi_session->id) */
	break;
    } /* RMCP_IPMI_PLD_T_SOL */

    case RMCP_IPMI_PLD_T_OEM:
    {
	  break;
    } /* RMCP_IPMI_PLD_T_OEM */

    case RMCP_IPMI_PLD_T_OPEN_SESSION_RQ:
    {
      err = rmcp_lanplus_open_session(net_params, &ipmi[12]);
	  break;
    } /* RMCP_IPMI_PLD_T_OPEN_SESSION_RQ */

    case RMCP_IPMI_PLD_T_RAKP_MSG_1:
    {
      err = rmcp_lanplus_rakp_exchange(net_params, RMCP_IPMI_PLD_T_RAKP_MSG_1, &ipmi[12]);
	  break;
    } /* RMCP_IPMI_PLD_T_RAKP_MSG_1 */

    case RMCP_IPMI_PLD_T_RAKP_MSG_3:
    {
      err = rmcp_lanplus_rakp_exchange(net_params, RMCP_IPMI_PLD_T_RAKP_MSG_3, &ipmi[12]);
	  break;
    } /* RMCP_IPMI_PLD_T_RAKP_MSG_3 */

    default:
    {
      // TODO: Check for any OEM handle values: OEM0 to OEM7, if not present break from switch
	  break;
    }
  }
  return err;
}

static rmcp_status_code_t rmcp_solve_ipmi(rmcp_ip_net_params_t net_params, size_t msg_length, uint8_t *msg)
{
  rmcp_status_code_t err = RMCP_STATUS_INVPLD;
  uint32_t session_seq_num = 0UL;
  uint32_t ipmi_session_id = 0UL;
  uint8_t *ipmi = (((uint8_t *)msg) + rmcp_header_size) ;

  switch(ipmi[0])
  {
    case RMCP_IPMI_SESSION_AUTH_T_NONE:
    {
      session_seq_num = (*((uint32_t*) &ipmi[1]));
      ipmi_session_id = (*((uint32_t*) &ipmi[5]));

      if( 0UL == ipmi_session_id) // 0UL for messages that are sent 'outside' of a session.
      {
        rmcp_op_in.net_params = net_params;
        rmcp_op_in.type       = LAN_IPMI_MSG;

        rmcp_op_in.pkt.lan.version       = RMCP_VERSION;
        rmcp_op_in.pkt.lan.rsvd          = 0x00;
        rmcp_op_in.pkt.lan.rmcp_sequence = RMCP_SEQUENCE_NUMBER_IPMI;
        rmcp_op_in.pkt.lan.msg_class     = RMCP_MSG_CLASS_IPMI;
        rmcp_op_in.pkt.lan.auth_t        = RMCP_IPMI_SESSION_AUTH_T_NONE;
        rmcp_op_in.pkt_len               = rmcp_header_size + rmcp_lan_ipmi_header_size;

        *((uint32_t *)rmcp_op_in.pkt.lanplus.session_id)    = 0UL;
        *((uint32_t *)rmcp_op_in.pkt.lanplus.ipmi_sequence) = 0UL;

        rmcp_op_in.ipmi_in_msg.channel = IPMI_ROUTER_CHANNEL_RMCP;
        rmcp_op_in.ipmi_in_msg.length  = ipmi[9];

        memcpy(&rmcp_op_in.ipmi_in_msg.data,&ipmi[10],rmcp_op_in.ipmi_in_msg.length);

        // Send packet operation
        xQueueSendToBack(rmcp_op_queue,&rmcp_op_in,0UL);

        err = RMCP_STATUS_OK;

      } /* if(0 == ipmi_session_id ) */
      else if( NULL != ipmi_session )
      {
        if(ipmi_session_id == ipmi_session->id)
        {
          if(session_seq_num != ipmi_session->last_seq_num)
          {
            // TODO: Solve in-session packets.
          }
          else
          {
            // TODO: Retransmit last received in-session packet.
          }
        }
      }
       break;
    } /* RMCP_IPMI_SESSION_AUTH_T_NONE */

    case RMCP_IPMI_SESSION_AUTH_T_MD2:
    {
      // TODO: Solve MD2 Authentication
      break;
    } /* RMCP_IPMI_SESSION_AUTH_T_MD2 */

    case RMCP_IPMI_SESSION_AUTH_T_MD5:
    {
      // TODO: Solve MD5 Authentication
      break;
    } /* RMCP_IPMI_SESSION_AUTH_T_MD5 */

    case RMCP_IPMI_SESSION_AUTH_T_PWRD:
    {
      // TODO: Solve password Authentication
      break;
    } /* RMCP_IPMI_SESSION_AUTH_T_PWRD */

    case RMCP_IPMI_SESSION_AUTH_T_FMT_LANPLUS:
    {
      err = rmcp_lanplus_solve_ipmi(net_params, msg_length, msg);
      break;
    } /* RMCP_IPMI_SESSION_AUTH_T_RMCPP */
    default:
    {
      break;
    }
  } /* switch(ipmi[0]) */
  return err;
}

void rmcp_udp_recv(rmcp_ip_net_params_t net_params, uint8_t* payload, uint16_t length)
{
  static uint8_t upper_lim = 31;
  static uint8_t lower_lim = 0;
  static uint8_t last_seq_num = 255;

  __rmcp_FreeRTOS_pat_watchdog();

  // Perform RMCP version check, only ASF 2.0 supported.
  if ( RMCP_VERSION == payload[0] )
  {
    last_seq_num = payload[2];
    switch(last_seq_num)
    {
      case RMCP_SEQUENCE_NUMBER_IPMI:
        rmcp_solve_ipmi(net_params, length, (void *)payload);
      break;
      default:
          if( ( upper_lim >= last_seq_num ) &&
              ( lower_lim <= last_seq_num ) &&
              ( last_seq_num != payload[2] ) )
          {
            last_seq_num = payload[2];
            ++upper_lim;
            ++lower_lim;
            // TODO: @Antonio: Solve non-IPMI packets.
          }
        break;
    }
  }
}

void rmcp_op_task(void *pvParameters)
{
  // Watchdog variables
  const uint32_t maxticks = pdMS_TO_TICKS(OpenIPMC_RMCP_SESSION_IDLE_MS_TIMEOUT);
  uint32_t tickref = osKernelGetTickCount();
  uint32_t tickupdate = tickref;

  // Packet processor variables
  rmcp_ip_net_params_t net_params;
  uint8_t  *udp_out_buffer = NULL;
  size_t   buffer_length = 0;

  // Wait for queue initialization.
  while( ( rmcp_watchdog_queue == NULL ) || (rmcp_op_queue == NULL) )
    taskYIELD();

  for(;;)
  {

    if( pdTRUE == xQueueReceive(rmcp_op_queue,&rmcp_op_out,pdMS_TO_TICKS(OpenIPMC_RMCP_SESSION_IDLE_MS_TIMEOUT)))
    {
      net_params = rmcp_op_out.net_params;

      switch(rmcp_op_out.type)
      {
        case LAN_IPMI_MSG:
        {
          rmcp_ipmi_msg_process(&rmcp_op_out.ipmi_in_msg,&output_msg);

          memcpy((void*)&rmcp_op_out.pkt.lan.payload_buffer,(void*)output_msg.data,output_msg.length);
          rmcp_op_out.pkt.lan.payload_length = output_msg.length;

          udp_out_buffer = (uint8_t *) &rmcp_op_out.pkt.lan;
          buffer_length  = rmcp_op_out.pkt_len + output_msg.length;
          break;
        }

        case LANPLUS_IPMI_MSG_OUTSIDE_SESSION:
        {
          rmcp_ipmi_msg_process(&rmcp_op_out.ipmi_in_msg,&output_msg);

          memcpy((void*)&rmcp_op_out.pkt.lanplus.payload_buffer,(void*)output_msg.data,output_msg.length);
          *((uint16_t *)rmcp_op_out.pkt.lanplus.payload_length) = output_msg.length;

          udp_out_buffer = (uint8_t *)&rmcp_op_out.pkt.lanplus;
          buffer_length  = rmcp_op_out.pkt_len + output_msg.length;
          break;
        }

        case LANPLUS_IPMI_MSG_IN_SESSION:
        {
          rmcp_ipmi_msg_process(&rmcp_op_out.ipmi_in_msg,&output_msg);
          memcpy((void*)&rmcp_op_out.pkt.lanplus.payload_buffer,(void*)output_msg.data,output_msg.length);
          *((uint16_t *)rmcp_op_out.pkt.lanplus.payload_length) = output_msg.length;

          udp_out_buffer = (uint8_t *)&rmcp_op_out.pkt.lanplus;
          buffer_length  = rmcp_op_out.pkt_len + output_msg.length;

          // Update the last packet information
          memcpy((void*)ipmi_session->last_pkt, (void*)udp_out_buffer,buffer_length);

          ipmi_session->last_pkt_size = buffer_length;

          /* Has the current session been invalidated by a close session command? */
          if(ipmi_session_invalid)
          {
            rmcp_terminate_ipmi_session();
          }

          break;
        }

        case LANPLUS_IPMI_MSG_REPEAT_PACKET:
        {
          // The packet has the same sequence number as the previous, retry sending the previous packet.
          udp_out_buffer = ipmi_session->last_pkt;
          buffer_length  = ipmi_session->last_pkt_size;
          break;
        }

        case LANPLUS_OPEN_SESSION:
        {
          udp_out_buffer = (uint8_t *)&rmcp_op_out.pkt.lanplus;
          buffer_length  = rmcp_op_out.pkt_len;
          break;
        }

        case LANPLUS_RAKP_EXCHANGE:
        {
          udp_out_buffer = (uint8_t *) &rmcp_op_out.pkt.lanplus;
          buffer_length  = rmcp_op_out.pkt_len;
          break;
        }
      default:
        break;
    }
    rmcp_callbacks->rmcp_udp_send_packet(net_params,udp_out_buffer,buffer_length);
   }

   /* Session Watchdog */
   if (pdTRUE == xQueueReceive(rmcp_watchdog_queue,&tickupdate,0UL))
     tickref = tickupdate;

   if( RMCP_ONGOING_SESSION == rmcp_session_status)
   {
     if(maxticks < ( osKernelGetTickCount() - tickref))
     {
       // Session has been idle for longer than the timeout specified. Terminate it.
       if(ipmi_session->payload_inst_mask & RMCP_IPMI_PLD_T_SOL)
       {
         sol_deactivate();
       }
       ipmi_session_invalid = 1;
       rmcp_callbacks->rmcp_event_send(RMCP_PLUS_SESSION_TERMINATED_CLIENT_UNRESPONSIVE);
       rmcp_terminate_ipmi_session();
     }
   }
  }
}
