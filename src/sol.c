 /*
 *  OpenIPMC-FW
 *  Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  @file   sol.c
 *  @brief  Serial Over LAN source code.
 *  @author Antonio Vitor Grossi Bassi
 *  @date   2023 - 05 - 22 : YYYY - MM - DD
 *
 */

/* standard C */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

/* FreeRTOS */
#include "FreeRTOS.h"
#include "semphr.h"
#include "stream_buffer.h"
#include "queue.h"

/* CMSIS */
#include "cmsis_os.h"

/* Serial Over LAN */
#include "rmcp.h"
#include "sol.h"


#define KB (1024UL)
#define MB (KB*KB)

#define SOL_LSB_POS (0b00000001)

#define SOL_REMOTE_STATUS_NACK      (SOL_LSB_POS << 6UL)
#define SOL_REMOTE_STATUS_RING      (SOL_LSB_POS << 5UL)
#define SOL_REMOTE_STATUS_BREAK     (SOL_LSB_POS << 4UL)
#define SOL_REMOTE_STATUS_CTS       (SOL_LSB_POS << 3UL)
#define SOL_REMOTE_STATUS_DCD       (SOL_LSB_POS << 2UL)
#define SOL_REMOTE_STATUS_FLUSH_IN  (SOL_LSB_POS << 1UL)
#define SOL_REMOTE_STATUS_FLUSH_OUT (SOL_LSB_POS << 0UL)

#define SOL_BMC_STATUS_NACK                 (SOL_REMOTE_STATUS_NACK)
#define SOL_BMC_STATUS_CHAR_TX_UNAVAILABLE  (SOL_LSB_POS << 5UL)
#define SOL_BMC_STATUS_DEACTIVATION         (SOL_LSB_POS << 4UL)
#define SOL_BMC_STATUS_TX_OVERRUN           (SOL_LSB_POS << 3UL)
#define SOL_BMC_STATUS_BREAK                (SOL_LSB_POS << 2UL)

#define SOL_CTRLSIG_INIT_PENDING_Msk    (SOL_LSB_POS << 0UL)
#define SOL_CTRLSIG_PLD_DEACTIVATE_Msk  (SOL_LSB_POS << 1UL)
#define SOL_CTRLSIG_TX_SUSPEND_Msk      (SOL_LSB_POS << 2UL)
#define SOL_CTRLSIG_FLUSH_OUT_Msk       (SOL_LSB_POS << 3UL)

#define SOL_SET_CTRLSIG_BIT(reg, mask)    (reg = ((reg & ~(mask)) | mask))
#define SOL_CLEAR_CTRLSIG_BIT(reg, mask)  (reg &= ~(mask))
#define SOL_RD_CTRLSIG(reg, mask)         (reg & mask)?(1UL):(0UL)
#define SOL_RESET_CTRLSIG(reg, mask)      (reg = (reg & 0UL) | (SOL_CTRLSIG_INIT_PENDING_Msk))

#define SOL_STREAM_LENGTH     (SOL_CHARACTER_BUFFER_LENGTH)
#define STREAM_TRIGGER_BYTES  (SOL_STREAM_LENGTH - 1UL)

#define SOL_MUTEX_TIMEOUT_MS       (1UL)
#define SOL_CHAR_STREAM_TIMEOUT_MS (50UL)
#define SOL_QUEUE_TIMEOUT_MS       (1UL)

typedef struct
{
  uint16_t      length;
  sol_payload_t payload   ;
}queue_wrapper_t;

static uint32_t          sol_mutex_timeout_ticks = pdMS_TO_TICKS(SOL_MUTEX_TIMEOUT_MS);
static SemaphoreHandle_t sol_mutex         = NULL;

static uint32_t             sol_char_stream_timeout_ticks = pdMS_TO_TICKS(SOL_CHAR_STREAM_TIMEOUT_MS);
static StreamBufferHandle_t sol_char_stream   = NULL;

static uint32_t      sol_queue_timeout_ticks = pdMS_TO_TICKS(SOL_QUEUE_TIMEOUT_MS);
static QueueHandle_t sol_inbound_queue = NULL;
static QueueHandle_t sol_outbound_queue = NULL;

static sol_callbacks_t* sol_callbacks;


/* mutex semaphore */
static void __sol_FreeRTOS_mutex_create(void);
static void __sol_FreeRTOS_unlock_mutex(void);
static int  __sol_FreeRTOS_lock_mutex(void);

/* Stream buffer */
static int __sol_FreeRTOS_stream_buf_create(void);
static unsigned int __sol_FreeRTOS_stream_buf_send(void *data, uint16_t length);
static unsigned int __sol_FreeRTOS_stream_buf_recv(void *bufp, uint16_t length);
static unsigned int __sol_FreeRTOS_stream_buf_send_from_isr(void *data, uint16_t length);
static unsigned int __sol_FreeRTOS_stream_buf_recv_from_isr(void *bufp, uint16_t length);

/* Queue */
static int __sol_FreeRTOS_create_queues(void);
static int __sol_FreeRTOS_enqueue_inbound(void *item);
static int __sol_FreeRTOS_dequeue_inbound(void *bufp);
static int __sol_FreeRTOS_enqueue_outbound(void *item);
static int __sol_FreeRTOS_dequeue_outbound(void *bufp);

/* USART port */
uint8_t usart1_char_input;
static unsigned int uart_init_periph_fn(void);

static void __sol_FreeRTOS_mutex_create(void)
{
  sol_mutex = xSemaphoreCreateMutex();
  return;
}

static void __sol_FreeRTOS_unlock_mutex(void)
{
  xSemaphoreGive(sol_mutex);
  return;
}

static int 	__sol_FreeRTOS_lock_mutex(void)
{
  return (pdTRUE == xSemaphoreTake(sol_mutex, sol_mutex_timeout_ticks));
}

static int __sol_FreeRTOS_stream_buf_create(void)
{
  sol_char_stream = xStreamBufferCreate((sol_config.send_threshold), (sol_config.send_threshold - 1UL));
  return (NULL != sol_char_stream);
}

static unsigned int __sol_FreeRTOS_stream_buf_send(void *data, uint16_t length)
{
  return (xStreamBufferSend(sol_char_stream, data, length, sol_char_stream_timeout_ticks));
}

static unsigned int __sol_FreeRTOS_stream_buf_recv(void *bufp, uint16_t length)
{
  return (xStreamBufferReceive(sol_char_stream, bufp, length, sol_char_stream_timeout_ticks));
}

static unsigned int __sol_FreeRTOS_stream_buf_send_from_isr(void *data, uint16_t length)
{
  static BaseType_t xHighPriorityTaskWoken = pdFALSE;
  const unsigned int n = xStreamBufferSendFromISR(sol_char_stream, data, length, &xHighPriorityTaskWoken);
  if(pdTRUE == xHighPriorityTaskWoken)
  {
    taskYIELD();
  }
  return n;
}

static unsigned int __sol_FreeRTOS_stream_buf_recv_from_isr(void *bufp, uint16_t length)
{
  static BaseType_t xHighPriorityTaskWoken = pdFALSE;
  const unsigned int n = xStreamBufferReceiveFromISR(sol_char_stream, bufp, length, &xHighPriorityTaskWoken);
  if(pdTRUE == xHighPriorityTaskWoken)
  {
    taskYIELD();
  }
  return n;
}

static unsigned int __sol_FreeRTOS_flush_stream_buf(void)
{
  static BaseType_t xReturnValue = pdTRUE;
  return (xReturnValue == xStreamBufferReset(sol_char_stream))?(1UL):(0UL);
}

static int __sol_FreeRTOS_create_queues(void)
{
  sol_inbound_queue = xQueueCreate(15UL, sizeof(queue_wrapper_t));
  sol_outbound_queue = xQueueCreate(15UL, sizeof(queue_wrapper_t));
  return ((NULL != sol_inbound_queue) && (NULL != sol_outbound_queue));
}

static int __sol_FreeRTOS_enqueue_inbound(void *item)
{
  return (pdTRUE == xQueueSend(sol_inbound_queue, item, sol_queue_timeout_ticks));
}

static int __sol_FreeRTOS_dequeue_inbound(void *bufp)
{
  return (pdTRUE == xQueueReceive(sol_inbound_queue, bufp, sol_queue_timeout_ticks));
}

static int __sol_FreeRTOS_enqueue_outbound(void *item)
{
  return (pdTRUE == xQueueSend(sol_outbound_queue, item, sol_queue_timeout_ticks));
}

static int __sol_FreeRTOS_dequeue_outbound(void *bufp)
{
  return (pdTRUE == xQueueReceive(sol_outbound_queue, bufp, sol_queue_timeout_ticks));
}

static unsigned int uart_init_periph_fn( void )
{
  return sol_callbacks->sol_uart_init();
}

static unsigned int uart_tx_char_payload(uint8_t *payload, uint16_t length)
{
  return sol_callbacks->sol_uart_tx(payload,length);
}


static unsigned int sol_init_is_pending = 1UL;

sol_config_t sol_config =
{
    .in_pld_size     = SOL_STREAM_LENGTH,
    .out_pld_size    = SOL_STREAM_LENGTH,
    .baudrate        = 115200UL,
    .char_acc_interv = 16UL,
    .send_threshold  = SOL_STREAM_LENGTH,
    .ms_retry_interv = 40UL,
    .retry_count     = 5UL,
    .use_fifo        = 0UL,
    .use_flow_ctl    = 0UL
};

sol_t sol_instance =
{
    .net_params               = {0},
    .remote_console_id        = 0UL,
    .ipmi_sequence            = 0UL,

    .stream_buf_create        = __sol_FreeRTOS_stream_buf_create,
    .stream_buf_send          = __sol_FreeRTOS_stream_buf_send,
    .stream_buf_recv          = __sol_FreeRTOS_stream_buf_recv,
    .stream_buf_send_from_isr = __sol_FreeRTOS_stream_buf_send_from_isr,
    .stream_buf_recv_from_isr = __sol_FreeRTOS_stream_buf_recv_from_isr,
    .stream_buf_flush_data    = __sol_FreeRTOS_flush_stream_buf
};

static int sol_udp_send(uint8_t *packet, uint16_t length)
{
  return sol_instance.sol_udp_send_packet(sol_instance.net_params,packet,length);;
}


void sol_get_config(sol_config_t** config)
{
  *config = &sol_config;
  return;
}


osThreadId_t sol_TxRx_task_handle;
const osThreadAttr_t sol_TxRx_task_attr =
{
  .name = "sol_TxRx_task",
  .priority = (osPriority_t)osPriorityHigh,
  .stack_size = 4UL*(128UL + 2UL*sizeof(queue_wrapper_t))
};

osThreadId_t sol_udp_task_handle;
const osThreadAttr_t sol_udp_task_attr =
{
    .name = "sol_udp_task",
    .priority = (osPriority_t)osPriorityAboveNormal,
    .stack_size = 4UL*(128UL + sizeof(rmcp_lanplus_t) + sizeof(queue_wrapper_t))
};

#define SOL_TXRX_CTRL_LSB              (0b00000001)
#define SOL_TXRX_CTRL_PENDING_ACK_Pos  (5UL)
#define SOL_TXRX_CTRL_PENDING_ACK_Msk  (SOL_TXRX_CTRL_LSB << SOL_TXRX_CTRL_PENDING_ACK_Pos)
#define SOL_TXRX_CTRL_SUSPEND_TX_Pos   (6UL)
#define SOL_TXRX_CTRL_SUSPEND_TX_Msk   (SOL_TXRX_CTRL_LSB << SOL_TXRX_CTRL_SUSPEND_TX_Pos)
#define SOL_TXRX_CTRL_300MS_BREAK_Pos  (7UL)
#define SOL_TXRX_CTRL_300MS_BREAK_Msk  (SOL_TXRX_CTRL_LSB << SOL_TXRX_CTRL_300MS_BREAK_Pos)
#define SOL_TXRX_CTRL_SEQ_OVERFLOW_Pos (4UL)
#define SOL_TXRX_CTRL_SEQ_OVERFLOW_Msk (SOL_TXRX_CTRL_LSB << SOL_TXRX_CTRL_SEQ_OVERFLOW_Pos)
#define SOL_TXRX_CTRL_SEQ_Msk          (0x1F)
void sol_TxRx_task(void *arg)
{
  /*
   * Control
   * |<--ctrl bits-->|<-- seq num -->|
   * +---+---+---+---+---+---+---+---+
   * | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
   * +---+---+---+---+---+---+---+---+
   *   ^   ^   ^   ^
   *   |   |   |   |___ Sequence number (seq num) overflow flag.
   *   |   |   |____ Pending Acknowledge hold enabled.
   *   |   |____ Suspend Tx enable.
   *   |____ 300 ms break enable.
   */
  queue_wrapper_t in = {0}, last = {0};
  uint32_t        tickref = 0UL;               // tick reference.
  uint16_t        acc_size = 0U;               // accumulated character size
  uint16_t        nchars = 0U;                 // number of characters.
  uint8_t         retries = 0U;
  uint8_t         ctrl = 0x01;

  const uint32_t retry_tick_timeout = pdMS_TO_TICKS(sol_config.ms_retry_interv);

  while(sol_init_is_pending)
  {
    // wait until resources are initialised.
    osDelay(pdMS_TO_TICKS(1UL));
  }

  while(1)
  {
    /* 1. Process received packets and transmit characters */
    if(__sol_FreeRTOS_dequeue_inbound((void *)&in))
    {
      // Did the remote console request a break?
      if( ctrl & SOL_TXRX_CTRL_300MS_BREAK_Msk )
      {
        osDelay(pdMS_TO_TICKS(300));
        ctrl &= ~(SOL_TXRX_CTRL_300MS_BREAK_Msk);
      }
      // ACK-Only packet?
      if(0 == in.payload.sequence)
      {
        // Did we receive a NACK?
        if(in.payload.status & SOL_REMOTE_STATUS_NACK)
        {
          if( (in.payload.ackd_sequence == last.payload.sequence) && (in.payload.ackd_char_count == 0) )
          {
            // Suspend ACK
            ctrl |= (SOL_TXRX_CTRL_SUSPEND_TX_Msk);
          }
        }
        else
        {
          if( (in.payload.ackd_sequence == last.payload.sequence) &&
              (in.payload.ackd_char_count == acc_size) )
          {
            // Completion ACK, Release packet transmission.
            ctrl &= ~(SOL_TXRX_CTRL_PENDING_ACK_Msk);
            acc_size = 0;
            nchars = 0;
          }
          else if( (in.payload.ackd_sequence == last.payload.sequence) &&
                   (in.payload.ackd_char_count < acc_size) )
          {
            // Partial ACK, send again.
            __sol_FreeRTOS_enqueue_outbound((void *)&last);
          }
          else if( (in.payload.ackd_sequence == last.payload.sequence) &&
                   (in.payload.ackd_char_count == 0 ) )
          {
            // Resume ACK
            ctrl &= ~(SOL_TXRX_CTRL_SUSPEND_TX_Msk);
          }
          else if((in.payload.ackd_sequence   == 0 ) &&
                  (in.payload.ackd_char_count == 0 ) )
          {
            // Control Packet
            if(in.payload.status & SOL_REMOTE_STATUS_FLUSH_OUT)
            {
              sol_instance.stream_buf_flush_data();
            }
            if(in.payload.status & SOL_REMOTE_STATUS_BREAK)
            {
              ctrl |= SOL_TXRX_CTRL_300MS_BREAK_Msk;
            }
          }
        }
      }
      else
      {
        if(0 == (ctrl & SOL_TXRX_CTRL_SUSPEND_TX_Msk))
        {
          if( (0UL == uart_tx_char_payload(in.payload.characters, (in.length - SOL_PAYLOAD_METADATA_LENGTH))) ||
               (0 > (in.length - SOL_PAYLOAD_METADATA_LENGTH)))
          {
            // Generate NACK-Only packet
            last.length = SOL_PAYLOAD_METADATA_LENGTH;
            last.payload.sequence = 0;
            last.payload.ackd_sequence = in.payload.sequence;
            last.payload.ackd_char_count = 0;
            last.payload.status |= SOL_BMC_STATUS_NACK;
            __sol_FreeRTOS_enqueue_outbound((void *)&last);
          }
          else
          {
            // Generate ACK-Only packet
            last.length = SOL_PAYLOAD_METADATA_LENGTH;
            last.payload.sequence = 0;
            last.payload.ackd_sequence = in.payload.sequence;
            last.payload.ackd_char_count = (in.length - SOL_PAYLOAD_METADATA_LENGTH);
            last.payload.status &= ~SOL_BMC_STATUS_NACK;
            __sol_FreeRTOS_enqueue_outbound((void *)&last);
          }
        }
      }
    }
    /* 2. Get characters from the receiver and enqueue outbound payload */
    // Has previous packet been acknowledged?
    if(0 == (ctrl & SOL_TXRX_CTRL_PENDING_ACK_Msk))
    {
      memset((void *)&last, 0, sizeof(queue_wrapper_t));
      do
      {
        nchars = sol_instance.stream_buf_recv((void *)&last.payload.characters[acc_size], (SOL_STREAM_LENGTH - acc_size));
        acc_size += nchars;
        if((0 == (SOL_STREAM_LENGTH - acc_size))||(0 == nchars))
        {
          if(0 < acc_size)
          {
            ctrl += SOL_TXRX_CTRL_LSB;
            if((ctrl & SOL_TXRX_CTRL_SEQ_OVERFLOW_Msk))
            {
              ctrl &= (~SOL_TXRX_CTRL_SEQ_Msk);
              ctrl += SOL_TXRX_CTRL_LSB;
            }
            last.payload.sequence = (ctrl & 0x0F);
            last.payload.status = 0;
            last.length = acc_size + SOL_PAYLOAD_METADATA_LENGTH;
            __sol_FreeRTOS_enqueue_outbound((void *)&last);
            ctrl |= SOL_TXRX_CTRL_PENDING_ACK_Msk;
            tickref = osKernelGetTickCount();
          }
          break;
        }
      }while(1);
    }
    else
    {
      // Check if retry timeout occurred.
      if(retry_tick_timeout < (osKernelGetTickCount() - tickref))
      {
        retries++;
        __sol_FreeRTOS_enqueue_outbound((void *)&last);
        tickref = osKernelGetTickCount();
        if(retries > sol_config.retry_count)
        {
          // If we exceed the retry limit, drop packet.
          acc_size = 0;
          nchars = 0;
          retries = 0;
          ctrl &= ~(SOL_TXRX_CTRL_PENDING_ACK_Msk);
        }
      }
    }
  }
  return;
}

void sol_udp_task(void *arg)
{
  rmcp_lanplus_t  rmcp = {0};
  queue_wrapper_t out  = {0};
  uint16_t        total_length = 0;

  while(sol_init_is_pending)
  {
    // wait until resources are initialised.
    osDelay(pdMS_TO_TICKS(1UL));
  }

  while(1)
  {
    if((__sol_FreeRTOS_dequeue_outbound((void *)&out)))
    {
      rmcp.version        = 0x06;
      rmcp.rsvd           = 0x00;
      rmcp.rmcp_sequence  = 0xFF;
      rmcp.msg_class      = 0x07;
      rmcp.auth_t         = 0x06;
      rmcp.payload_t      = 0x01;

      if(__sol_FreeRTOS_lock_mutex())
      {
        (*((uint32_t *)rmcp.session_id))      = sol_instance.remote_console_id;
        (*((uint32_t *)rmcp.ipmi_sequence))   = sol_instance.ipmi_sequence;
        (*((uint16_t *)rmcp.payload_length))  = out.length;
        __sol_FreeRTOS_unlock_mutex();
      }

      memcpy((void *)rmcp.payload_buffer, (void *)&out.payload, out.length);

      total_length = rmcp_header_size + rmcp_lanplus_ipmi_header_size + out.length;
      sol_udp_send((uint8_t *)&rmcp, total_length);

      memset((void *)&out, 0, sizeof(queue_wrapper_t));
    }
  }
  return;
}

int sol_deactivate(void)
{
  /* Check if client sent a payload deactivation command */
  osStatus udp_task_termination = osError;
  osStatus txrx_task_termination = osError;

  sol_init_is_pending = 1UL;

  // Terminate tasks
  udp_task_termination = osThreadTerminate(sol_TxRx_task_handle);
  txrx_task_termination = osThreadTerminate(sol_udp_task_handle);

  // Delete allocated system resources
  vStreamBufferDelete(sol_char_stream);
  vSemaphoreDelete(sol_mutex);
  vQueueDelete(sol_inbound_queue);
  vQueueDelete(sol_outbound_queue);

  sol_char_stream = NULL;
  sol_inbound_queue = NULL;
  sol_outbound_queue = NULL;
  sol_mutex = NULL;

  return ((osOK == udp_task_termination)&&(osOK == txrx_task_termination));
}

int sol_activate(rmcp_ip_net_params_t net_params, uint32_t remote_console_id, rmcp_udp_send_packet_t sol_udp_send_packet)
{
  unsigned int err = 0;
  if(sol_init_is_pending)
  {
    sol_instance.sol_udp_send_packet = sol_udp_send_packet;
    sol_instance.net_params          = net_params;
    sol_instance.remote_console_id   = remote_console_id;

    sol_char_stream_timeout_ticks = pdMS_TO_TICKS(sol_config.char_acc_interv);



    err = uart_init_periph_fn();
    if(0UL == err)
    {
      sol_TxRx_task_handle = osThreadNew(sol_TxRx_task, NULL, &sol_TxRx_task_attr);
      sol_udp_task_handle = osThreadNew(sol_udp_task, NULL, &sol_udp_task_attr);
      __sol_FreeRTOS_mutex_create();
      __sol_FreeRTOS_create_queues();
      sol_instance.stream_buf_create();
      // TODO: add uart flush
      sol_init_is_pending = 0UL;
    }
  }
  return err;
}

int openipmc_sol_impl_init(sol_callbacks_t* callbacks)
{
  sol_callbacks = callbacks;

  if ((sol_callbacks->sol_uart_init) == NULL)
    return -1;

  if ((sol_callbacks->sol_uart_tx) == NULL )
    return -1;

  // Initializes SOL UART peripheral to give a well defined logic values on the physical pins.
  sol_callbacks->sol_uart_init();
  return 1;
}

void sol_enqueue_payload(uint8_t *recv_rmcp_payload)
{
  queue_wrapper_t recv = {0};
  rmcp_lanplus_t  *rmcp = NULL;

  if(NULL != recv_rmcp_payload)
  {
    rmcp = (rmcp_lanplus_t *)recv_rmcp_payload;

    // Update ipmi sequence number from received packet and get payload length.
    __sol_FreeRTOS_lock_mutex();
    sol_instance.ipmi_sequence = (*((uint32_t *)rmcp->ipmi_sequence));
    __sol_FreeRTOS_unlock_mutex();

    recv.length = (*((uint16_t *)rmcp->payload_length));

    // Copy received sol payload into wrapper
    memcpy((void *)&recv.payload, (void *)rmcp->payload_buffer, recv.length);

    __sol_FreeRTOS_enqueue_inbound((void *)&recv);
  }
  return;
}

void openipmc_sol_uart_rx(uint8_t* data, uint32_t len)
{
  if(!sol_init_is_pending)
  {
    sol_instance.stream_buf_send_from_isr((void *)data, len);
  }
}







