/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file sel_entry_templates.c
 *
 * @authors Carlos Ruben Dell'Aquila
 *
 * @brief Template functions to create SEL entries according sensor types.
 *
 */


#include <string.h>
#include <stdint.h>

#include "sdr_definitions.h"
#include "sdr_manager.h"
#include "sel_entry_templates.h"


/*
 * Enum type with three possible values of sensor reading variation direction.
 */
typedef enum {
	EQUAL,
	HIGH,
	LOW
} sensor_reading_dir_var_t;

/*
 * Callback function to create SEL entry from sensor_reading values.
 */

sel_entry_status_t create_sel_entry_generic_analog_sensor_1( short unsigned int record_id, sensor_reading_t* sensor_reading, ipmi_sel_entry_t* ipmi_sel_entry )
{

  sdr_type_01_t* record = allrecords[record_id].ptr;
  sensor_reading_t prev_sensor_reading = sensor_reading_cache[record_id];
  sensor_event_info_t prev_sensor_event_info = sensor_event_info_cache[record_id];

  uint8_t  event_th;
  uint8_t  event_offset;
  uint16_t event_mask;

  sensor_reading_dir_var_t value_var_dir;

	// define the variation direction between present and previous value.
	int diff_raw_value = sensor_reading->raw_value - prev_sensor_reading.raw_value;

  if (diff_raw_value == 0)
  {
    value_var_dir = EQUAL;

  }
  else
  {
    if (diff_raw_value > 0) {
      value_var_dir = HIGH;
    }
    else
    {
      value_var_dir = LOW;
    }
  }

  // determine if the present and previous event are the same.
  if ( ( sensor_reading->present_state & prev_sensor_reading.present_state ) && ( value_var_dir == EQUAL ) )
    return SEL_NO_ENTRY; // It isn't a new event.


	// define if is a new assert OR desassert event.
	if (sensor_reading->present_state)
	{
		// Check if the corresponding event mask is active.
		switch (sensor_reading->present_state)
		{ // Define mask.

		  case UPPER_NON_RECOVERABLE:
			  event_mask = ASSERTION_EVENT_UPPER_NON_REC_TH_GOING_LOW_SUPPORTED;
			  event_offset = 0x0A;
			  event_th = record->upper_non_recoverable_threshold;
			  // The values of actual and past sensor reading could be the same but the thresholds values may have changed.
			  // In this contex an event should be created.
			  value_var_dir = value_var_dir == EQUAL ? HIGH : value_var_dir;
			break;

		  case UPPER_CRITICAL:
			  event_mask = ASSERTION_EVENT_UPPER_CRITICAL_TH_GOING_LOW_SUPPORTED;
			  event_offset = 0x08;
			  event_th = record->upper_critical_threshold;
			  value_var_dir = value_var_dir == EQUAL ? HIGH : value_var_dir;
			break;

		  case UPPER_NON_CRITICAL:
			  event_mask = ASSERTION_EVENT_UPPER_NON_CRIT_TH_GOING_LOW_SUPPORTED;
			  event_offset = 0x06;
			  event_th = record->upper_non_critical_threshold;
			  value_var_dir = value_var_dir == EQUAL ? HIGH : value_var_dir;
			break;

		  case LOWER_NON_RECOVERABLE:
			  event_mask = ASSERTION_EVENT_LOWER_NON_REC_TH_GOING_LOW_SUPPORTED;
			  event_offset = 0x04;
			  event_th = record->lower_non_recoverable_threshold;
			  value_var_dir = value_var_dir == EQUAL ? LOW : value_var_dir;
			break;

			case LOWER_CRITICAL:
			  event_mask = ASSERTION_EVENT_LOWER_CRITICAL_TH_GOING_LOW_SUPPORTED;
			  event_offset = 0x02;
			  event_th = record->lower_critical_threshold;
			  value_var_dir = value_var_dir == EQUAL ? LOW : value_var_dir;
			break;

		  case LOWER_NON_CRITICAL:
			  event_mask = ASSERTION_EVENT_LOWER_NON_CRIT_TH_GOING_LOW_SUPPORTED;
			  event_offset = 0x00;
			  event_th = record->lower_non_critical_threshold;
			  value_var_dir = value_var_dir == EQUAL ? LOW : value_var_dir;
		  break;

			default:
				return SEL_NO_ENTRY;
		}


    if ( value_var_dir == HIGH )
	  {
	    event_mask = event_mask << 1;
		  event_offset += 1;
    }

    if (!( event_mask & record->threshold_assertion_event_mask ))
	    return SEL_NO_ENTRY;

    // Fills event fields data specific for THRESHOLD sensor type.
    ipmi_sel_entry->event_dir_type = record->event_reading_type; // This field is set as asserted event.

    ipmi_sel_entry->event_data_1 = 0;
    ipmi_sel_entry->event_data_1 |= 0x40; // bits [7:6] = 01b : trigger reading in byte 2
    ipmi_sel_entry->event_data_1 |= 0x10; // bits [5:4] = 01b : trigger threshold value in byte 3
    ipmi_sel_entry->event_data_1 |= 0x0F & event_offset;

    ipmi_sel_entry->event_data_2 = sensor_reading->raw_value;
    ipmi_sel_entry->event_data_3 = event_th;

	}
  else
  {
	  // There is not event at present reading, however according to previous event could be a desassertion one.
	  if (( !(prev_sensor_event_info.event_dir_type & 0x80) ) && (prev_sensor_event_info.event_dir_type & 0x7F ))
	  {
      event_offset = prev_sensor_event_info.event_data_1 & 0x03;
      event_mask = 0x01 << event_offset;

			if (!( event_mask & record->threshold_deassertion_event_mask ))
        return SEL_NO_ENTRY;

      ipmi_sel_entry->event_dir_type = 0x80;
      ipmi_sel_entry->event_dir_type |= prev_sensor_event_info.event_dir_type & 0x7F;

      ipmi_sel_entry->event_data_1 = prev_sensor_event_info.event_data_1;
      ipmi_sel_entry->event_data_2 = prev_sensor_event_info.event_data_2;
      ipmi_sel_entry->event_data_3 = prev_sensor_event_info.event_data_3;

		}
	  else
	  {
      return SEL_NO_ENTRY;
		}
	}

  ipmi_sel_entry->record_id_lsb = (record->record_id & 0x00FF);
  ipmi_sel_entry->record_id_msb = ((record->record_id >> 8) & 0x00FF);
  ipmi_sel_entry->record_type = 0x02;

  ipmi_sel_entry->generator_id_1 = record->sensor_owner_id;
  ipmi_sel_entry->generator_id_2 = 0x00;
  ipmi_sel_entry->evm_rev = 0x04;
  ipmi_sel_entry->sensor_type = record->sensor_type;
  ipmi_sel_entry->sensor_number = record->sensor_number;

  // update SEL backup;
  prev_sensor_event_info.event_dir_type = ipmi_sel_entry->event_dir_type;
  prev_sensor_event_info.event_data_1 = ipmi_sel_entry->event_data_1;
  prev_sensor_event_info.event_data_2 = ipmi_sel_entry->event_data_2;
  prev_sensor_event_info.event_data_3 = ipmi_sel_entry->event_data_3;

  sensor_event_info_cache[record_id] = prev_sensor_event_info;

  return 	SEL_ENTRY;
}

