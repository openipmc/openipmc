/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmb_0.c
 *
 * @authors Alison Franca Queiroz da Costa
 * @authors Andre Muller Cascadan
 * @authors Carlos Ruben Dell'Aquila
 *
 * @brief  Management tasks for incoming and outgoing messages from IPMB-A and IPMB-B.
 *
 * @sa {@link ipmb_0.h }
 */

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#include "ipmb_0.h"
#include "ipmc_ios.h"
#include "ipmi_msg_router.h"

#define DELAY_1_SECOND  1000UL

// Maximun number of characters to represent a maximun size IPMI message in hexadecimal format, plus an space character.
#define DEBUG_MAX_STRING_SIZE_FOR_IPMI_MAX_SIZE  ( IPMI_MAX_MESSAGE_SIZE * 3 )


// IPMB address in 8bit format.
uint8_t ipmb_0_addr;

// Activate/deactivate debugging prints
_Bool ipmb_0_debug_flag = 0;

TaskHandle_t ipmb_0_msg_receiver_task_handle = NULL;
TaskHandle_t ipmb_0_msg_sender_task_handle = NULL;

// Print bytes for debug
static void debug_print(const char *prefix, uint8_t data[], int len)
{
  if(ipmb_0_debug_flag == 0) return;  //Debug is disabled

  else
  {
    char bytes[DEBUG_MAX_STRING_SIZE_FOR_IPMI_MAX_SIZE];
    char byte[3];
    int i;

    bytes[0] = 0;
    for (i=0; i<len; i++)
    {
      itoa(data[i], byte, 16);
      if(data[i] <= 0x0F)
        strcat(bytes, "0"); // add zero to left

      strcat(bytes, byte);
      strcat(bytes, " ");
    }
    ipmc_ios_printf("%s  %s\r\n", prefix, bytes);
  }
}


/*
 * Task to manage the incoming messages from IPMB-A and
 * IPMB-B.
 *
 * It is responsible for getting messages from both IPMB
 * channels and discriminate them as response or request.
 *
 */
void ipmb_0_msg_receiver_task( void *pvParameters )
{
  ipmi_msg_t received_data_A;
  ipmi_msg_t received_data_B;

  uint8_t read_buffer[IPMI_MAX_MESSAGE_SIZE];
  int len;
  int i;
  uint8_t header_checksum;

  // Wait until ipmc_ios be ready!
  while ( ipmc_ios_ready() != pdTRUE )
  {
    vTaskDelay( pdMS_TO_TICKS(100) );
  }

  // Enable the IPMB-0 Channel Input.
  while ( ipmi_router_channel_input_enable(IPMI_ROUTER_CHANNEL_IPMB_0) == IPMI_MSG_ROUTER_RET_NOT_INITIALIZED )
    vTaskDelay( pdMS_TO_TICKS(100) );

  for( ;; )
  {
    // Blocks until receive something or timeout
    ipmc_ios_ipmb_wait_input_msg();

    len = ipmc_ios_ipmba_read(read_buffer);
    header_checksum = read_buffer[0] + read_buffer[1] + read_buffer[2];

    if ( (len > 0) && (len <= IPMI_MAX_MESSAGE_SIZE) && (header_checksum == 0) )
    {
      received_data_A.channel = IPMI_ROUTER_CHANNEL_IPMB_0;
      received_data_A.length = len;

      for (i=0; i<len; i++)
      {
        received_data_A.data[i] = read_buffer[i]; // why not use memecpy?
      }

      // Check the NetFn (byte 1) to decide the proper queue for this message.
      // Even means Request, odd means Response. The LSB of NetFn is in bit 2.
      if( (received_data_A.data[1] & (1<<2)) == 0 )
    	ipmi_router_queue_req_post(&received_data_A);
      else
    	ipmi_router_queue_res_post(&received_data_A);

      debug_print( "IPMB-A Rcvd", received_data_A.data, received_data_A.length);
    }

    len = ipmc_ios_ipmbb_read(read_buffer);
    header_checksum = read_buffer[0] + read_buffer[1] + read_buffer[2];

    if ( (len > 0) && (len <= IPMI_MAX_MESSAGE_SIZE) && (header_checksum == 0) )
    {
      received_data_B.channel = IPMI_ROUTER_CHANNEL_IPMB_0;
      received_data_B.length  = len;

      for (i=0; i<len; i++)
      {
        received_data_B.data[i] = read_buffer[i];
      }

      // Check the NetFn (byte 1) to decide the proper queue for this message.
      // Even means Request, odd means Response. The LSB of NetFn is in bit 2.
      if( (received_data_B.data[1] & (1<<2)) == 0 )
        ipmi_router_queue_req_post(&received_data_B);
      else
        ipmi_router_queue_res_post(&received_data_B);

      debug_print( "IPMB-B Rcvd", received_data_B.data, received_data_B.length);
    }
  }
}

/*
 * Task to manage the outgoing messages to IPMB-A and
 * IPMB-B.
 *
 * It is responsible for send the messages though both
 * channels using the proper arbitration rule.
 */
void ipmb_0_msg_sender_task( void *pvParameters )
{
  ipmi_router_ret_code_t ipmi_router_ret;

  uint8_t read_addr;
  ipmi_msg_t rec_data;

  char round_robin_state = 'A'; // 'A' or 'B'
  const int max_attempts = 3;
  int attempt;

  // Wait until ipmc_ios be ready!
  while ( ipmc_ios_ready() != pdTRUE )
    vTaskDelay( pdMS_TO_TICKS(100) );

  // Set the IPMB address to the IPMB interfaces.
  // The Hardware Address is converted into 8bit format and stored in a global.
  read_addr = ipmc_ios_read_haddress();

  if (read_addr != HA_PARITY_FAIL)
    ipmb_0_addr = read_addr << 1; // Normal address
  else
    ipmb_0_addr = IPMB_ERROR_ADDR; // Failure address

  ipmc_ios_ipmb_set_addr(ipmb_0_addr);

  // Enable the IPMB-0 Channel Output.
  while (ipmi_router_channel_output_enable(IPMI_ROUTER_CHANNEL_IPMB_0) == IPMI_MSG_ROUTER_RET_NOT_INITIALIZED )
    vTaskDelay( pdMS_TO_TICKS(100) );

  for( ;; )
  {
    // Gets a message from queue to be transmitted
    ipmi_router_ret = ipmi_router_queue_out_get(IPMI_ROUTER_CHANNEL_IPMB_0,&rec_data);

    if ( ipmi_router_ret == IPMI_MSG_ROUTER_RET_OK )
    {
      int send_complete = pdFALSE;

      // Attempts to transmit
      for(attempt = 0; (attempt < max_attempts) && (send_complete == pdFALSE); attempt++)
      {
        if(round_robin_state == 'A') // IPMB-A
        {
          round_robin_state = 'B'; // swap channel

          if( ipmc_ios_ipmba_send(rec_data.data, rec_data.length) != IPMB_SEND_FAIL )
          {
            send_complete = pdTRUE;

            if (attempt == 0)
              debug_print( "IPMB-A Send", rec_data.data, rec_data.length);
            else
              debug_print( "IPMB-A Retry", NULL, 0); // Tells if this is a retry from IPMB-A
          }

        }
        else // IPMB-B
        {
          round_robin_state = 'A'; // swap channel

          if( ipmc_ios_ipmbb_send(rec_data.data, rec_data.length) != IPMB_SEND_FAIL )
          {
            send_complete = pdTRUE;

            if (attempt == 0)
              debug_print( "IPMB-B Send", rec_data.data, rec_data.length);
            else
              debug_print( "IPMB-B Retry", NULL, 0); // Tells if this is a retry from IPMB-B
          }
        }
      }

      // Announces transmission failure
      if(send_complete != pdTRUE)
        debug_print("IPMB-0 Transmission FAILED!\r\n", NULL, 0);
    }
  }
}


sensor_reading_status_t ipmb0_sensor_reading(sensor_reading_t* sensor_reading, sensor_thres_values_t* sensor_thres_values)
{
  // This sensor doesn't have associated threshold values so the sensor_thres_values parameter will be NULL.
  sensor_reading->raw_value = (1<<7) | (1<<3); // Local Control State, No failure (for both channels)

  sensor_reading->present_state = 1<<3; // Both enabled

  return SENSOR_READING_OK;
}
