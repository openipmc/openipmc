/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_netfn_app.c
 * 
 * @authors Alison Franca Queiroz da Costa
 * @authors Andre Muller Cascadan
 * @authors Bruno Augusto Casu
 * @authors Luigi Calligaris
 * @authors Carlos Ruben Dell'Aquila
 * 
 * @brief  Response functions for Application Network Function commands.
 *
 * This file contains the specific functions to manage the requests of Application Network Function commands (defined in PICMG v3.0 and IPMI v1.5).
 * Each function returns the response data bytes and the completion code.
 * 
 * The specific functions are called by ipmi_msg_solve_request_ipmb0().    
 */

#include <stdint.h>
#include <string.h>
#include "ipmc_ios.h"
#include "device_id.h"
#include "ipmi_msg_router.h"
#include "ipmi_msg_manager.h"
#include "ipmi_completion_code_defs.h"
#include "fru_payload_ipmc_control.h"
#include "fru_state_machine.h"
#include "ipmc_self_test_result.h"
#include "rmcp.h"
#include "sol.h"


/**
 * @{
 * @name Application NetFn commands
 */

/**
 * @brief Specific function to provide a response for "Get Device ID" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_device_id( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  response_bytes[0] = 0x00;	// Device ID

  // Device Revision (Bit 7 set as 1: Device provides SDR)
  response_bytes[1] = (ipmc_device_id.device_revision & 0x0F) | 0x80;

  // Major Firmware Revision (Bit 7 set as 0: Device available: normal operation)
  response_bytes[2] = ipmc_device_id.firmware_major_revision & 0x7F;

  // Minor Firmware Revision (BCD)
  response_bytes[3] = ipmc_device_id.firmware_minor_revision;

  //response_bytes[4] = 0x51;   // IPMI Version v1.5 (bits 7:4 holds the Least Significant digit, bits 3:0 holds the Most Significant digit)

  response_bytes[4] = 0x02;


  // Additional Device Support
  response_bytes[5] = ipmc_device_id.device_support;

  // Manufacturer ID
  response_bytes[6] = (ipmc_device_id.manufacturer_id    )&0xFF;
  response_bytes[7] = (ipmc_device_id.manufacturer_id>>8 )&0xFF;
  response_bytes[8] = (ipmc_device_id.manufacturer_id>>16)&0xFF;

  // Product ID
  response_bytes[9]  = (ipmc_device_id.product_id   )&0xFF;
  response_bytes[10] = (ipmc_device_id.product_id>>8)&0xFF;

  // Auxiliary Firmware Revision
  response_bytes[11] = ipmc_device_id.auxiliar_firmware_rev_info[0];
  response_bytes[12] = ipmc_device_id.auxiliar_firmware_rev_info[1];
  response_bytes[13] = ipmc_device_id.auxiliar_firmware_rev_info[2];
  response_bytes[14] = ipmc_device_id.auxiliar_firmware_rev_info[3];

  *res_len = 15;
  *completion_code = 0; //OK
}

/**
 * @brief Specific function to call the implementation for and provide a response to the IPMI Payload "Cold Reset" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IPMI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_ipmc_cold_reset( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  do_begin_ipmc_cold_reset(completion_code);
}

/**
 * @brief Specific function to call the implementation for and provide a response to the IPMI Payload "Warm Reset" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IPMI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_ipmc_warm_reset( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  do_begin_ipmc_warm_reset(completion_code);
}

/**
 * @brief Specific function to call the implementation for and provide a response to the IPMI Payload "Get Self Test Results" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IPMI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_ipmc_self_test_results( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  // Refer to IPMI 1.5 chapter 17.4 or, alternatively, IPMI 2.0 chapter 20.4
  *res_len = 2;
  ipmc_self_test_get_results(completion_code, &(response_bytes[0]), &(response_bytes[1]));
}

/**
 * @brief Specific function to call the implementation for and provide a response to the IPMI Payload "Manufacturing Test On" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IPMI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_enable_manufacturing_test_mode( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  do_start_ipmc_manufacturing_test(completion_code);
}

/**
 * @brief Specific function to call the implementation for and provide a response to the IPMI Payload "Get Device GUID" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IPMI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_ipmc_device_guid( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  // Refer to IPMI 1.5 chapter 17.4 or, alternatively, IPMI 2.0 chapter 20.4
  *res_len = 16;
  impl_ipmc_get_device_guid(completion_code, response_bytes);
}


/**
 * @brief Specific function to call the implementation for and provide a response to the IPMI Payload "Get Message Flags" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IPMI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 *
 */
void ipmi_cmd_get_message_flags(uint8_t channel_origin, uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
  int msg_waiting;
  ipmi_router_queue_bridge_res_check(channel_origin, &msg_waiting);

  response_bytes[0] = 0x00;
  *res_len = 1;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;

  if(msg_waiting)
  {
    response_bytes[0] = 0x01; // Receive Message Available. One or mode messages ready for reading from Receive Message Queue.
  }
}


/**
 * @brief Specific function to call the implementation for and provide a response to the IPMI Payload "Get Message" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IPMI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 *
 */
void ipmi_cmd_get_message(ipmi_channel_enum_t channel_origin, uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
  ipmi_router_ret_code_t ret;
  ipmi_msg_t data;
  uint8_t checksum;

  ret = ipmi_router_queue_bridge_res_get(channel_origin,&data);

  switch(ret)
  {
    case IPMI_MSG_ROUTER_RET_TIMEOUT:
      *completion_code = IPMI_COMPL_CODE_TIMEOUT;
      *res_len = 0;
      return;
    case IPMI_MSG_ROUTER_RET_OK:
    default:
      *completion_code = IPMI_COMPL_CODE_SUCCESS;
      break;
  }

  // The contents of the Message Data field for GET MESSAGE response is according to Channel Type and Channel protocol
  // that was used to place the message in the Receive Message Queue, based on IPMI spec. 1v5 and 2v0;

  data.data[0] = data.channel;                              // Channel number field is added following the logic of SIPL library
                                                            // but the IPMI specification does not say anything.

                                                            // IPMI 2.0, Table 22, foot note 1: the value 0x20 shall always
  checksum =  (~(0x20 + data.data[0] + data.data[1])) + 1;  // be used for the checksum calculation for the receive message
                                                            // queue data whenever IPMB is listed as the originating bus and
                                                            //  with IPMB as the Channel Protocol.
  data.data[2] = checksum;

  *res_len = data.length;
  memcpy(&response_bytes[0],&data.data,(int) data.length);
}


/**
 * @brief Specific function to call the implementation for and provide a response to the IPMI Payload "Send Message" command.
 *
 * @param channel_origin  Channel identification where the request is coming from.
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IPMI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_send_message(ipmi_channel_enum_t channel_origin, uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  bridge_ipmi_msg_t data_to_bridge;
  ipmi_router_ret_code_t ret;

  data_to_bridge.channel_origin   = channel_origin;
  data_to_bridge.ipmi_msg.channel = request_bytes[0] & 0x0F; // [3:0] channel number to send message to.
  data_to_bridge.ipmi_msg.length  = req_len - 1;

  memcpy(&data_to_bridge.ipmi_msg.data,&request_bytes[1],data_to_bridge.ipmi_msg.length);

  *res_len = 0;

  ret = ipmi_router_queue_bridge_req_post(&data_to_bridge);

  switch(ret)
  {
    case IPMI_MSG_ROUTER_RET_TIMEOUT:
      *completion_code = IPMI_COMPL_CODE_TIMEOUT;
      break;

    case IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT:
      *completion_code = IPMI_COMPL_CODE_DESTINATION_UNAVAILABLE;
      break;

    case IPMI_MSG_ROUTER_RET_OK:
    default:
      *completion_code = IPMI_COMPL_CODE_SUCCESS;
      break;
  }
}


/**
 * @brief Specific function to call the implementation for and provide a response to the IPMI Payload "Get Channel Authentication Capabilities" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IPMI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_channel_auth_capabilities(uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
  uint8_t const ipmiv_ext = ( request_bytes[0] & 0x80 ) >> 7;
  uint8_t const ch_num    = ( request_bytes[0] & 0x0F );
  uint8_t const priv_lvl  = ( request_bytes[1] & 0x0F );

  uint32_t const channel_auth_cpblts = rmcp_get_channel_auth_cap();

  *res_len = 0;

  if( priv_lvl < RMCP_IPMI_PRIV_OEM_LVL )
  {
    *completion_code = IPMI_COMPL_CODE_SUCCESS;

    response_bytes[0] = (uint8_t)((channel_auth_cpblts & 0xFF000000) >> 24);
    response_bytes[1] = (uint8_t)((channel_auth_cpblts & 0x00FF0000) >> 16);
    response_bytes[2] = (uint8_t)((channel_auth_cpblts & 0x0000FF00) >> 8 );
    response_bytes[3] = (uint8_t)((channel_auth_cpblts & 0x000000FF) >> 0 );

    if ( ipmiv_ext == 0 )
    {
      /* If client requested backwards compatibility mode with IPMIv1.5
       * just clear bit 7 from byte 3 and bit 5 from byte 4 and redefine 5th byte.
       */
      response_bytes[1] &= ~0x80; // Clear IPMI v2.0+ extended capabilities.
      response_bytes[2] &= ~0x20; // Clear Kg authentication status.
      response_bytes[3] =   0x01; // We support backwards compatibility mode.
    }

    // No OEM Authentication available
    response_bytes[4] = 0x00;
    response_bytes[5] = 0x00;
    response_bytes[6] = 0x00;
    response_bytes[7] = 0x00;

    *res_len = 8;
  }
  else
  {
    *completion_code = IPMI_COMPL_CODE_CANNOT_EXEC_CMD_NOT_SUPPORTED_IN_STATE;
    *res_len = 0;
  }
  return;
}


/**
 * @brief Specific function to call the implementation for and provide a response to the IPMI Payload "Set Session Privilege Level" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IPMI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_set_session_privilege_level(uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
  rmcp_ipmi_priv_t priv = (rmcp_ipmi_priv_t)(request_bytes[0] & 0x0F);
  rmcp_ipmi_priv_t channel_privilege = rmcp_get_channel_privilege();

  rmcp_ipmi_session_handle_t *ipmi_session;

  rmcp_session_handle_get(&ipmi_session);

  *res_len = 0;
  *completion_code = IPMI_COMPL_CODE_INVALID_DATA_FIELD_IN_REQUEST;

  if( NULL != ipmi_session )
  {
    if( RMCP_IPMI_PRIV_OEM_LVL  == priv )
    {
      response_bytes[0] = ipmi_session->privilege;
      *res_len=1;
      *completion_code = IPMI_COMPL_CODE_PRIVILEGE_LEVEL_NOT_AVAILABLE;
     }
     else if( ( priv > channel_privilege ) || ( priv > ipmi_session->user->user_privilege_code ) )
     {
       response_bytes[0] = ipmi_session->privilege;
       *res_len = 1;
       *completion_code = IPMI_COMPL_CODE_PRIVILEGE_LEVEL_EXCEEDS_LIMIT;
     }
     else
     {
       rmcp_set_session_status(RMCP_ONGOING_SESSION);
       *res_len = 1;
       ipmi_session->privilege = priv;
       response_bytes[0] = priv;
       *completion_code = IPMI_COMPL_CODE_SUCCESS;
     }
  }
  return;
}


/**
 * @brief Specific function to call the implementation for and provide a response to the IPMI Payload "Close Session" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IPMI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_close_session(uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
  rmcp_ipmi_session_handle_t *ipmi_session;
  uint32_t session_id = 0;

  session_id = ( (((uint32_t)request_bytes[0]) << 0 ) & 0x000000FF )
             | ( (((uint32_t)request_bytes[1]) << 8 ) & 0x0000FF00 )
             | ( (((uint32_t)request_bytes[2]) << 16) & 0x00FF0000 )
             | ( (((uint32_t)request_bytes[3]) << 24) & 0xFF000000 );

  rmcp_session_handle_get(&ipmi_session);

  *res_len = 0;
  *completion_code = ( session_id == ipmi_session->id ) ? IPMI_COMPL_CODE_SUCCESS : IPMI_COMPL_CODE_INVALID_REQ_SESSION_HANDLE;

  if( IPMI_COMPL_CODE_SUCCESS == *completion_code )
  {
    rmcp_set_session_invalid(1UL);
  }
}

/**
 * @brief Specific function to call the implementation for and provide a response to the IPMI Payload "Get Channel Info" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IPMI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_channel_info(uint8_t channel_origin, uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
  uint8_t channel = (request_bytes[0] & 0x0F);
  uint8_t channel_type_number;
  rmcp_session_status_t status;

  if( 0x0E == channel )
  {
    channel = channel_origin;
  }
  response_bytes[0] = (channel & 0x0F); // actual channel number

  if( !(IPMI_MSG_ROUTER_RET_OK == ipmi_router_get_channel_medium_type_number(channel,&channel_type_number)))
  { // channel not present.
    *res_len = 0;
    *completion_code = IPMI_COMPL_CODE_CANNOT_RETURN_NREQUESTED_DATA_BYTES;
    return;
  }

  response_bytes[1] = (channel_type_number & 0x7F);   // 7-bit Channel Medium type (Table 6-3 v1.5)
  response_bytes[2] = 0x01; // 5-bit Channel IPMI Messaging Protocol Type (Table 6-2 v1.5)
                            // 0x01h IPMB-1.0, used for IPMB, serial/modem Basic Mode, and LAN.

  if ( 0x04 == channel_type_number )
  { // 802.3 LAN in openipmc that channel supports single-session.
    rmcp_get_session_status(&status);

    const unsigned active_session_count = (RMCP_NO_SESSION != status ) ? 1 : 0;
    response_bytes[3] = ( 0xC0 ) | ( active_session_count & 0x3F );
  }
  else
  {
    // channel is session less.
	  response_bytes[3] = 0x00;
  }

  // Vendor ID (IANA Enterprise Number).
  // Returns the IPMI IANA for IPMI-specification defined, non-OEM protocol type numbers other than OEM.
  // The IPMI Enterprise Number is 7154 (decimal) LSB first (0xF2, 0x1B, 0x00)
  response_bytes[4] = 0xF2;
  response_bytes[5] = 0x1B;
  response_bytes[6] = 0x00;

  // Auxiliary Channel Info
  // It is only for System Interface (0x0F) and OEM channel types. The field is reserved for other channel types.
  response_bytes[7] = 0x00;
  response_bytes[8] = 0x00;

  *res_len = 9;
  *completion_code = IPMI_COMPL_CODE_SUCCESS;
}


/**
 * @brief Specific function to call the implementation for and provide a response to the IPMI Payload "Activate Payload" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IPMI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_activate_payload(uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
  rmcp_ipmi_session_handle_t *ipmi_session = NULL;
  rmcp_callbacks_t           *callbacks = NULL;
  rmcp_ip_net_params_t        local_net_params;
  sol_config_t               *sol_config = NULL;

  uint8_t aux_req_data = request_bytes[2];
  *completion_code = IPMI_COMPL_CODE_INVALID_DATA_FIELD_IN_REQUEST;

  rmcp_session_handle_get(&ipmi_session);
  rmcp_callbacks_get(&callbacks);
  local_net_params = rmcp_get_local_net_params();
  sol_get_config(&sol_config);

  /* Serial Over LAN Payload */
  if( RMCP_IPMI_PLD_T_SOL == (0b00111111 & request_bytes[0]))
  {
    /* The session may have an ongoing SOL payload transmission */
    *completion_code = IPMI_COMPL_CODE_PAYLOAD_IN_ANOTHER_SESSION;

    if( 0 == (RMCP_IPMI_PLD_T_SOL & ipmi_session->payload_inst_mask))
    {
      /* No payload was active, free to activate it */
      *res_len = 12;

      /* Check if remote console is requesting authentication and encryption */
      *completion_code = IPMI_COMPL_CODE_SUCCESS;

      response_bytes[0] = 0x00;
      response_bytes[1] = 0x00;
      response_bytes[2] = 0x00;
      response_bytes[3] = (aux_req_data & 0b00100000) ? (0x01) : (0x00);
      response_bytes[4] = (uint8_t)((sol_config->in_pld_size  & 0x00FF) >> 0);
      response_bytes[5] = (uint8_t)((sol_config->in_pld_size  & 0xFF00) >> 8);
      response_bytes[6] = (uint8_t)((sol_config->out_pld_size & 0x00FF) >> 0);
      response_bytes[7] = (uint8_t)((sol_config->out_pld_size & 0xFF00) >> 8);
      response_bytes[8] = (uint8_t)((local_net_params.port & 0x00FF) >> 0);
      response_bytes[9] = (uint8_t)((local_net_params.port & 0xFF00) >> 8);
      response_bytes[10] = 0xFF;
      response_bytes[11] = 0xFF;

      sol_activate(ipmi_session->client_net_params, ipmi_session->remote_console_id, callbacks->rmcp_udp_send_packet);

      ipmi_session->payload_inst_mask |= RMCP_IPMI_PLD_T_SOL;
    }
  }

  return;
}


/**
 * @brief Specific function to call the implementation for and provide a response to the IPMI Payload "Deactivate Payload" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IPMI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_deactivate_payload(uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
  rmcp_ipmi_session_handle_t* ipmi_session;

  int flag = 0;

  rmcp_session_handle_get(&ipmi_session);

  *res_len = 0;
  *completion_code = IPMI_COMPL_CODE_PAYLOAD_TYPE_DISABLE;

  if(RMCP_IPMI_PLD_T_SOL == (0b00111111 & request_bytes[0])) // Move these macros to an rmcp_h file. I think is better to share
  {
    if(ipmi_session->payload_inst_mask & RMCP_IPMI_PLD_T_SOL)
    {
      flag = sol_deactivate();

      if (0 == flag)
      {
        // mt_printf("FreeRTOS error: Failed to kill tasks.");
    	// TODO: Send to RMCP notification system.
      }

      // Clear payload instance bit from mask
      ipmi_session->payload_inst_mask &= (~RMCP_IPMI_PLD_T_SOL);

      *completion_code = IPMI_COMPL_CODE_SUCCESS;
    }
    else
    {
	  *completion_code = IPMI_COMPL_CODE_PAYLOAD_IS_ALREADY_DEACTIVATE;
    }
  }
  else if (RMCP_IPMI_PLD_T_MSG == (0b00111111 & request_bytes[0]))
  {
    *completion_code = IPMI_COMPL_CODE_INVALID_DATA_FIELD_IN_REQUEST;
  }
}

///@}
