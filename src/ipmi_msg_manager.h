/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_msg_manager.h
 * 
 * @author Andre Muller Cascadan
 * 
 * @brief Management interface for messages transmitted and received through the IPMB.
 *
 * This file has the function to generate a request from the IPMC to Shelf manager, and retrieve the response bytes for this request.
 * Also, it contains the task that manages the requests from Shelf Manager to the IPMC, and solve the request, providing the response
 * bytes.
 */

#ifndef IPMI_MSG_MANAGER_H
#define IPMI_MSG_MANAGER_H

#include "ipmi_msg_router.h"

#define IPMI_MSG_SEND_OK                 0
#define IPMI_MSG_SEND_TIMEOUT            1
#define IPMI_MSG_SEND_ERROR              2
#define IPMI_MSG_SEND_CHANNEL_NO_PRESENT 3


/**
 * @brief Activate/deactivate IPMI messaging debugging prints
 *
 * This flag can be set or reset by the user
 */
extern _Bool ipmi_msg_debug_flag;

/**
 * @brief Function to compute the checksums field of an IPMI message
 *
 * @param data_ipmb pointer to IPMI message data
 * @param uint8_t pointer to send back the header checksum
 * @param uint8_t pointer to send back the data checksum
 *
 * @return void
 *
 * This function computes the IPMI message checksums fields according IPMI spec. 1v5.
 */
void ipmi_checksum_generator(ipmi_msg_t* data, uint8_t* checksum_header, uint8_t* checksum_data);

/**
 * @brief Function to generate an IPMI request using raw IPMI data bytes by different IPMI channels.
 *
 * @param uint8_t channel for sending the IPMI request.
 * @param data_ipmb pointer with IPMI request bytes.
 * @param data_ipmb pointer with IPMI response bytes.
 *
 * @return Return the status of the transmission ({@link IPMI_MSG_SEND_OK}, {@link IPMI_MSG_SEND_TIMEOUT} or {@link IPMI_MSG_SEND_ERROR}).
 *
 * The function sends an IPMI request from IMPI raw bytes as input parameter. The response is also sent back in raw bytes format.
 *
 * @note The function has a timer and waits for the response from shelf manager before resume the processing, in case of a fail, the function will return {@link IPMI_MSG_SEND_TIMEOUT}.
 */
int ipmi_msg_send_request_raw_data(ipmi_channel_enum_t channel, ipmi_msg_t* req_data, ipmi_msg_t* res_data);


/**
 * @brief Function to generate an IPMI request.
 *
 * @param uint8_t channel for sending the IPMI request.
 * @param netfn Byte which specifies the Network Function in the IPMI request.
 * @param command Byte which specifies the Command in the IPMI request.
 * @param request_bytes Data bytes in the IPMI request. The interpretation of the data bytes is done in the designated function to manage the response.
 * @param req_len Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param completion_code Byte returned by the response function to identify the Completion Code.
 * @param response_bytes Data bytes returned by the command specific function to respond Shelf Manager request.
 * @param res_len Number of bytes in the response returned.
 *
 * @return Return the status of the transmission  ({@link IPMI_MSG_SEND_OK}, {@link IPMI_MSG_SEND_TIMEOUT} or {@link IPMI_MSG_SEND_ERROR}).
 *
 * The function organizes an IPMI request using the parameters inserted, and redirects the message to the IPMB queue to be transmitted.
 * The OpenIPMC state machine uses this function to inform Shelf Manager the Hot Swap State transitions (definition in PICMG v3.0).
 *
 * @note The function has a timer and waits for the response from shelf manager before resume the processing, in case of a fail, the function will return {@link IPMI_MSG_SEND_TIMEOUT}.
 */
int ipmi_msg_send_request(uint8_t channel,
                          uint8_t   netfn,
                          uint8_t   command,
                          uint8_t  *request_bytes,
                          int  req_len,
                          uint8_t  *completion_code,
                          uint8_t  *response_bytes,
                          int *res_len         );





/**
 * @brief Function to solve the IPMI request returning the response bytes and completion code. 
 *
 * @param netfn Byte which specifies the Network Function in the IPMI request.
 * @param command Byte which specifies the Command in the IPMI request.
 * @param request_bytes Data bytes in the IPMI request. The interpretation of the data bytes is done in the designated function to manage the response.
 * @param req_len Number of data bytes in the IPMI request. The size of the request can be different depending on the command. 
 * @param completion_code Byte returned by the response function to identify the Completion Code.
 * @param response_bytes Data bytes returned by the command specific function to respond Shelf Manager request. 
 * @param res_len Number of bytes in the response returned.
 *
 * @return A value of 1 (TRUE) is returned if the request was solved (the response function is implemented).
 *         If the response for the command is not implemented the function returns 0 (FALSE).
 *
 * The purpose of this function is to solve the request sent by Shelf Manager and return the answer.
 *
 * The solve request function is implemented using Switch/Case to direct the IPMI request. The first Switch interprets the NetFn and 
 * a second Switch interprets the Command (definition in IPMI v1.5 and PICMG v3.0). In each Case a specific function is 
 * called to manage the request data bytes sent from Shelf Manager and return the response data bytes with the completion code.
 *
 * @sa ipmi_msg_manager_task().
 */
int ipmi_msg_solve_request (uint8_t channel_origin,
                            uint8_t   netfn,
                            uint8_t   command,
                            uint8_t  *request_bytes,
                            int  req_len,
                            uint8_t  *completion_code,
                            uint8_t  *response_bytes,
                            int *res_len         );



#endif
