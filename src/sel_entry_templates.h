/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file sel_entry_templates.h
 *
 * @authors Carlos Ruben Dell'Aquila
 *
 * @brief Interface template functions to create SEL entries
 *
 */


#ifndef SEL_ENTRY_TEMPLATES_H
#define SEL_ENTRY_TEMPLATES_H


/**
 * @brief Function to create SEL entry from sensor reading.
 *
 * @param short unsigned int it is the record Id.
 * @param sensor_reading_status_t to get the actual raw sensor measurement and its status.
 *
 * This function will be called by the sensor manager task to create the SEL message according to sensor reading status.
 */
sel_entry_status_t create_sel_entry_generic_analog_sensor_1( short unsigned int, sensor_reading_t*, ipmi_sel_entry_t*);


#endif // SEL_ENTRY_TEMPLATES_H
