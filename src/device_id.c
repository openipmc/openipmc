/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file device_id.c
 *
 * @author Andre Muller Cascadan
 *
 * @brief  This file contains the Device ID data struct, which identifies the
 * IPMC and its capabilities.
 */

#include "device_id.h"

ipmc_device_id_t ipmc_device_id = {0};
