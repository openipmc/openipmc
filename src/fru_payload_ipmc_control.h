/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

#ifndef OPENIPMC_FRU_PAYLOAD_IPMC_CONTROL_H_
#define OPENIPMC_FRU_PAYLOAD_IPMC_CONTROL_H_

#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////
// Functions implemented on the openipmc side and called by the user firmware //
////////////////////////////////////////////////////////////////////////////////

// This function allows to update the IPMC self-test result by the user firmware.
void ipmi_global_ctrl_update_self_test_result(uint8_t result);

//////////////////////////////////////////////////////////////////////
// Functions which *shall* be implemented on the user firmware side //
//////////////////////////////////////////////////////////////////////

void impl_ipmc_get_device_guid(uint8_t* compl_code, uint8_t* guid_16bytes);

// These functions implement the IPMC/FRU action.
void do_begin_ipmc_cold_reset(uint8_t* compl_code);
void do_begin_ipmc_warm_reset(uint8_t* compl_code);
void do_start_ipmc_manufacturing_test(uint8_t* compl_code);

void do_begin_payload_cold_reset(uint8_t* compl_code);
void do_begin_payload_warm_reset(uint8_t* compl_code);
void do_begin_payload_graceful_reboot(uint8_t* compl_code);
void do_issue_payload_diagnostic_int(uint8_t* compl_code);

#endif /* OPENIPMC_FRU_PAYLOAD_IPMC_CONTROL_H_ */
