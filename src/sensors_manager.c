/*************************************************************************************/
/*                                                                                   */
/* This Source Code Form is subject to the terms of the Mozilla Public               */
/* License, v. 2.0. If a copy of the MPL was not distributed with this               */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                          */
/*                                                                                   */
/*************************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file sensors_manager.c
 *
 * @author Carlos Ruben Dell'Aquila
 *
 * @brief Management task for sensor reading, System Event Log (SEL) and actions sensor related event.
 *
 */

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"

#include "stdint.h"
#include "ipmc_ios.h"

#include "sdr_manager.h"
#include "sdr_definitions.h"
#include "ipmi_sel_cmds.h"
#include "sel_entry_templates.h"

/**
 * Enumeration of the Cache sensor reading value.
 */
typedef enum
{
  NO_READ,
  NEW_READ
} cache_sensor_reading_status_t;


/*
 * This type is used by the get threshold function.
 */
typedef enum
{
  TH_NO,
  TH_PRESENT
} thres_in_record_t;

// External definitions
extern sensor_record_descriptor_t allrecords[N_RECORDS_MAX];
extern sensor_reading_t sensor_reading_cache[N_RECORDS_MAX];
extern sensor_reading_status_t (*sensor_manager_global_reading_callback)(short unsigned int, sensor_reading_t * );

// Public function
void sensors_manager_task ( void * );

// Private function
//thres_in_record_t get_sensor_threshold(short unsigned int, sensor_thres_values_t * );
thres_in_record_t get_sensor_threshold(short unsigned int, sensor_thres_values_t * );

// Public variables
TaskHandle_t sensor_manager_task_handle;

// Private variables
static cache_sensor_reading_status_t cache_sensor_reading_status[N_RECORDS_MAX];
static SemaphoreHandle_t sensor_manager_mutex;

/*
 * Sensor manager task
 */
void sensors_manager_task(void *pvParameters)
{
  sensor_reading_status_t sensor_reading_status;
  sel_entry_status_t sel_entry_status = SEL_NO_ENTRY;
  sensor_thres_values_t sensor_thres_values;
  sensor_reading_t sensor_reading;
  ipmi_sel_entry_t ipmi_sel_entry = {0};

  short unsigned int i;

  // Create sensor manager mutex
  sensor_manager_mutex = xSemaphoreCreateMutex();

  if (sensor_manager_mutex != NULL)
  {
    for (;;)
    {
      vTaskDelay(pdMS_TO_TICKS(1000)); // 1 second periodic task read the sensor value.

      xSemaphoreTake(sensor_manager_mutex, portMAX_DELAY);

      for (i = 0; i < N_RECORDS_MAX; ++i)
      {
        if (allrecords[i].type != EMPTY_RECORD)
        {

          if (allrecords[i].get_sensor_reading_cb == NULL)
            break;

          if ( get_sensor_threshold(i, &sensor_thres_values) == TH_PRESENT )
          {
            sensor_reading_status = allrecords[i].get_sensor_reading_cb(	&sensor_reading, &sensor_thres_values);
           }
           else
           {
             sensor_reading_status = allrecords[i].get_sensor_reading_cb(	&sensor_reading, NULL);
           }

          switch (sensor_reading_status)
          {
            case SENSOR_READING_OK:

              // Set default value for sel_entry_status
              sel_entry_status = SEL_NO_ENTRY;

              // call SEL message generator.
              if (allrecords[i].create_sel_msg != NULL)
                sel_entry_status = allrecords[i].create_sel_msg(i,&sensor_reading, &ipmi_sel_entry);

              // save sensor_reading result to cache
              // (NOTE: This should be done after reading value evaluation for SEL entry creation.)
              sensor_reading_cache[i] = sensor_reading;
              cache_sensor_reading_status[i] = NEW_READ;

              // send event entry to SEL in Shelf-Manager and execute sensor action callback if there is an event.
              if (sel_entry_status != SEL_NO_ENTRY)
              {
                ipmi_sel_add_entry(&ipmi_sel_entry);

                if (allrecords[i].sensor_action_req != NULL)
                  allrecords[i].sensor_action_req(&ipmi_sel_entry);
              }
            break;

          case SENSOR_READING_UNAVAILABLE:

            // Define the action to take in this case.
            cache_sensor_reading_status[i] = NO_READ;

           break;

          default:
            break;
          }
        } 
      }
      xSemaphoreGive(sensor_manager_mutex);
    } // end infinite-loop.
  }
}

/*
 * Sensor reading callback which will be used for Get Sensor Reading Command IPMI v1.5
 */
sensor_reading_status_t sensor_reading_callback(short unsigned int recordId, sensor_reading_t *sensor_reading)
{
  sensor_reading_status_t status = SENSOR_READING_UNAVAILABLE;

  if (sensor_manager_mutex != NULL)
  {

    xSemaphoreTake(sensor_manager_mutex, portMAX_DELAY);

    if (cache_sensor_reading_status[recordId] == NEW_READ)
    {
      *sensor_reading = sensor_reading_cache[recordId];
      status = SENSOR_READING_OK;
    }
    xSemaphoreGive(sensor_manager_mutex);
  }
 return status;
}


/*
 * This function is used to read the thresholds from Sensor Data Record to be used for the sersor_reading value.
 */
thres_in_record_t get_sensor_threshold(short unsigned int record_id, sensor_thres_values_t * thres_ptr)
{

  // Get record type
  uint8_t type = allrecords[record_id].type;

  sdr_type_01_t* record = allrecords[record_id].ptr;

  if ( type == FULL_SENSOR_RECORD )
  {

    switch (record->event_reading_type)
    {

      case EVENT_READING_THRESHOLD:

        thres_ptr->upper_non_recoverable_threshold = record->upper_non_recoverable_threshold;
        thres_ptr->upper_critical_threshold = record->upper_critical_threshold;
        thres_ptr->upper_non_critical_threshold =	record->upper_non_critical_threshold;
        thres_ptr->lower_non_recoverable_threshold = record->lower_non_recoverable_threshold;
        thres_ptr->lower_non_recoverable_threshold = record->lower_non_recoverable_threshold;
        thres_ptr->lower_critical_threshold =	record->lower_critical_threshold;
        thres_ptr->lower_non_critical_threshold =	record->lower_non_critical_threshold;

        return TH_PRESENT;

      case EVENT_READING_SPECIFIC:
      default:
        return TH_NO;
    }
  }
  return TH_NO;
}
