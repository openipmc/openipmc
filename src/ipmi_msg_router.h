/*************************************************************************************/
/*                                                                                   */
/* This Source Code Form is subject to the terms of the Mozilla Public               */
/* License, v. 2.0. If a copy of the MPL was not distributed with this               */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                          */
/*                                                                                   */
/*************************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file openipmc_ipmi_msg_router.h
 *
 * @author Carlos Ruben Dell'Aquila
 *
 * @brief Set of functions to use the IPMI command processors from different interfaces like IPMB-0, RMCP or Payload Interface.
 *
 */

#ifndef OPENIPMC_IPMI_MSG_ROUTER_H
#define OPENIPMC_IPMI_MSG_ROUTER_H

#include "openipmc_hal_config.h"
#include "queue.h"

/**
 * @name Macros with IPMI MESSAGE SIZE
 * @{
 */
#define IPMI_MSG_HEADER_SIZE                 8  // 8 = rqAddr + netFn(odd)/rqLUN + checksum1 + rsAddr + rqSeq/rsLUN + cmd + completion code + checksum2
#define IPMI_MSG_DATA_SIZE                   32 // data size in a IPMI message can be up to 32 bytes
#define IPMI_CHANNEL_NUMBER_FOR_BRIDGING_MSG 1  // Channel Number is a field present in a commands for bridging IPMI messages like 'Get Message' from IPMI 1v5 2V0.

// the IPMI MAX MESSAGE SIZE comes from the fact that an IPMI messages can encapsulates other one as it is needed in bridging messages.
#define IPMI_MAX_MESSAGE_SIZE (IPMI_MSG_HEADER_SIZE + IPMI_MSG_DATA_SIZE + IPMI_CHANNEL_NUMBER_FOR_BRIDGING_MSG)
///@}


/**
 * @name Timeout definition to add new data to a queue
 * @{
 */
#define IPMI_ROUTER_QUEUE_TIMEOUT_MS  250UL
#define IPMI_ROUTER_QUEUE_TIMEOUT_DIV 10UL
///@}


/**
 * @name Depth queues definitions
 * @{
 */
#define OPENIPMC_IPMI_ROUTER_QUEUE_IN_DEPTH         ( 5 )
#define OPENIPMC_IPMI_ROUTER_QUEUE_OUT_DEPTH        ( 5 )
#define OPENIPMC_IPMI_ROUTER_BRIDGE_QUEUE_REQ_DEPTH ( 1 )
#define OPENIPMC_IPMI_ROUTER_BRIDGE_QUEUE_RES_DEPTH ( 1 )
///@}


/**
 * @name Channel status values
 * @{
 */
#define OPENIPMC_IPMI_ROUTER_CHANNEL_OUTPUT_EN  0x01
#define OPENIPMC_IPMI_ROUTER_CHANNEL_INPUT_EN   0x02
///@}

/**
 * Type for channels enumeration
 *
 */
typedef enum
{
  IPMI_CHANNELS_ENUMERATION,
  NUMBER_OF_CHANNELS
} ipmi_channel_enum_t;

/*
 * Type to describe an IPMI MESSAGE that is transmitted by a channel
 */
typedef struct
{
  ipmi_channel_enum_t channel;
  int length;
  uint8_t data[IPMI_MAX_MESSAGE_SIZE];
} ipmi_msg_t;

/*
 * Type to describe an IPMI MESSAGE which is used to bridge data between channels.
 */
typedef struct
{
  ipmi_channel_enum_t channel_origin;
  ipmi_msg_t ipmi_msg;
} bridge_ipmi_msg_t;

/**
 * @name Amount of channels
 */
#define IPMI_MSG_ROUTER_AMOUNT_CHANNEL NUMBER_OF_CHANNELS

/*
 * Type to describe the main characteristics
 * and state of a channel.
 *
 */
typedef struct
{
  /* channel meta data */
  uint8_t channel_medium_type_number; // Table 6-3, Channel Medium Type Numbers, IPMI v1.5

  /* channel operational data fields */
  uint8_t channel_status;
  QueueHandle_t queue_ipmi_out;
  QueueHandle_t queue_ipmi_bridge_res;
  uint8_t out_req_seq_num;
  uint8_t in_req_seq_num;
} ipmi_router_channel_t;


/**
 * Enumeration of IPMI ROUTER QUEUE RET CODE
 *
 * Return code to get information about IPMI Router execution.
 */
typedef enum {
  IPMI_MSG_ROUTER_RET_OK,
  IPMI_MSG_ROUTER_RET_ERROR,
  IPMI_MSG_ROUTER_RET_TIMEOUT,
  IPMI_MSG_ROUTER_RET_NOT_INITIALIZED,
  IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT,
  IPMI_MSG_ROUTER_RET_CHANNEL_NOT_READY
} ipmi_router_ret_code_t;

/**
 * @brief Function to initialize the ipmi msg queues
 * @return Return the status of the function ({@link IPMI_ROUTER_QUEUE_RET_OK }, {@link IPMI_ROUTER_RET_ERROR }).
 *
 * This function initialize the queues used for process IPMI command processor.
 *
 */
ipmi_router_ret_code_t ipmi_router_init( void );


/**
 * @brief Function to verify if the IPMI ROUTER have been initialized.
 * @return Return the status of the router ({@link OPENIPMC_IPMI_ROUTER_RET_OK }, {@link IPMI_ROUTER_RET_NOT_INITIALIZED }).
 *
 * This function verifies if the ipmi_router_init( void ) have been executed.
 *
 */
ipmi_router_ret_code_t ipmi_router_is_initialized( void );



/**
 * @brief Function to Enable Channel Output
 * @return Return the status of the router ({@link OPENIPMC_IPMI_ROUTER_RET_OK }, {@link IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT }).
 *
 * This function should be called after the physical and logical initialization of the channel for output have be done.
 *
 */
ipmi_router_ret_code_t ipmi_router_channel_output_enable(ipmi_channel_enum_t channel);


/**
 * @brief Function to Disable Channel Output
 * @return Return the status of the router ({@link OPENIPMC_IPMI_ROUTER_RET_OK }, {@link IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT }).
 *
 * This function should be called before the physical and logical de-initialization of the channel for output have be done.
 *
 */
ipmi_router_ret_code_t ipmi_router_channel_output_disable(ipmi_channel_enum_t channel);


/**
 * @brief Function to Enable Channel Input
 * @return Return the status of the router ({@link OPENIPMC_IPMI_ROUTER_RET_OK }, {@link IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT }).
 *
 * This function should be called after the physical and logical initialization of the channel for output have be done.
 *
 */
ipmi_router_ret_code_t ipmi_router_channel_input_enable(ipmi_channel_enum_t channel);


/**
 * @brief Function to Disable Channel Output
 * @return Return the status of the router ({@link OPENIPMC_IPMI_ROUTER_RET_OK }, {@link IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT }).
 *
 * This function should be called before the physical and logical de-initialization of the channel for input have be done.
 *
 */
ipmi_router_ret_code_t ipmi_router_channel_input_disable(ipmi_channel_enum_t channel);



/**
 * @brief Function to verify in an IPMI Channel is available on the system.
 *
 * @param uint8_t with the channel number.
 * @return Return the status of the function ({@link IPMI_ROUTER_RET_OK }, {@link IPMI_MSG_ROUTER_RET_CHANNEL_NOT_READY} ,{@link IPMI_ROUTER_RET_CHANNEL_NOT_PRESENT }).
 *
 * The available channels are defined in the openipmc_ipmi_router_config.h file.
 */
ipmi_router_ret_code_t ipmi_router_channel_is_ready(ipmi_channel_enum_t channel);


/**
 * @brief Function to read the name of the channel as a string.
 *
 * @param data_ipmb pointer with data channel information.
 * @param pointer to char pointer to get back the string with the name.
 * @return Return the status of the function ({@link IPMI_ROUTER_RET_OK }, {@link IPMI_ROUTER_RET_CHANNEL_NOT_PRESENT }).
 *
 * The channels string name are defined in the openipmc_ipmi_router_config.h file.
 *
 */
ipmi_router_ret_code_t ipmi_router_channel_name_get(ipmi_msg_t* data, char** pname );


/**
 * @brief Funtion to add data to the IPMI request queue
 *
 * @param data_ipmb pointer to the data to add to the queue.
 * @return Return the status of the function ({@link IPMI_ROUTER_RET_OK }, {@link IPMI_ROUTER_RET_TIMEOUT }).
 *
 * This function adds a new data entry to the IPMI request queue
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_req_post(ipmi_msg_t* data);


/**
 * @brief Funtion to get data from request queue
 *
 * @param data_ipmb pointer to send back the queue output.
 * @return Return the status of the function ({@link IPMI_ROUTER_RET_OK }, {@link IPMI_ROUTER_RET_TIMEOUT }).
 *
 * To read data from request queue.
 */
ipmi_router_ret_code_t ipmi_router_queue_req_get(ipmi_msg_t* data);


/**
 * @brief Function to RESET the response queue
 *
 * @param void
 * @return Return the status of the function ({@link IPMI_MSG_ROUTER_RET_NOT_INITIALIZED}, {@link IPMI_MSG_ROUTER_RET_OK}).
 *
 * Function useful when a request will be sent and needs a response, for any channel.
 */
ipmi_router_ret_code_t ipmi_router_queue_res_reset( void );


/**
 * @brief Funtion to POST data to the IPMI response queue
 *
 * @param data_ipmb pointer to the data to add to the queue.
 * @return Return the status of the function ({@link IPMI_ROUTER_RET_OK }, {@link IPMI_ROUTER_RET_TIMEOUT }).
 *
 * This function adds a new data entry to the IPMI response queue
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_res_post(ipmi_msg_t* data);


/**
 * @brief Function to GET data from response queue
 *
 * @param data_ipmb pointer with channel number information and to send back the data response.
 * @return Return the status of the function ({@link IPMI_ROUTER_RET_OK }, {@link IPMI_ROUTER_RET_CHANNEL_NOT_PRESENT },{@link IPMI_ROUTER_RET_TIMEOUT }).
 *
 * The function waits to get any data from the specified channel.
 */
ipmi_router_ret_code_t ipmi_router_queue_res_get(ipmi_msg_t* pdata);

/**
 * @brief Funtion to send data to the IPMI out queue
 *
 * @param data_ipmb pointer to the data to add to the queue.
 * @return Return the status of the function ({@link IPMI_ROUTER_RET_OK }, {@link IPMI_ROUTER_RET_ERROR }).
 *
 * This function to send data to the IPMI out queue corresponding to each interface.
 */
ipmi_router_ret_code_t ipmi_router_queue_out_post(ipmi_msg_t* data);


/**
 * @brief Function to get data from IPMI out specific channel queue
 *
 * @param data_ipmb pointer to send back the data from the queue
 * @return Return the status of the function ({@link IPMI_ROUTER_RET_OK }, {@link IPMI_ROUTER_RET_CHANNEL_NOT_PRESENT }).
 *
 * The function the waits to get any data from specific channel output queue. The data is then sent by physical interface.
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_out_get(ipmi_channel_enum_t channel,ipmi_msg_t* data);

/**
 * @brief  Function to Set a specific channel's sequence number for incomming request
 *
 * @param data_ipmb pointer to message data.
 * @param uint8_t sequence number to send.
 *
 * @return Return the status of the function ({@link IPMI_ROUTER_RET_OK }, {@link IPMI_ROUTER_RET_CHANNEL_NOT_PRESENT }).
 *
 */
ipmi_router_ret_code_t ipmi_router_in_req_seq_number_set(ipmi_msg_t* data, uint8_t seq_number);


/**
 * @brief  Function to Get a specific channel's sequence number for incomming request
 *
 * @param data_ipmb pointer to message data.
 * @param uint8_t pointer sequence number to read.
 *
 * @return Return the status of the function ({@link IPMI_ROUTER_RET_OK }, {@link IPMI_ROUTER_RET_CHANNEL_NOT_PRESENT }).
 *
 */
ipmi_router_ret_code_t ipmi_router_in_req_seq_number_get(ipmi_msg_t* data, uint8_t* seq_number);


/**
 * @brief  Function to Set a specific channel's sequence number for outgoing request
 *
 * @param data_ipmb pointer to message data.
 * @param uint8_t sequence number to set
 *
 * @return Return the status of the function ({@link IPMI_ROUTER_RET_OK }, {@link IPMI_ROUTER_RET_CHANNEL_NOT_PRESENT }).
 *
 */
ipmi_router_ret_code_t ipmi_router_out_req_seq_number_set(ipmi_msg_t* data, uint8_t seq_number);


/**
 * @brief  Function to Get a specific channel's sequence number for outgoing request
 *
 * @param data_ipmb pointer to message data.
 * @param uint8_t  pointer to sequence number to get
 *
 * @return Return the status of the function ({@link IPMI_ROUTER_RET_OK }, {@link IPMI_ROUTER_RET_CHANNEL_NOT_PRESENT }).
 *
 */
ipmi_router_ret_code_t ipmi_router_out_req_seq_number_get(ipmi_msg_t* data, uint8_t* seq_number);


/**
 * @brief Function to POST a IPMI message request for bridgin messages between channels.
 *
 * @param bridge_data_ipmb0_t pointer to the message with channel origin and data_ipmb. The last data have the IPMI message with data and channel information.
 *
 * @return Return the status of the function ({@link IPMI_MSG_ROUTER_RET_OK}, {@link IPMI_MSG_ROUTER_RET_TIMEOUT}, {@link IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT})
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_bridge_req_post(bridge_ipmi_msg_t* data);


/**
 * @brief Function to GET a request IPMI message for bridging between channels.
 *
 * @param bridge_data_ipmb0_t pointer to the message with channel origin and data_ipmb. The last data have the IPMI message with data and channel information.
 *
 * @return Return the status of the function ({@link IPMI_MSG_ROUTER_RET_OK})
 */
ipmi_router_ret_code_t ipmi_router_queue_bridge_req_get(bridge_ipmi_msg_t* data);


/**
 * @brief Function to POST a response IPMI messages from bridging between channels.
 *
 * @param uint8_t with channel origin information that is who originates the bridge request.
 * @param data_ipmb pointer with IPMI message.
 *
 * @return Return status of the function ({@link IPMI_MSG_ROUTER_RET_OK}, {@link IPMI_MSG_ROUTER_RET_TIMEOUT})
 */
ipmi_router_ret_code_t ipmi_router_queue_bridge_res_post(ipmi_channel_enum_t channel_origin, ipmi_msg_t* data);


/**
 * @brief Function to GET a response IPMI messages for bridging messages between channels.
 *
 * @param uint8_t with channel origin information that is who originates the bridge request.
 * @param data_ipmb pointer with IPMI message.
 *
 * @return Return status of the function ({@link IPMI_MSG_ROUTER_RET_OK},{@link IPMI_MSG_ROUTER_RET_TIMEOUT})
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_bridge_res_get(ipmi_channel_enum_t channel_origin, ipmi_msg_t* data);


/**
 * @brief Function to CHECK if there is a IPMI response message waiting to be sent to other channel.
 *
 * @param uint8_t channel_origin where the request is coming from.
 * @param int pointer to send back the number of responses to sent as a result of a bridge request.
 *
 * @return Return status of the function ({@link IPMI_MSG_ROUTER_RET_OK}).
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_bridge_res_check(ipmi_channel_enum_t channel_origin, int* msg_waiting);


/**
 * @brief Function to GET the Channel Medium Type Numbers
 *
 * @param ipmi_channel_enum_t channel
 * @param data_ipmb pointer to send back the type number
 *
 * The function is used for Get Channel Info IPMI command in order to get the Channel Medium Type Numbers
 */
ipmi_router_ret_code_t ipmi_router_get_channel_medium_type_number(ipmi_channel_enum_t channel, uint8_t *type_number);

#endif // OPENIPMC_IPMI_MSG_ROUTER_H
