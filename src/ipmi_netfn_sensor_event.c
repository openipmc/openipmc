/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_netfn_sensor_event.c
 *
 * @authors Alison Franca Queiroz da Costa
 * @authors Andre Muller Cascadan
 * @authors Bruno Augusto Casu
 *
 * @brief  Response functions for Sensor Event (S/E) Network Function commands.
 *
 * This file contains the specific functions to manage the requests of Sensor and Event Network Function commands (definition in PICMG v3.0 and IPMI v1.5).
 * Each function returns the response data bytes and the completion code.
 *
 * The specific functions are called by ipmi_msg_solve_request_ipmb0().
 */

#include <string.h>
#include <stdint.h>

#include "sdr_manager.h"
#include "sensors_manager.h"
#include "ipmc_ios.h"

/**
 * @{
 * @name Sensor Event NetFn commands
 */

/**
 * @brief Specific function to provide a response for "Set Event Receiver" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_set_event_rec(uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len,
  uint8_t *completion_code)
{
  if (request_bytes[0] == 0x20 && request_bytes[1] == 0x00) // Check IPMB Slave Address and Event Receiver LUN
  {
    *res_len = 0;
    *completion_code = 0; // OK
  }
  else
  {
    *res_len = 0;
    *completion_code = 0xCC; // Invalid data field in Request
  }
}

/**
 * @brief Specific function to provide a response for "Reserve Device SDR Repository" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_reserve_device_sdr_repos(uint8_t request_bytes[], int req_len, uint8_t response_bytes[],  int *res_len,
  uint8_t *completion_code)
{
  response_bytes[0] = RESERVATION_ID_LSB; // Reservation ID (Least Significant Byte)
  response_bytes[1] = RESERVATION_ID_MSB; // Reservation ID (Most Significant Byte)
  *res_len = 2;
  *completion_code = 0; // OK
}

/**
 * @brief Specific function to provide a response for "Set Sensor Threshold" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_set_sensor_threshold(uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len,
  uint8_t *completion_code)
{
  const uint8_t sensor_number = request_bytes[0];
  const uint8_t sensor_req_thres = (0x3F & request_bytes[1]); // the requested sensor's threshold to change are specified by bits 5 to 0.
  const uint16_t record_id = get_record_id(sensor_number);

  if (record_id == RECORD_NOT_FOUND)
  {
    *completion_code = 0xCB; // Requested Sensor, data, or record not present.
    *res_len = 0;
  }
  else
  {
    *completion_code = 0x00;
    *res_len = 0;
  }

  if (allrecords[record_id].type == FULL_SENSOR_RECORD)
  {
    sdr_type_01_t *record = allrecords[record_id].ptr;
    const uint8_t settable_mask = record->settable_threshold_mask & 0x3F;

    if (sensor_req_thres & LOWER_NON_CRITICAL    & settable_mask) record->lower_non_critical_threshold    = request_bytes[2];
    if (sensor_req_thres & LOWER_CRITICAL        & settable_mask) record->lower_critical_threshold        = request_bytes[3];
    if (sensor_req_thres & LOWER_NON_RECOVERABLE & settable_mask) record->lower_non_recoverable_threshold = request_bytes[4];
    if (sensor_req_thres & UPPER_NON_CRITICAL    & settable_mask) record->upper_non_critical_threshold    = request_bytes[5];
    if (sensor_req_thres & UPPER_CRITICAL        & settable_mask) record->upper_critical_threshold        = request_bytes[6];
    if (sensor_req_thres & UPPER_NON_RECOVERABLE & settable_mask) record->upper_non_recoverable_threshold = request_bytes[7];
  }
  else
  {
    *completion_code = 0xCB; // Requested Sensor, data, or record not present.
    *res_len = 0;
  }
}

/**
 * @brief Specific function to provide a response for "Get Sensor Threshold" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_sensor_threshold(uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len,
  uint8_t *completion_code)
{
  const uint8_t sensor_number = request_bytes[0];
  const uint16_t record_id = get_record_id(sensor_number);

  if (record_id == RECORD_NOT_FOUND)
  {
    *completion_code = 0xCB; // Requested Sensor, data, or record not present.
    *res_len = 0;
  }

  response_bytes[1] = response_bytes[2] = response_bytes[3] = response_bytes[4] = response_bytes[5] = response_bytes[6] = 0;

  if (allrecords[record_id].type == FULL_SENSOR_RECORD)
  {
    sdr_type_01_t *record = allrecords[record_id].ptr;
    const uint8_t readable_mask = record->readable_threshold_mask & 0x3F;
    response_bytes[0] = readable_mask;

    if (readable_mask & LOWER_NON_CRITICAL   ) response_bytes[1] = record->lower_non_critical_threshold;
    if (readable_mask & LOWER_CRITICAL       ) response_bytes[2] = record->lower_critical_threshold;
    if (readable_mask & LOWER_NON_RECOVERABLE) response_bytes[3] = record->lower_non_recoverable_threshold;
    if (readable_mask & UPPER_NON_CRITICAL   ) response_bytes[4] = record->upper_non_critical_threshold;
    if (readable_mask & UPPER_CRITICAL       ) response_bytes[5] = record->upper_critical_threshold;
    if (readable_mask & UPPER_NON_RECOVERABLE) response_bytes[6] = record->upper_non_recoverable_threshold;

    *completion_code = 0x00;
    *res_len = 7;
  }
  else
  {
    *completion_code = 0xCB; // Requested Sensor, data, or record not present.
    *res_len = 0;
  }
}

/**
 * @brief Specific function to provide a response for "Get Device SDR" command.definition
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 * 
 * This function is part of the SDR management. As a Get Device SDR command is received this function calls get_sdr_data()
 * to access SDR repository and retrieve the requested data.
 * 
 * @sa get_sdr_data()
 */
void ipmi_cmd_get_device_sdr(uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
  int i;

  if (request_bytes[0] == RESERVATION_ID_LSB && request_bytes[1] == RESERVATION_ID_MSB) // Check reserve ID
  {
    uint8_t record_data_buff[64]; // Maximum size of a SDR is 64 bytes (definition in IPMI v1.5)

    size_t bytes_to_read = request_bytes[5];
    size_t const offset_in_record = request_bytes[4];
    uint8_t const record_id_ms = request_bytes[3];
    uint8_t const record_id_ls = request_bytes[2];
    uint16_t record_id = (((uint16_t) record_id_ms) << 8) + (((uint16_t) record_id_ls) << 0);

    // Case of first SDR
    if (record_id == 0) // "record_id = 0" means the first SDR is requested
    {
      while (allrecords[record_id].type == EMPTY_RECORD)
        record_id++; // Jumps to the next non empty DR after 0
    }

    // Protection against invalid Record ID
    if (allrecords[record_id].type == EMPTY_RECORD)
    {
      *res_len = 0;
      *completion_code = 0xCB; // Requested Sensor, data, or record not present.
      return;
    }

    // Access repository and load the requested segment into the buffer
    get_sdr_data(offset_in_record, bytes_to_read, record_id, record_data_buff);

    // Informs next Record ID
    uint16_t next_record_id;
    for (next_record_id = record_id + 1; next_record_id < N_RECORDS_MAX; next_record_id++)
    {
      if (allrecords[next_record_id].type != EMPTY_RECORD)
        break;
    }

    if (next_record_id == N_RECORDS_MAX) // If reaches the end ...
      next_record_id = 0xFFFF;    // Informs that there is no more records

    response_bytes[0] = (uint8_t) ((next_record_id & 0x00FF) >> 0); // Low  byte
    response_bytes[1] = (uint8_t) ((next_record_id & 0xFF00) >> 8); // High byte

    // Protections against querying more bytes than possible
    if (bytes_to_read > 22) // If more than IPMI command limit
    {
      *res_len = 0;
      *completion_code = 0xCA; // Can not return number of requested data bytes
      return;
    }

    for (i = 0; i < bytes_to_read; i++)
    {
      response_bytes[i + 2] = record_data_buff[i]; // Load response bytes from buffer
    }

    *res_len = bytes_to_read + 2;
    *completion_code = 0; // OK
  }
  else
  {
    *res_len = 0;
    *completion_code = 0xC5; // Reservation Canceled or Invalid Reservation ID
  }
}

/**
 * @brief Specific function to provide a response for "Get Device SDR Info" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_device_sdr_info(uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code)
{
  uint16_t n = 0;

  for (int aux = 0; aux < N_RECORDS_MAX; aux++)
  {
    if (allrecords[aux].type != EMPTY_RECORD)
    {
      n++;
    }
  }

  /*
  while (allrecords[aux].type != EMPTY) // Search for last record id
  {
  aux++; // number of the last record = number of sensors
  }
  */

  response_bytes[0] = n; // Number of Sensors in Device (counting the Management Controller)

  // Bitmap of response_bytes[1]:
  // [7]   0 = Static sensor population
  // [6:4] Reserved
  // [3]   1 = LUN 3 has sensors
  // [2]   1 = LUN 2 has sensors
  // [1]   1 = LUN 1 has sensors
  // [0]   1 = LUN 0 has sensors
  response_bytes[1] = 0x01;

  *res_len = 2;
  *completion_code = 0; // OK
}

/**
 * @brief Specific function to provide a response for "Get Sensor Reading" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_sensor_reading(uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len,
  uint8_t *completion_code)
{
  uint8_t sensor_number;
  uint16_t record_id;
  sensor_reading_t reading;
  sensor_reading_status_t sensor_reading_status;

  sensor_number = request_bytes[0];
  record_id = get_record_id(sensor_number);

  if ((record_id != RECORD_NOT_FOUND) && (allrecords[record_id].get_sensor_reading_cb != NULL))
  {
    sensor_reading_status = sensor_reading_callback(record_id, &reading);

    if (sensor_reading_status == SENSOR_READING_OK)
    {
      response_bytes[0] = reading.raw_value;
      response_bytes[1] = 0xC0; // Event messages and Sensor scanning are enabled
      response_bytes[2] = (uint8_t) (reading.present_state & 0x00FF); // Present State flags
      response_bytes[3] = (uint8_t) ((reading.present_state & 0xFF00) >> 8);
      *res_len = 4;
      *completion_code = 0;     // OK
    }
    else
    {
      *res_len = 0;
      *completion_code = 0xCE;
    }
  }
  else
  {
    *res_len = 0;
    *completion_code = 0xCB;     // Sensor does not exist
  }
}

/**
 * @brief Specific function to provide a response for "Set Sensor Event Enable" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_set_sensor_event_enable(uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len,
  uint8_t *completion_code)
{
  uint8_t sensor_number;
  uint16_t record_id;

  uint8_t actions;

  sdr_type_01_t *record;
  uint16_t thres_assertion_mask;
  uint16_t thres_desassertion_mask;

  sensor_number = request_bytes[0];
  record_id = get_record_id(sensor_number);

  if (record_id != RECORD_NOT_FOUND)
  {
    record = allrecords[record_id].ptr;

    *res_len = 0;
    *completion_code = 0x00;

    actions = (0x30 & request_bytes[1]) >> 4;

    thres_assertion_mask = ((0x0F) & request_bytes[3]) << 8;
    thres_assertion_mask |= request_bytes[2];

    thres_desassertion_mask = ((0x0F) & request_bytes[3]) << 8;
    thres_desassertion_mask |= request_bytes[4];

    switch (actions)
    {
      case 0x00: // do not change individual enables

        // assertion event.
        record->threshold_assertion_event_mask = thres_assertion_mask;

        // desassertion event.
        record->threshold_deassertion_event_mask = thres_desassertion_mask;
        break;

      case 0x01: // enable selected event messages

        // assertion event.
        record->threshold_assertion_event_mask |= thres_assertion_mask;

        // desassertion event.
        record->threshold_deassertion_event_mask |= thres_desassertion_mask;
        break;

      case 0x10: // disable selected event messages

        // assertion event.
        record->threshold_assertion_event_mask &= ~thres_assertion_mask;

        // desassertion event.
        record->threshold_deassertion_event_mask &=
            ~thres_desassertion_mask;
        break;

      default:
        *res_len = 0;
        *completion_code = 0xCC; // Invalid data field in Request.
        break;
    }
  }
  else
  {
    *res_len = 0;
    *completion_code = 0xCB;     // Sensor does not exist
  }

  return;
}

/**
 * @brief Specific function to provide a response for "Get Sensor Event Status" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_sensor_event_status(uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len,
  uint8_t *completion_code)
{
  uint8_t sensor_number;
  uint16_t record_id;

  sdr_type_01_t* record;

  sensor_number = request_bytes[0];
  record_id = get_record_id(sensor_number);

  if ( record_id != RECORD_NOT_FOUND )
  {
    record = allrecords[record_id].ptr;

    // Sensor scanning is enabled; Messages disabled
    response_bytes[0] = 0x40;

    response_bytes[1] = 0;
    response_bytes[2] = 0;
    response_bytes[3] = 0;
    response_bytes[4] = 0;

    *res_len = 5;
    *completion_code = 0; // OK
  }
  else
  {
    *res_len = 0;
    *completion_code = 0xCB; // Sensor does not exist
  }
}

///@}
