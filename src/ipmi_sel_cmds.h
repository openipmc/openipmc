/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_sel_cmd.h
 *
 * @author Carlos Ruben Dell'Aquila
 *
 * @brief Interface to send System Event Log (SEL) commands to Manager. In ATCA context the centralized SEL is the Self-Manager.
 *
 * The functions declared here are based on IPMI v1.5 and PICGMG 3.0 R3 AdavancedTCA Base Specification. The first one defines the command's fields.
 * The second one defines that the centralized SEL is maintained by the Shelf Manager (ShM).
 *
 */


#ifndef IPMI_SEL_CMDS_H
#define IPMI_SEL_CMDS_H

typedef uint16_t ipmi_sel_reserve_id_t;
typedef uint16_t ipmi_sel_record_id_t;

typedef struct {
  uint16_t num_possible_alloc_unit;
  uint16_t alloc_unit_bytes;
  uint16_t free_alloc_units;
  uint16_t largest_free_alloc_units;
  uint16_t max_record_size_alloc_unit;
} ipmi_sel_allocation_info_t;


typedef enum {INITIATE_ERASE,GET_STATUS} ipmi_sel_op_clr_t;

// So far, the return codes for IPMI message transactions and the completion codes do not overlap.
// This enum is here to establish one place where the return codes for the IPMI SEL commands are defined.

enum {
  IPMI_SEL_RETCODE_OK         ,
  IPMI_SEL_RETCODE_MSG_TIMEOUT,
  IPMI_SEL_RETCODE_MSG_ERROR  ,
  IPMI_SEL_RETCODE_CCODE_ERROR
};


/**
 * @brief Function to get from SEL Manager the allocation information.
 *
 * @param ipmi_sel_allocation_info pointer where the retrieve data command will be saved.
 *
 * @return Return the status of the command ({@IPMI_SEL_RETCODE_OK}, {@link IPMI_SEL_RETCODE_MSG_TIMEOUT}, {@link IPMI_SEL_RETCODE_MSG_ERROR} or {@link IPMI_SEL_RETCODE_CCODE_ERROR}).
 *
 * The present function retrieve data to openIPMC from ShM about event space allocation in SEL.
 *
 * @note This function send IPMI message to Shelf-Manager by calling the ipmi_msg_send_request_ipmb0 function from openIPMC library.
 */
int ipmi_sel_get_allocation_info(ipmi_sel_allocation_info_t*, ipmi_sel_record_id_t);

/**
 * @brief Function to reserve the SEL before to send other commands.
 *
 * @param ipmi_sel_reserve_id_t pointer where the retrieve data command will be saved.
 *
 * @return Return the status of the command ({@link IPMI_SEL_RETCODE_OK}, {@link IPMI_SEL_RETCODE_MSG_TIMEOUT}, {@link IPMI_SEL_RETCODE_MSG_ERROR} or {@link IPMI_SEL_RETCODE_CCODE_ERROR}).
 *
 * The present command is used to set the present 'owner' of the SEL. The response is the Reservation ID that
 * it's needed to execute others related commands.
 *
 * @note This function send IPMI message to Shelf-Manager by calling the ipmi_msg_send_request_ipmb0 function from openipmc library.
 */
int ipmi_sel_reserve_sel( ipmi_sel_reserve_id_t* );

/**
 * @brief Function to get SEL entry
 *
 * @param ipmi_sel_entry_t pointer where the retrieve data command will be saved.
 * @param ipmi_sel_reserve_id_t keeps the ID of the requested entry.
 *
 * @return Return the status of the command ({@link IPMI_SEL_RETCODE_OK}, {@link IPMI_SEL_RETCODE_MSG_TIMEOUT}, {@link IPMI_SEL_RETCODE_MSG_ERROR} or {@link IPMI_SEL_RETCODE_CCODE_ERROR}).
 *
 * The present command is used to retrieve entries from the SEL.
 *
 * @note This function send IPMI message to Shelf-Manager by calling the ipmi_msg_send_request_ipmb0 function from openipmc library.
 */
int ipmi_sel_get_entry( ipmi_sel_entry_t*, ipmi_sel_reserve_id_t );

/**
 * @brief Function to add entry to SEL
 *
 * @param ipmi_sel_entry_t pointer to input data.
 *
 * @return Return the status of the command ({@link IPMI_SEL_RETCODE_OK}, {@link IPMI_SEL_RETCODE_MSG_TIMEOUT}, {@link IPMI_SEL_RETCODE_MSG_ERROR} or {@link IPMI_SEL_RETCODE_CCODE_ERROR}).
 *
 * This function is provided to add records to the SEL. The input argument is SEL entry and it returns the Record ID.
 *
 * @note This function send IPMI message to Shelf-Manager by calling the ipmi_msg_send_request_ipmb0 function from openipmc library.
 */
int ipmi_sel_add_entry( ipmi_sel_entry_t* );


/**
 * @brief Function to partial add entry to SEL
 *
 * @param ipmi_sel_entry_t pointer to input data.
 *
 * @return Return the status of the command ({@link IPMI_SEL_RETCODE_OK}, {@link IPMI_SEL_RETCODE_MSG_TIMEOUT}, {@link IPMI_SEL_RETCODE_MSG_ERROR} or {@link IPMI_SEL_RETCODE_CCODE_ERROR}).
 *
 * It is a version of the Add SEL Entry command that allows the records to incrementally added to the SEL.The Partial Add SEL Entry
 * command must be preceded by a Reserve SEL function.
 *
 * @note This function send IPMI message to Shelf-Manager by calling the ipmi_msg_send_request_ipmb0 function from openipmc library.
 */
int ipmi_sel_partial_add_entry( ipmi_sel_entry_t* );


#endif // IPMI_SEL_CMDS_H_


