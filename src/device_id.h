/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file device_id.h
 *
 * @author Andre Muller Cascadan
 *
 * @brief  This header file contains the Device ID data struct, which identifies the
 * IPMC and its capabilities.
 *
 * For more information about these fields, see IPMI 1.5: "Get Device ID" command and
 * "SDR type 12h"
 */

#ifndef DEVICE_ID_H
#define DEVICE_ID_H

#include <stdint.h>

/**
 * @name Device Support Capabilities
 * @anchor device_support
 * @{
 */
#define DEVICE_SUPPORT_BRIDGE                  0x40
#define DEVICE_SUPPORT_IPMB_EVENT_GENERATOR    0x20
#define DEVICE_SUPPORT_IPMB_EVENT_RECEIVER     0x10
#define DEVICE_SUPPORT_FRU_INVENTORY           0x08
#define DEVICE_SUPPORT_SEL                     0x04
#define DEVICE_SUPPORT_SENSOR_SDR_REPOSITORY   0x02
#define DEVICE_SUPPORT_SENSOR                  0x01
///@}


typedef struct
{
	uint8_t  device_revision;               ///< 4 bits (0 ~ 15)
	uint8_t  firmware_major_revision;       ///< 7 bits (0 ~ 127)
	uint8_t  firmware_minor_revision;       ///< BCD from 00 to 99. (Example: Ver. x.2.3 -> 0x23)
	uint8_t  device_support;                ///< See \ref device_support "Device Support Capabilities"
	uint32_t manufacturer_id;               ///< Manufacturer ID (IANA code). 20 bit number. The rest MSB must be 0. 0x000000 = unspecified. 0x0FFFFF = reserved
	uint16_t product_id;                    ///< Product ID. 0x0000 = unspecified. 0xFFFF = reserved
	uint8_t  auxiliar_firmware_rev_info[4]; ///< 4 bytes typically displayed in hex format.
	char*    device_id_string;              ///< Identification (e.g.: Name of the board carrying this IPMC). 16 characters maximum.

}ipmc_device_id_t;

extern ipmc_device_id_t ipmc_device_id;


#endif //DEVICE_ID_H
