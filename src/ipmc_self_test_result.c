/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

#include <stdint.h>
#include "ipmc_self_test_result.h"

// The value of these two bytes is described in IPMI 1.5 chapter 17.4
// or, alternatively, IPMI 2.0 chapter 20.4
uint8_t ipmc_self_test_result_compl = 0xC0; // At the beginning, set the completion as "Node Busy".
uint8_t ipmc_self_test_result_byte2 = 0x55; // No error. All Self Tests Passed.
uint8_t ipmc_self_test_result_byte3 = 0x00; // Must be zero for byte2 = 0x55.

inline void ipmc_self_test_set_results(uint8_t* completion, uint8_t* byte2, uint8_t* byte3)
{
  ipmc_self_test_result_compl = *completion;
  ipmc_self_test_result_byte2 = *byte2;
  ipmc_self_test_result_byte3 = *byte3;
}

inline void ipmc_self_test_get_results(uint8_t* completion, uint8_t* byte2, uint8_t* byte3)
{
  *completion = ipmc_self_test_result_compl;
  *byte2      = ipmc_self_test_result_byte2;
  *byte3      = ipmc_self_test_result_byte3;
}
