/*
 *  OpenIPMC-FW
 *  Copyright (C) 2020-2021 Andre Cascadan, Luigi Calligaris
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *  @file   sol.h
 *  @brief  Serial Over LAN header file containing function references and useful macro definitions.
 *  @author Antonio Vitor Grossi Bassi
 *  @date   2023 - 05 - 22 : YYYY - MM - DD
 *
 */

#ifndef SRC_SOL_H_
#define SRC_SOL_H_

#include "h7uart_bare.h"
#include "rmcp.h"
#include "lwip/udp.h"
#include "emh_malloc.h"

#define SOL_PENDING_ACK_POOL_SIZE   (15UL)
#define SOL_PAYLOAD_METADATA_LENGTH (4UL)
#define SOL_CHARACTER_BUFFER_LENGTH (RMCP_MAX_PLD_BUF_LENGTH - SOL_PAYLOAD_METADATA_LENGTH)

typedef int  (*sol_uart_init_t)(void);
typedef int  (*sol_uart_tx_t)(uint8_t *payload, uint16_t length);

typedef struct
{
  sol_uart_init_t sol_uart_init;
  sol_uart_tx_t   sol_uart_tx;

} sol_callbacks_t;


typedef struct
{
  uint16_t in_pld_size;     // Inbound payload size.
  uint16_t out_pld_size;    // Outbound payload size.
  uint32_t baudrate;        // Baud-Rate for serial communication.
  uint32_t char_acc_interv; // Interval to wait for incoming bytes from the server.
  uint32_t send_threshold;  // Specifies the fifo length used for threshold control.
  uint32_t ms_retry_interv; // Amount of time that SOL waits for an ACK/NACK packet before retrying.
  uint32_t retry_count;
  uint32_t use_fifo;        // Enables the use of FiFo Callbacks.
  uint32_t use_flow_ctl;    // Specifies whether SOL uses software flow control.
}sol_config_t;

typedef struct
{
  uint8_t sequence;
  uint8_t ackd_sequence;
  uint8_t ackd_char_count;
  uint8_t status;
  uint8_t characters[SOL_CHARACTER_BUFFER_LENGTH];
}sol_payload_t;


typedef struct
{
  // Connection Information
  rmcp_ip_net_params_t net_params;
  uint32_t             remote_console_id;
  uint32_t             ipmi_sequence;

  // UDP send functions
  rmcp_udp_send_packet_t sol_udp_send_packet;

  // Stream Buffer Comm. functions
  int (*stream_buf_create)(void);
  unsigned int (*stream_buf_send)(void *data, uint16_t length);
  unsigned int (*stream_buf_recv)(void *bufp, uint16_t length);
  unsigned int (*stream_buf_send_from_isr)(void *data, uint16_t length);
  unsigned int (*stream_buf_recv_from_isr)(void *bufp, uint16_t length);
  unsigned int (*stream_buf_flush_data)(void);
}sol_t;

extern sol_config_t       sol_config;
extern sol_t              sol_instance;
extern uint8_t            usart1_char_input;

extern void sol_get_config(sol_config_t** sol_config);
extern int  sol_deactivate(void);
extern int  sol_activate(rmcp_ip_net_params_t net_params, uint32_t remote_console_id, rmcp_udp_send_packet_t sol_udp_send_packet);
extern int  sol_hw_init(void);
extern void sol_enqueue_payload(uint8_t *recv_rmcp_payload);


/* Functions to interact with Sol implementation */
extern void openipmc_sol_uart_rx(uint8_t* data ,uint32_t len);
extern int  openipmc_sol_impl_init(sol_callbacks_t* callbacks);

#endif /* SRC_SOL_H_ */
