/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_netfn_transport.c
 *
 * @authors Carlos Ruben Dell'Aquila
 *
 * @brief  Response functions for Transport Network Function commands.
 *
 * This file contains the specific functions to manage the requests of Transport Network Function commands (definition in PICMG v3.0 and IPMI v1.5).
 * Each function returns the response data bytes and the completion code.
 *
 * The specific functions are called by ipmi_msg_solve_request_ipmb0().
 */

#include <string.h>
#include <stdint.h>
#include "ipmi_completion_code_defs.h"
#include "ipmi_msg_router.h"
#include "ipmi_msg_manager.h"

/**
 * @{
 * @name Transport NetFn commands
 */

/**
 * @brief Specific function to provide a response for "Set LAN Configuration Parameters" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 *
 */
void ipmi_cmd_set_lan_config_parameters(ipmi_channel_enum_t channel_origin, uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{
  ipmi_channel_enum_t channel;
  uint8_t channel_type_number;
  int ret;
  int req_set_param_len = req_len - 2;
  const uint8_t *param_data = &request_bytes[2];


  *res_len = 0; // command without response data.
  *completion_code = IPMI_COMPL_CODE_OP_NOT_SUPPORTED_FOR_THIS_RECORD_TYPE; // default completion code which can be change after operations.

  // Channel number
  if( 0x0E == (request_bytes[0] & 0x0F))
  {
    // set parameters for channel this request was issued on.
    channel = channel_origin;
  }
  else
  {
    channel = (request_bytes[0] & 0x0F);
  }

  if(!((IPMI_MSG_ROUTER_RET_OK == ipmi_router_get_channel_medium_type_number(channel,&channel_type_number)) &&
       ( 0x04 == channel_type_number )))
  {
    // channel is not present or 802.3 LAN type
    return;
  }

  switch(request_bytes[1])
  { // Parameter selector.

    case 0x03: // IP Address
      ret = lan_param_set_ip_addr(param_data,req_set_param_len);
      break;
    case 0x04: // IP Address Source
      ret = lan_param_set_ip_addr_source(param_data,req_set_param_len);
      break;
    case 0x05: // MAC Address
      ret = lan_param_set_mac_addr(param_data,req_set_param_len);
      break;
    case 0x06: // Subnet Mask
      ret = lan_param_set_subnet_mask(param_data,req_set_param_len);
      break;

    case 0x0C: // Default Gateway Address
      ret = lan_param_set_default_gateway_addr(param_data,req_set_param_len);
      break;

      /* OEM Parameters */
    case 0xC0: // HPM.2
      response_bytes[1] = 0x00;
      response_bytes[2] = 0x00;
      response_bytes[3] = 0x01;
      response_bytes[4] = 0x17;
      response_bytes[5] = 0x00;
      response_bytes[6] = 0x17;
      response_bytes[7] = 0x00;

      *res_len = 8;
      *completion_code = IPMI_COMPL_CODE_SUCCESS;
      break;

    default:
      // parameter not supported
      ret = -1;
      break;
  }

  if(ret)
    *completion_code = IPMI_COMPL_CODE_SUCCESS;

return;
}

/**
 * @brief Specific function to provide a response for "Get LAN Configuration Parameters" command.
 *
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5).
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 *
 */
void ipmi_cmd_get_lan_config_parameters(ipmi_channel_enum_t channel_origin, uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{

  response_bytes[0] = 0x11;

  *res_len = 1;

  if(!(0x80 & request_bytes[0]))
  { // get parameter.

    switch(request_bytes[1]) // Parameter selector
    {
      case 0x00: // Set In Progress
    	  lan_param_get_set_in_progress(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x01: // Authentication Type Support (Read Only)
    	  lan_param_get_auth_type_support(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x02: // Authentication Type Enables
    	  lan_param_get_auth_type_enables(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x03: // IP Address
    	  lan_param_get_ip_addr(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x04: // IP Address Source
    	  lan_param_get_ip_addr_source(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x05: // MAC Address
    	  lan_param_get_mac_addr(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x06: // Subnet Mask
    	  lan_param_get_subnet_mask(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x07: // IPv4 Header Parameters
    	  lan_param_get_ipv4_header_params(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x0A: // BMC-generated ARP control (optional[2])
    	  lan_param_get_bmc_generated_arp_control(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x0B: // Gratuitous ARP interval (optional)
    	  lan_param_get_gratuitous_arp_interval(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x0C: // Default Gateway Address
    	  lan_param_get_default_gateway_addr(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x0D: // Default Gateway MAC Address
    	  lan_param_get_default_gateway_mac_addr(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x0E: // Backup Gateway Address
    	  lan_param_get_bkp_gateway_addr(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x0F: // Backup Gateway MAC Address
    	  lan_param_get_bkp_gateway_addr(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x10: // Community String
    	  lan_param_get_community_string(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x14: // 802.1q VLAN ID (12-bit)
    	  lan_param_get_802_1q_vlan_id(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x15: // 802.1q VLAN Priority
    	  lan_param_get_802_1q_vlan_priority(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x16: // RMCP+ Messaging Cipher Suite Entry Support (Read Only)
    	  lan_param_get_rmcp_plus_msh_cipher_suite_entry_support(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x17: // RMCP+ Messaging Cipher Suite Entries (Read Only)
    	  lan_param_get_rmcp_plus_msh_cipher_suite_entries(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x18: // RMCP+ Messaging Cipher Suite Privilege Levels
    	  lan_param_get_rmcp_plus_msh_cipher_suite_privilege_levels(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;
      case 0x19: // Destination Address VLAN TAGs (can be READ ONLY,see description)
    	  break;
      case 0x1A: // Bad Password Threshold (optional)
    	  lan_param_get_bad_password_threshold(channel_origin,&response_bytes[1],res_len,completion_code);
    	  break;

      /* OEM Parameters */
      case 0xC0: // HPM.2
        response_bytes[1] = 0x00;
        response_bytes[2] = 0x00;
        response_bytes[3] = 0x01;
        response_bytes[4] = 0x17;
        response_bytes[5] = 0x00;
        response_bytes[6] = 0x17;
        response_bytes[7] = 0x00;

        *res_len = 8;
        *completion_code = IPMI_COMPL_CODE_SUCCESS;
        break;

      default:
        // parameter not supported
        *res_len = 0;
        *completion_code = IPMI_COMPL_CODE_OP_NOT_SUPPORTED_FOR_THIS_RECORD_TYPE;
    	  break;
     }
  }

return;
}



