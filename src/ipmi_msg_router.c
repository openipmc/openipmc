/*************************************************************************************/
/*                                                                                   */
/* This Source Code Form is subject to the terms of the Mozilla Public               */
/* License, v. 2.0. If a copy of the MPL was not distributed with this               */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                          */
/*                                                                                   */
/*************************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file openipmc_ipmi_router.c
 *
 * @author Carlos Ruben Dell'Aquila
 *
 * @brief Set of functions to use the IPMI command processors from different interfaces like IPMB-0, RMCP or Payload Interface.
 *
 */

#include "openipmc_hal_config.h"
#include "ipmc_ios.h"
#include "ipmi_msg_router.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "timers.h"

/*
 * Channel associated data including for each one:
 *  - outgoing msg queue.
 *  - outgoing request sequence number.
 *  - incomming request sequence number.
 */
static const char *ipmi_channel_router_names[IPMI_MSG_ROUTER_AMOUNT_CHANNEL] = {IPMI_STRING_NAMES};
static ipmi_router_channel_t ipmi_channel[IPMI_MSG_ROUTER_AMOUNT_CHANNEL];
static const uint8_t ipmi_channel_medium_type_number[IPMI_MSG_ROUTER_AMOUNT_CHANNEL] = {IPMI_CHANNEL_MEDIUM_TYPE_NUMBERS};

/*
 * IPMI incoming message queues.
 *
 * All incoming messages though any channel
 * are put in these queues. They are the interface between
 * the channels and commands processor.
 *
 */
static QueueHandle_t queue_ipmi_req_in = NULL;
static QueueHandle_t queue_ipmi_res_in = NULL;

/*
 * Queues use for bridging IPMI messages between channels.
 */
static QueueHandle_t queue_bridge_ipmi_req = NULL;


 /*
  * IPMI Router Status
  *
  */
static uint8_t ipmi_router_initialized = 0;


 /*
  * Function to do a lazy initialization of the IPMI_MSG_ROUTER
  *
  */
ipmi_router_ret_code_t ipmi_router_init( void )
{
  int i;

  if ( ipmi_router_initialized == 0 )
  {

    // Create queues for request, response and bridge messages
    if(!queue_ipmi_req_in)
      queue_ipmi_req_in = xQueueCreate(OPENIPMC_IPMI_ROUTER_QUEUE_IN_DEPTH, sizeof( ipmi_msg_t) );

    if(!queue_ipmi_res_in)
      queue_ipmi_res_in = xQueueCreate(OPENIPMC_IPMI_ROUTER_QUEUE_IN_DEPTH, sizeof( ipmi_msg_t) );

    if(!queue_bridge_ipmi_req)
      queue_bridge_ipmi_req = xQueueCreate(OPENIPMC_IPMI_ROUTER_BRIDGE_QUEUE_REQ_DEPTH, sizeof( bridge_ipmi_msg_t ) );

    if ( queue_ipmi_req_in == NULL || queue_ipmi_res_in == NULL || queue_bridge_ipmi_req == NULL )
      return IPMI_MSG_ROUTER_RET_ERROR;

    for(i=0;i<IPMI_MSG_ROUTER_AMOUNT_CHANNEL;i++)
    {
      if(!ipmi_channel[i].queue_ipmi_out)
        ipmi_channel[i].queue_ipmi_out  = xQueueCreate(OPENIPMC_IPMI_ROUTER_QUEUE_OUT_DEPTH, sizeof(ipmi_msg_t));

      if(!ipmi_channel[i].queue_ipmi_bridge_res)
        ipmi_channel[i].queue_ipmi_bridge_res = xQueueCreate(OPENIPMC_IPMI_ROUTER_BRIDGE_QUEUE_RES_DEPTH,sizeof(ipmi_msg_t));

      if ( ipmi_channel[i].queue_ipmi_out == NULL || ipmi_channel[i].queue_ipmi_bridge_res == NULL )
        return IPMI_MSG_ROUTER_RET_ERROR;

      ipmi_channel[i].out_req_seq_num = 0xAA;
      ipmi_channel[i].in_req_seq_num  = 0xAA;

      ipmi_channel[i].channel_status  = 0;
    }
    ipmi_router_initialized = 1;
  }
  return IPMI_MSG_ROUTER_RET_OK;
}

/*
 * Function to Enable Channel Output
 *
 */
ipmi_router_ret_code_t ipmi_router_channel_output_enable(uint8_t channel)
{
  if (ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  if( !(channel < IPMI_MSG_ROUTER_AMOUNT_CHANNEL) )
    return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT;

  ipmi_channel[channel].channel_status |= OPENIPMC_IPMI_ROUTER_CHANNEL_OUTPUT_EN;

  return IPMI_MSG_ROUTER_RET_OK;
}


/*
 * Function to Disable Channel Output
 *
 */
ipmi_router_ret_code_t ipmi_router_channel_output_disable(uint8_t channel)
{
  if( !(channel < IPMI_MSG_ROUTER_AMOUNT_CHANNEL) )
    return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT;

  ipmi_channel[channel].channel_status &= ~OPENIPMC_IPMI_ROUTER_CHANNEL_OUTPUT_EN;

  return IPMI_MSG_ROUTER_RET_OK;
}


/*
 * Function to Enable Channel Input
 */
ipmi_router_ret_code_t ipmi_router_channel_input_enable(uint8_t channel)
{
  if (ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
	  return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  if( !(channel < IPMI_MSG_ROUTER_AMOUNT_CHANNEL) )
    return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT;

  ipmi_channel[channel].channel_status |= OPENIPMC_IPMI_ROUTER_CHANNEL_INPUT_EN;

  return IPMI_MSG_ROUTER_RET_OK;
}


/*
 * Function to Disable Channel Input
 *
 */
ipmi_router_ret_code_t ipmi_router_channel_input_disable(uint8_t channel)
{
  if( !(channel < IPMI_MSG_ROUTER_AMOUNT_CHANNEL) )
    return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT;

  ipmi_channel[channel].channel_status &= ~OPENIPMC_IPMI_ROUTER_CHANNEL_INPUT_EN;

  return IPMI_MSG_ROUTER_RET_OK;
}


/*
 * Function to verify if the channel is available
 *
 */
ipmi_router_ret_code_t ipmi_router_channel_is_ready(uint8_t channel)
{
  uint8_t const mask = OPENIPMC_IPMI_ROUTER_CHANNEL_INPUT_EN | OPENIPMC_IPMI_ROUTER_CHANNEL_OUTPUT_EN;

  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  if( !(channel < IPMI_MSG_ROUTER_AMOUNT_CHANNEL) )
    return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT;

  if( !((ipmi_channel[channel].channel_status & mask ) == mask) )
	  return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_READY;

  return IPMI_MSG_ROUTER_RET_OK;
}


/*
 * Function to return the channel name as string
 *
 */
ipmi_router_ret_code_t ipmi_router_channel_name_get(ipmi_msg_t* data, char** pname )
{
  uint8_t const channel = data->channel;

  if( channel < IPMI_MSG_ROUTER_AMOUNT_CHANNEL )
  {
    *pname = (char*) ipmi_channel_router_names[channel];
  }
  else
  {
    return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT;
  }

  return IPMI_MSG_ROUTER_RET_OK;
}


 /*
  * Function to POST new IPMI message data on the req queue.
  *
  * It is used to send a request command to an agent like Shelf-Manager.
  *
  */
ipmi_router_ret_code_t ipmi_router_queue_req_post(ipmi_msg_t* data)
{
  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  xQueueSendToBack(queue_ipmi_req_in,data,0UL);
    return IPMI_MSG_ROUTER_RET_OK;
}


/*
 * Function to GET new IPMI message data from queue req.
 *
 * The data is a request command from any external agent like Shelf-Manager.
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_req_get(ipmi_msg_t* data)
{
  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  xQueueReceive(queue_ipmi_req_in,data,portMAX_DELAY);
    return IPMI_MSG_ROUTER_RET_OK;
}


/*
 * Function to RESET queue response input
 *
 * The function resets the where the incoming responses from external agents are loaded.
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_res_reset( void )
{
  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  xQueueReset(queue_ipmi_res_in);
  return IPMI_MSG_ROUTER_RET_OK;
}


/*
 * Function to POST IPMI message data on queue res
 *
 * The function is used to load an IPMI message response from an external agent.
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_res_post(ipmi_msg_t* data)
{
  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  xQueueSendToBack(queue_ipmi_res_in,data,0UL);
    return IPMI_MSG_ROUTER_RET_OK;
}

/*
 * Function to get data_ipmb data from input response queue
 *
 * An IPMI message request should be replied within IPMI_ROUTER_QUEUE_TIMEOUT period value, according IPMI spec. 1v5/2v0.
 * OpenIPMC sends one request to other agent (like Shelf-Manager) at the time. The response should to arrive from the
 * same channel that request was sent. However, the request and response queues are shared among channels.
 *
 * The function waits for a response, however, a late one from unexpected channel can arrive to the queue. This is detected
 * for the function dividing by IPMI_ROUTER_QUEUE_TIMEOUT_DIV the defined period and ask for the right response within the
 * resulting shorter period.
 *
 * The IPMI_ROUTER_QUEUE_TIMEOUT_DIV is a trade-off between CPU time consumption and how fast the function gets the right response.
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_res_get(ipmi_msg_t* pdata)
{
  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  uint8_t const channel = pdata->channel;
  uint32_t const timestart = ipmc_ios_ms_tick();

  while(( ipmc_ios_ms_tick() - timestart) < IPMI_ROUTER_QUEUE_TIMEOUT_MS )
  {
    if( xQueueReceive(queue_ipmi_res_in,pdata,pdMS_TO_TICKS(IPMI_ROUTER_QUEUE_TIMEOUT_MS/IPMI_ROUTER_QUEUE_TIMEOUT_DIV)) == pdPASS )
    {
      if ( channel == pdata->channel)
        return IPMI_MSG_ROUTER_RET_OK;
    }
  }
  return IPMI_MSG_ROUTER_RET_TIMEOUT;
}


/*
 * Function to POST IPMI message data on channel specific queue out
 *
 * An OpenIPMC request or response is sent to a channel specific output queue.
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_out_post(ipmi_msg_t* data)
{
  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  uint8_t const channel = data->channel;

  if ( channel < IPMI_MSG_ROUTER_AMOUNT_CHANNEL )
  {
    if( xQueueSendToBack(ipmi_channel[channel].queue_ipmi_out,data,pdMS_TO_TICKS(IPMI_ROUTER_QUEUE_TIMEOUT_MS)) == pdFALSE )
      return IPMI_MSG_ROUTER_RET_TIMEOUT;
  }
  else
    return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT;

  return IPMI_MSG_ROUTER_RET_OK;
}

/*
 * Function to GET IPMI message data from channel specific queue out
 *
 * The function lets to get an IPMI message data to be sent by a channel. The specific channel task will wait for this data.
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_out_get(uint8_t channel,ipmi_msg_t* data)
{
  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  if ( channel < IPMI_MSG_ROUTER_AMOUNT_CHANNEL )
  {
    xQueueReceive(ipmi_channel[channel].queue_ipmi_out,data,portMAX_DELAY);
  }
  else
  {
    return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT;
  }
  return IPMI_MSG_ROUTER_RET_OK;
}

/*
 * Function to Set a specific channel's sequence number for outgoing requests
 *
 * It sets the value to the corresponding channel.
 * This number is used when OpenIPMC sends a request to an agent.
 *
 */
ipmi_router_ret_code_t ipmi_router_out_req_seq_number_set(ipmi_msg_t* data, uint8_t seq_number)
{
  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  uint8_t const channel = data->channel;

  if( channel < IPMI_MSG_ROUTER_AMOUNT_CHANNEL)
  {
    ipmi_channel[channel].out_req_seq_num = seq_number;
  }
  else
  {
    return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT;
  }

  return IPMI_MSG_ROUTER_RET_OK;
}

/*
 * Function to Get a specific channel's sequence number for outgoing request
 *
 * It gets the value to the corresponding channel.
 * This number is used when OpenIPMC sends a request to an agent. *
 *
 */
ipmi_router_ret_code_t ipmi_router_out_req_seq_number_get(ipmi_msg_t* data, uint8_t* seq_number)
{

  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  uint8_t const channel = data->channel;

  if( channel < IPMI_MSG_ROUTER_AMOUNT_CHANNEL)
  {
    *seq_number = ipmi_channel[channel].out_req_seq_num;
  }
  else
  {
    return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT;
  }

  return IPMI_MSG_ROUTER_RET_OK;
}

/*
 * Function to Set a specific channel's sequence number for incoming request.
 *
 * Saves the sequence number of the incoming IPMI message request from an external agent.
 *
 *
 */
ipmi_router_ret_code_t ipmi_router_in_req_seq_number_set(ipmi_msg_t* data, uint8_t seq_number)
{
  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  uint8_t const channel = data->channel;

  if( channel < IPMI_MSG_ROUTER_AMOUNT_CHANNEL)
  {
    ipmi_channel[channel].in_req_seq_num = seq_number;
  }
  else
  {
    return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT;
  }

  return IPMI_MSG_ROUTER_RET_OK;
}

/*
 * Function to Get a specific channel's sequence number for incoming request
 *
 * Return a previous IPMI message request sequence number corresponding to a specific channel.
 *
 */
ipmi_router_ret_code_t ipmi_router_in_req_seq_number_get(ipmi_msg_t* data, uint8_t* seq_number)
{
  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  uint8_t const channel = data->channel;

  if( channel < IPMI_MSG_ROUTER_AMOUNT_CHANNEL)
  {
    *seq_number = ipmi_channel[channel].in_req_seq_num;
  }
  else
  {
    return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT;
  }

  return IPMI_MSG_ROUTER_RET_OK;
}


/*
 * Function to POST a request IPMI messages for bridging messages between channels.
 *
 * The 'Send Message' IPMI command uses this function to post the bridging data to a queue.
 *
 * The data then is used for a bridging task forwarding the data to the corresponding channel.
 */
ipmi_router_ret_code_t ipmi_router_queue_bridge_req_post(bridge_ipmi_msg_t* data)
{
  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  // Checks if the destination channel is available.
  if( data->ipmi_msg.channel < IPMI_MSG_ROUTER_AMOUNT_CHANNEL)
  {
    if( xQueueSendToBack(queue_bridge_ipmi_req,data,pdMS_TO_TICKS(IPMI_ROUTER_QUEUE_TIMEOUT_MS)) == pdFALSE )
      return IPMI_MSG_ROUTER_RET_TIMEOUT;
  }
  else
  {
    return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT;
  }

  return IPMI_MSG_ROUTER_RET_OK;
}


/*
 * Function to GET a request IPMI messages for bridging messages between channels.
 *
 * The bridging task will read bridging data and forward it to the corresponding channel.
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_bridge_req_get(bridge_ipmi_msg_t* data)
{

  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  xQueueReceive(queue_bridge_ipmi_req,data,portMAX_DELAY);

  return IPMI_MSG_ROUTER_RET_OK;
}


/*
 * Function to POST a response IPMI messages from bridging messages between channels.
 *
 * In a bridging task after a response arrive the message is added to a queue that works as a mail box specific for each channel.
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_bridge_res_post(ipmi_channel_enum_t channel_origin, ipmi_msg_t* data)
{
  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  if( xQueueSendToBack(ipmi_channel[channel_origin].queue_ipmi_bridge_res,data,pdMS_TO_TICKS(IPMI_ROUTER_QUEUE_TIMEOUT_MS)) == pdFALSE )
    return IPMI_MSG_ROUTER_RET_TIMEOUT;

  return IPMI_MSG_ROUTER_RET_OK;
}


/*
 * Function to GET a response IPMI messages for bridging messages between channels.
 *
 * The function is used by GET MESSAGE IPMI command to get a resulting response from a bridging request.
 * It useful, for example, when an agent ask to Shelf-Manager its address by PAYLOAD INTERFACE of OpenIPMC. OpenIPMC forward the request
 * to Shelf-Manager by IPMB-0. The OpenIPMC gets the response from Shelf-Manager and forwards to payload interface.
 *
 * The response from the last operation is posted into channel specific queue and the command GET MESSAGE read it from that queue.
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_bridge_res_get(uint8_t channel_origin, ipmi_msg_t* data)
{
  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  if( xQueueReceive(ipmi_channel[channel_origin].queue_ipmi_bridge_res,data,pdMS_TO_TICKS(IPMI_ROUTER_QUEUE_TIMEOUT_MS)) == pdFALSE )
    return IPMI_MSG_ROUTER_RET_TIMEOUT;

  return IPMI_MSG_ROUTER_RET_OK;
}


/*
 * Function to CHECK if there is a IPMI response message waiting to be sent to other channel.
 *
 * The function is used by MESSAGE FLAG IPMI command to verify there is available a new response resulting from a bridging message.
 *
 */
ipmi_router_ret_code_t ipmi_router_queue_bridge_res_check(uint8_t channel_origin, int* msg_waiting)
{
  if( ipmi_router_init() != IPMI_MSG_ROUTER_RET_OK )
    return IPMI_MSG_ROUTER_RET_NOT_INITIALIZED;

  *msg_waiting = (int) uxQueueMessagesWaiting(ipmi_channel[channel_origin].queue_ipmi_bridge_res);

  return IPMI_MSG_ROUTER_RET_OK;
}


/*
 * Function to GET the Channel Medium Type Numbers
 *
 * The function is used for Get Channel Info IPMI command in order to get the Channel Medium Type Numbers
 *
 */
ipmi_router_ret_code_t ipmi_router_get_channel_medium_type_number(ipmi_channel_enum_t channel, uint8_t *type_number)
{
  if( channel < IPMI_MSG_ROUTER_AMOUNT_CHANNEL)
  {
    *type_number = ipmi_channel_medium_type_number[channel];
    return IPMI_MSG_ROUTER_RET_OK;
  }
  else
  {
    return IPMI_MSG_ROUTER_RET_CHANNEL_NOT_PRESENT;
  }
}





