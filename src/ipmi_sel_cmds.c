/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_sel_cmd.c
 *
 * @author Carlos Ruben Dell'Aquila
 *
 * @brief Interface to send System Event Log (SEL) commands to Manager. In ATCA context the centralized SEL is the Self-Manager.
 *
 */

#include <stdio.h>
#include <string.h>

#include "ipmc_ios.h"
#include "ipmi_msg_manager.h"
#include "ipmi_completion_code_defs.h"
#include "sdr_definitions.h"
#include "ipmi_sel_cmds.h"


#define NET_FN_STORAGE_REQ         (0x0A)

#define IPMI_RESERVE_SEL           (0x42)
#define IPMI_GET_SEL_ENTRY         (0x43)
#define IPMI_ADD_SEL_ENTRY         (0x44)
#define IPMI_PARTIAL_ADD_SEL_ENTRY (0x45)
#define IPMI_DEL_SEL_ENTRY         (0x46)
#define IPMI_CLEAR_SEL_ENTRY       (0x47)
#define IPMI_GET_SEL_TIME          (0x48)
#define IPMI_SET_SEL_TIME          (0x49)


/*
* Function to reserve the SEL before sending other commands.
*/
int ipmi_sel_reserve_sel(ipmi_sel_reserve_id_t* ipmi_sel_reserve_id)
{
  // REQUEST BYTES
  uint8_t request_bytes[1] = {0};
  int req_len = 0;

  // RESPONSE BYTES
  uint8_t completion_code;
  uint8_t response_bytes[24];
  int res_len;

  int const transaction_outcome = ipmi_msg_send_request (IPMI_ROUTER_CHANNEL_IPMB_0,
		  NET_FN_STORAGE_REQ,IPMI_RESERVE_SEL, request_bytes, req_len, &completion_code,
		  response_bytes, &res_len);

  switch (transaction_outcome)
  {
    case IPMI_MSG_SEND_OK:
      break;
    case IPMI_MSG_SEND_TIMEOUT:
      return IPMI_SEL_RETCODE_MSG_TIMEOUT;
      break;
    case IPMI_MSG_SEND_ERROR:
    default:
      return IPMI_SEL_RETCODE_MSG_ERROR;
      break;
  }

  if (completion_code != IPMI_COMPL_CODE_SUCCESS)
    return IPMI_SEL_RETCODE_CCODE_ERROR;


  if (res_len == 2)
  {
    *ipmi_sel_reserve_id = 0;
    *ipmi_sel_reserve_id |= ((uint16_t) response_bytes[1]) << 8;
    *ipmi_sel_reserve_id |= ((uint16_t) response_bytes[0]);
  }
  else
  {
    return IPMI_MSG_SEND_ERROR;
  }

  return IPMI_MSG_SEND_OK;
}

/*
* Function to get the SEL entry 
*/
int ipmi_sel_get_entry(ipmi_sel_entry_t* ipmi_sel_entry, ipmi_sel_record_id_t ipmi_sel_record_id)
{
  // REQUEST BYTES
  uint8_t request_bytes[6];
  int req_len = 6;

  // RESPONSE BYTES
  uint8_t completion_code;
  uint8_t response_bytes[24];
  int res_len;

  // Reservation ID
  request_bytes[0] = 0x00;
  request_bytes[1] = 0x00;

  // SEL Record ID
  request_bytes[2] = (uint8_t) ((0x00FF & ipmi_sel_record_id)     );
  request_bytes[3] = (uint8_t) ((0xFF00 & ipmi_sel_record_id) >> 8);

  request_bytes[4] = 0x00; // offset in the record
  request_bytes[5] = 0xFF; // read entire record.

  int const transaction_outcome = ipmi_msg_send_request (IPMI_ROUTER_CHANNEL_IPMB_0,
          NET_FN_STORAGE_REQ, IPMI_GET_SEL_ENTRY,request_bytes, req_len, &completion_code,
          response_bytes, &res_len);

  switch (transaction_outcome)
  {
    case IPMI_MSG_SEND_OK:
      break;
    case IPMI_MSG_SEND_TIMEOUT:
      return IPMI_SEL_RETCODE_MSG_TIMEOUT;
      break;
    case IPMI_MSG_SEND_ERROR:
    default:
      return IPMI_SEL_RETCODE_MSG_ERROR;
      break;
  }

  if (completion_code != IPMI_COMPL_CODE_SUCCESS)
    return IPMI_SEL_RETCODE_CCODE_ERROR;

  if (res_len == 18)
  {
    memcpy((void*) ipmi_sel_entry, &response_bytes[3], 16);
  }
  else
  {
    return IPMI_MSG_SEND_ERROR;
  }

  return IPMI_MSG_SEND_OK;
}


/*
 * Function to add entry to SEL
 */
int ipmi_sel_add_entry(ipmi_sel_entry_t* ipmi_sel_entry)
{
  // REQUEST BYTES
  uint8_t* request_bytes = (uint8_t*) ipmi_sel_entry;

  int const req_len = 16;

  // RESPONSE BYTES
  uint8_t completion_code;
  uint8_t response_bytes[24];
  int res_len;

  // Reservation ID
  request_bytes[0] = 0x00;

  int const transaction_outcome = ipmi_msg_send_request(IPMI_ROUTER_CHANNEL_IPMB_0,
          NET_FN_STORAGE_REQ,IPMI_ADD_SEL_ENTRY, request_bytes, req_len, &completion_code,
		  response_bytes, &res_len);

  switch (transaction_outcome)
  {
    case IPMI_MSG_SEND_OK:
      break;
    case IPMI_MSG_SEND_TIMEOUT:
      return IPMI_SEL_RETCODE_MSG_TIMEOUT;
      break;
    case IPMI_MSG_SEND_ERROR:
    default:
      return IPMI_SEL_RETCODE_MSG_ERROR;
      break;
  }

  if (completion_code != IPMI_COMPL_CODE_SUCCESS)
    return IPMI_SEL_RETCODE_CCODE_ERROR;

  if (res_len == 0x02)
  {
    ipmi_sel_entry->record_id_lsb = response_bytes[0];
    ipmi_sel_entry->record_id_msb = response_bytes[1];
  }
  else
  {
    return IPMI_MSG_SEND_ERROR;
  }

  return IPMI_MSG_SEND_OK;
}


/*
 * Function to partial add entry
 */
int ipmi_sel_partial_add_entry(ipmi_sel_entry_t* ipmi_sel_entry)
{
  // REQUEST BYTES
  uint8_t request_bytes[23];
  int req_len = 23;

  // RESPONSE BYTES
  uint8_t completion_code;
  uint8_t response_bytes[24];
  int res_len;

  // Reservation ID
  request_bytes[0] = 0x00;
  request_bytes[1] = 0x00;

  // Record ID
  request_bytes[2] = 0x00;
  request_bytes[3] = 0x00;

  request_bytes[4] = 0x00; // Offset in the record.

  request_bytes[5] = 0x01; // bit [3:0] 0x00 partial add in progress, 0x01 last record data being transferred with this request.

  // SEL record data
  memcpy((void *) &request_bytes[6], (void *) ipmi_sel_entry, 16);

  int const transaction_outcome = ipmi_msg_send_request (IPMI_ROUTER_CHANNEL_IPMB_0,
          NET_FN_STORAGE_REQ, IPMI_GET_SEL_ENTRY,request_bytes, req_len, &completion_code,
          response_bytes, &res_len);

  switch (transaction_outcome)
  {
    case IPMI_MSG_SEND_OK:
      break;
    case IPMI_MSG_SEND_TIMEOUT:
      return IPMI_SEL_RETCODE_MSG_TIMEOUT;
      break;
    case IPMI_MSG_SEND_ERROR:
    default:
      return IPMI_SEL_RETCODE_MSG_ERROR;
      break;
  }

  if (completion_code != IPMI_COMPL_CODE_SUCCESS)
    return IPMI_SEL_RETCODE_CCODE_ERROR;

  if (res_len == 0x02)
  {
    ipmi_sel_entry->record_id_lsb = response_bytes[0];
    ipmi_sel_entry->record_id_msb = response_bytes[1];
  }
  else
  {
    return IPMI_MSG_SEND_ERROR;
  }

  return IPMI_MSG_SEND_OK;
}


