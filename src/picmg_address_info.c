/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file picmg_address_info.c
 *
 * @authors Carlos Ruben Dell'Aquila
 *
 * @brief  Functions to get FRU and Shelf-Manager (ShM) address from ShM.
 *
 * This file contains the specific functions to manage the requests to ShM by using ipmi_msh_send_request_ipmb0 function.
 *
 */

#include <stdio.h>
#include "ipmc_ios.h"
#include "ipmi_msg_manager.h"
#include "picmg_address_info.h"

/*		DEFINITIONS		*/
#define NET_FN_PICMG_REQ		0x2C
#define PICMG_GET_ADDRESS_INFO	0x01
#define PICMG_GET_SHELF_ADDRESS 0x02

/*
 * Get all FRU's address information. It is:
 * 					- Hardware Address
 * 					- IPMB-O Address
 * 					- FRU Device ID
 * 					- Site Number.
 * 					- Site Type.
 */

int picmg_get_address_info(picmg_address_info_data_t *data) {

	// REQUEST BYTES from "Get Address Info" command (on IPM CONTROLLER)
	uint8_t request_bytes[5] = { 0x00, 0x00, 0x00, 0x00, 0x00 };
	request_bytes[3] = ipmc_ios_read_haddress();
	const int req_len = 5;

	// RESPONSE BYTES from "Get Address Info" command (on IPM CONTROLLER)
	uint8_t completion_code;
	uint8_t response_bytes[24];
	int res_len;

	int const transaction_outcome = ipmi_msg_send_request(IPMI_ROUTER_CHANNEL_IPMB_0,
			NET_FN_PICMG_REQ,PICMG_GET_ADDRESS_INFO, request_bytes, req_len, &completion_code,
			response_bytes, &res_len);

	switch (transaction_outcome) {
	case IPMI_MSG_SEND_TIMEOUT:
		return PICMG_ADDRESS_INFO_ERR_IPMI_TIMEOUT;
		break;
	case IPMI_MSG_SEND_ERROR:
		return PICMG_ADDRESS_INFO_ERR_IPMI_ERROR;
		break;
	case IPMI_MSG_SEND_OK:
	default:
		break;
	}

	if (completion_code != 0x00) {
		return PICMG_ADDRESS_INFO_ERR_IPMI_ERROR;
	}

	if (res_len == 7) {

		data->hardware_address = response_bytes[1];
		data->ipmb_0_address = response_bytes[2];
		data->fru_device_id = response_bytes[4];
		data->site_number = response_bytes[5];
		data->site_type = response_bytes[6];

	} else {
		return PICMG_ADDRESS_INFO_ERR_IPMI_ERROR;
	}

	return PICMG_ADDRESS_INFO_ERR_OK;
}

/*
 * Get Shelf address from that. It is received as stream characters. 
 */
int picmg_get_shelf_address_info(picmg_shelf_address_info_data_t *data) {

	int i;

	// REQUEST BYTES from "Get Address Info" command (on IPM CONTROLLER)
	uint8_t request_bytes = 0x00;
	const int req_len = 1;

	// RESPONSE BYTES from "Get Address Info" command (on IPM CONTROLLER)
	uint8_t completion_code;
	uint8_t response_bytes[24];
	int res_len;

	int const transaction_outcome = ipmi_msg_send_request(IPMI_ROUTER_CHANNEL_IPMB_0,
	        NET_FN_PICMG_REQ,PICMG_GET_SHELF_ADDRESS, &request_bytes, req_len, &completion_code,
			response_bytes, &res_len);

	switch (transaction_outcome) {
	case IPMI_MSG_SEND_TIMEOUT:
		return PICMG_GET_SHELF_ADDRESS_ERR_IPMI_TIMEOUT;
		break;
	case IPMI_MSG_SEND_ERROR:
		return PICMG_GET_SHELF_ADDRESS_ERR_IPMI_ERROR;
		break;
	case IPMI_MSG_SEND_OK:
	default:
		break;
	}

	if (completion_code != 0x00) {
		return PICMG_ADDRESS_INFO_ERR_IPMI_ERROR;
	}

	if (res_len >= 2) {

		data->type_code = response_bytes[1] >> 6;
		data->len = response_bytes[1] & 0x1F;

		for (i=0;i<(data->len);i++){
			data->data[i] = response_bytes[2+i];
		}

	} else {
		return PICMG_ADDRESS_INFO_ERR_IPMI_ERROR;
	}

	return PICMG_GET_SHELF_ADDRESS_ERR_OK;

}
