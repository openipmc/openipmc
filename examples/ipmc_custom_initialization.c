/******************************************************************************

MIT License

Copyright (c) 2019 Bruno Casu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

******************************************************************************/



/*
 * ipmc_custom_initialization.c
 *
 *  Created on: 2 Oct 2019
 *      Author: brunocasu
 *              cascadan
 *              calligar
 */

/*
 * This file contains examples of how to initialize OpenIPMC with:
 *   - Board Info
 *   - Sensor creation
 *   - Power Levels
 */


// Uncomment this to run this example code
//#define IPMC_CUSTOM_INITIALIZATION_EXAMPLE


#ifdef IPMC_CUSTOM_INITIALIZATION_EXAMPLE

#include <stdint.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"

//OpenIPMC includes
#include "../src/sdr_manager.h"
#include "../src/power_manager.h"
#include "../src/fru_inventory_manager.h"
#include "../src/sdr_definitions.h"
#include "../src/ipmc_ios.h"
#include "../src/device_id.h"
#include "../src/sensors_templates.h"

void power_initialization(void);


/*
 * Sensor reading functions
 *
 * These functions tell OpenIPMC how to get the sensor reading.
 */
sensor_reading_status_t sensor_reading_fpga_temp(sensor_reading_t* sensor_reading);
sensor_reading_status_t sensor_reading_air_temp(sensor_reading_t* sensor_reading);
sensor_reading_status_t sensor_reading_vcc_out(sensor_reading_t* sensor_reading);

/*
 * Sensor action functions
 *
 * These functions defines actions to take according sensor events
 */
void sensor_action_fpga_temp(ipmi_sel_entry_t* ipmi_sel_entry);

/*
 * This function is called by OpenIPMC during initialization process. The
 * following initializations are expected to be done by this function:
 *   - Initial power scheme
 *   - FRU Inventory
 *   - Sensor Records (if any)
 */
void ipmc_custom_initialization()
{

	/* 
	 * Power Initialization.
	 * 
	 * To simplify the example, the Power Initialization was wrapped in a function
	 * defined later in this file.
	 */
	power_initialization();

	/*
	 *  Create FRU Inventory
	 */

	// Create the Board Info Area
	fru_inventory_field_t board_info;
	board_info = create_board_info_field ( 13017600,            // Manufacturing Date/Time (in minutes from January 1th 1996)
	                                       "SPRACE-KIT",        // Board Manufacturer       (string fields: max of 63 ASCII char)
	                                       "OpenIPMC-HW",       // Board Product Name
	                                       "000000-0001",       // Serial Number
	                                       "XXXXXXX1",          // Part Number
	                                       "file.xml"    );     // FRU File ID
	// Create the Product Info Area
	fru_inventory_field_t product_info;
	product_info = create_product_info_field ( "SPRACE-KIT",        // Manufacturer Name       (string fields: max of 63 ASCII char)
	                                           "OpenIPMC-HW",       // Product Name 
	                                           "123456AB",          // Product Part/Model Number
	                                           "2.0",               // Product Version 
	                                           "AB5555-01",         // Product Serial Number 
	                                           "2021-1234",         // Asset Tag 
	                                           "file.xml"      );   // FRU File ID


	// Aggregates FRU Info areas to create the FRU Info inventory
	create_fru_inventory ( NULL,             // Internal Use Field NOT USED
	                       NULL,             // Chassis Info Field NOT USED
	                       board_info,
	                       product_info,
	                       NULL,             // Multi Record Field NOT USED
	                       0             );  // Multi Record Field NOT USED

	// The individual areas now can be freed
	vPortFree( board_info   );
	vPortFree( product_info );


	/*
	 * Define the device identification and capabilities (Device ID)
	 * This information refers to the IPMC itself
	 */
	ipmc_device_id.firmware_major_revision    = 1;                    // Integer 0 ~ 127
	ipmc_device_id.firmware_minor_revision    = 0x23;                 // BCD from 00 to 99 (two digits) Example: Ver. 1.2.3 -> 0x23
	ipmc_device_id.device_revision            = 1;                    // Integer 0 ~ 15
	ipmc_device_id.auxiliar_firmware_rev_info = 0x1a2b3c4d;           // 8 digits, hexadecimal
	ipmc_device_id.device_id_string           = "some-ATCA-board";    // String, 16 characters maximum.
	ipmc_device_id.device_support             = DEVICE_SUPPORT_IPMB_EVENT_GENERATOR  |
	                                            DEVICE_SUPPORT_FRU_INVENTORY         |
	                                            DEVICE_SUPPORT_SENSOR                ;

	/*
	 *  Create Sensors
	 */
	uint8_t threshold_list[6];

	init_sdr_repository();

	create_hot_swap_carrier_sensor ("Hot Swap Carrier");
	create_ipmb0_sensor ("IPMB-0 Sensor");

  // Dummy sensor for FPGA temperature.
	analog_sensor_1_init_t sensor1;

  threshold_list[0] = 0;    // Lower Non Recoverable NOT USED
  threshold_list[1] = 0;    // Lower Critical        NOT USED
  threshold_list[2] = 0;    // Lower Non Critical    NOT USED
  threshold_list[3] = 65;   // Upper Non Critical    65°C
  threshold_list[4] = 75;   // Upper Critical        75°C
  threshold_list[5] = 100;  // Upper Non Recoverable 100°C

  sensor1.sensor_type = TEMPERATURE;
  sensor1.base_unit_type = DEGREES_C;
  sensor1.m = 1;    // y = 1*x + 0  (temperature in °C is identical to it raw value)
  sensor1.b = 0;
  sensor1.b_exp = 0;
  sensor1.r_exp = 0;
  sensor1.threshold_mask_read = UPPER_NON_CRITICAL | UPPER_CRITICAL | UPPER_NON_RECOVERABLE;
  sensor1.threshold_mask_set = UPPER_NON_CRITICAL | UPPER_CRITICAL | UPPER_NON_RECOVERABLE;
  sensor1.threshold_list = threshold_list;
  sensor1.id_string = "FPGA TEMP";
  sensor1.get_sensor_reading_func = &sensor_reading_fpga_temp;
  sensor1.sensor_action_req = &sensor_action_fpga_temp;

  create_generic_analog_sensor_1( &sensor1 );

	// Dummy sensor for Air temperature.
  analog_sensor_1_init_t sensor2;

	threshold_list[0] = 0;    // Lower Non Recoverable  NOT USED
	threshold_list[1] = 0;    // Lower Critical         NOT USED
	threshold_list[2] = 0;    // Lower Non Critical     NOT USED
	threshold_list[3] = 100;  // Upper Non Critical     30°C  (see conversion below)
	threshold_list[4] = 120;  // Upper Critical         40°C
	threshold_list[5] = 0;    // Upper Non Recoverable  NOT USED

  sensor2.sensor_type = TEMPERATURE;
  sensor2.base_unit_type = DEGREES_C;
  sensor2.m = 5;    // y = (0.5*x - 20) = (5*x - 200)*0.1
  sensor2.b = -200;
  sensor2.b_exp = 0;
  sensor2.r_exp = -1;
  sensor2.threshold_mask_read = UPPER_NON_CRITICAL | UPPER_CRITICAL;
  sensor2.threshold_mask_set = UPPER_NON_CRITICAL | UPPER_CRITICAL;
  sensor2.threshold_list = threshold_list;
  sensor2.id_string = "AIR TEMP";
  sensor2.get_sensor_reading_func = &sensor_reading_air_temp;
  sensor2.sensor_action_req = NULL;

  create_generic_analog_sensor_1( &sensor2 );

	// Dummy sensor for Voltage.
  analog_sensor_1_init_t sensor3;

	threshold_list[0] = 0;    // Lower Non Recoverable  NOT USED
	threshold_list[1] = 0;    // Lower Critical         NOT USED
	threshold_list[2] = 0;    // Lower Non Critical     NOT USED
	threshold_list[3] = 0;    // Upper Non Critical     NOT USED
	threshold_list[4] = 0;    // Upper Critical         NOT USED
	threshold_list[5] = 0;    // Upper Non Recoverable  NOT USED

  sensor3.sensor_type = VOLTAGE;
  sensor3.base_unit_type = VOLTS;
  sensor3.m = 1;    // y = 0.1*x = (1*x + 0)*0.1
  sensor3.b = 0;
  sensor3.b_exp = 0;
  sensor3.r_exp = -1;
  sensor3.threshold_mask_read = 0;
  sensor3.threshold_mask_set = 0;
  sensor3.threshold_list = threshold_list;
  sensor3.id_string = "12V_RAIL";
  sensor3.get_sensor_reading_func = &sensor_reading_vcc_out;
  sensor3.sensor_action_req = NULL;

  create_generic_analog_sensor_1( &sensor3 );

}

sensor_reading_status_t sensor_reading_fpga_temp(sensor_reading_t* sensor_reading)
{

	// This sensor uses y = 1*x + 0 for conversion

	uint8_t raw_temp = 53; // 53°C

	// Fill the raw temp field
	sensor_reading->raw_value = raw_temp;

	// Fill the threshold flag field
	sensor_reading->present_state = 0;
	if(raw_temp > 65)
		sensor_reading->present_state |= UPPER_NON_CRITICAL;
	if(raw_temp > 75)
		sensor_reading->present_state |= UPPER_CRITICAL;
	if(raw_temp > 100)
		sensor_reading->present_state |= UPPER_NON_RECOVERABLE;

	return SENSOR_READING_OK;
}

void sensor_action_fpga_temp(ipmi_sel_entry_t* ipmi_sel_entry)
{
  uint8_t event_dir  = ipmi_sel_entry->event_dir_type & SEL_EVENT_RECORDS_DIR_MASK; // to get event dir (assertion or deassertion)
  uint8_t event_code = ipmi_sel_entry->event_data_1   & EVENT_CODE_MASK;            // to get the event code.
  int reading_value = ipmi_sel_entry->event_data_2;
  int threshold = ipmi_sel_entry->event_data_3;

  if ( event_dir == DIR_ASSERTION_EVENT )
  {
    switch(event_code)
    {
      case THRESHOLD_LOWER_NON_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_LOWER_NON_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_LOWER_NON_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_LOWER_NON_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_LOWER_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_LOWER_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_LOWER_NON_RECOVERABLE_GOING_LOW:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_LOWER_NON_RECOVERABLE_GOING_LOW \n\r");
        break;
      case THRESHOLD_LOWER_NON_RECOVERABLE_GOING_HIGH:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_LOWER_NON_RECOVERABLE_GOING_HIGH \n\r");
        break;
      case THRESHOLD_UPPER_NON_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_UPPER_NON_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_UPPER_NON_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_UPPER_NON_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_UPPER_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_UPPER_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_UPPER_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_UPPER_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_UPPER_NON_RECOVERABLE_GOING_LOW:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_UPPER_NON_RECOVERABLE_GOING_LOW \n\r");
        break;
      case THRESHOLD_UPPER_NON_RECOVERABLE_GOING_HIGH:
        /* Action */
        mt_printf("ASSERTION: THRESHOLD_UPPER_NON_RECOVERABLE_GOING_HIGH \n\r");
        break;
      default:
        break;
    }
  }
  else
  {
    switch(event_code)
    {
      case THRESHOLD_LOWER_NON_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_LOWER_NON_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_LOWER_NON_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_LOWER_NON_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_LOWER_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_LOWER_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_LOWER_NON_RECOVERABLE_GOING_LOW:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_LOWER_NON_RECOVERABLE_GOING_LOW \n\r");
        break;
      case THRESHOLD_LOWER_NON_RECOVERABLE_GOING_HIGH:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_LOWER_NON_RECOVERABLE_GOING_HIGH \n\r");
        break;
      case THRESHOLD_UPPER_NON_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_UPPER_NON_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_UPPER_NON_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_UPPER_NON_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_UPPER_CRITICAL_GOING_LOW:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_UPPER_CRITICAL_GOING_LOW \n\r");
        break;
      case THRESHOLD_UPPER_CRITICAL_GOING_HIGH:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_UPPER_CRITICAL_GOING_HIGH \n\r");
        break;
      case THRESHOLD_UPPER_NON_RECOVERABLE_GOING_LOW:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_UPPER_NON_RECOVERABLE_GOING_LOW \n\r");
        break;
      case THRESHOLD_UPPER_NON_RECOVERABLE_GOING_HIGH:
        /* Action */
        mt_printf("DEASSERTION: THRESHOLD_UPPER_NON_RECOVERABLE_GOING_HIGH \n\r");
        break;
      default:
        break;
    }
  }

  mt_printf("Value: %d, Threshold: %d  \n\r",reading_value,threshold);

  return;
}


sensor_reading_status_t sensor_reading_air_temp(sensor_reading_t* sensor_reading)
{

	// This sensor uses y = (0.5*x - 20) for conversion

	uint8_t raw_temp = 97; // 28.5°C

	// Fill the raw temp field
	sensor_reading->raw_value = raw_temp;

	// Fill the threshold flag field
	sensor_reading->present_state = 0;
	if(raw_temp > 100) // 30°C
		sensor_reading->present_state |= UPPER_NON_CRITICAL;
	if(raw_temp > 120) // 40°C
		sensor_reading->present_state |= UPPER_CRITICAL;

	return SENSOR_READING_OK;
}

sensor_reading_status_t sensor_reading_vcc_out(sensor_reading_t* sensor_reading)
{
	
	// This sensor uses y = 0.1*x for conversion
	sensor_reading->raw_value = 124; // 12.4V

	sensor_reading->present_state = 0; // No thresholds supported by this sensor

	return SENSOR_READING_OK;
}

void payload_cold_reset (void)
{
    // user defined for reseting the payload
    ipmc_ios_printf("\nCOLD RESET SUCCESSFUL!\r\n");

}

uint8_t get_fru_control_capabilities (void)
{
    uint8_t const capabilities = 0; // WARM_RESET_SUPPORTED | GRACEFUL_REBOOT_SUPPORTED | DIAGNOSTIC_INTERRUPT_SUPPORTED
    return capabilities;
}


void power_initialization(void)
{

	power_envelope_t pwr_envelope;

	pwr_envelope.num_of_levels =  2; // Max of 20 beyond the 0-th value
	pwr_envelope.multiplier    = 10; // Global multiplier in tenths of Watt

	// REMINDER: PICMG spec **requires** the maximum power envelope values in this array to increase monotonically!
	pwr_envelope.power_draw[ 0] = 0;   // Power Level 0: RESERVED and always means 0 Watt (payload off)
	pwr_envelope.power_draw[ 1] = 20;  // Power Level 1: 20 Watts ( power_draw[1] * multiplier * 0.1W = 20 * 10 * 0.1 W = 20W )
	pwr_envelope.power_draw[ 2] = 100; // Power Level 2: 100 Watts
	//pwr_envelope.power_draw[ 3] = 110;
	//pwr_envelope.power_draw[ 4] = 140;
	//pwr_envelope.power_draw[ 5] = 150;
	//pwr_envelope.power_draw[ 6] = 160;
	//pwr_envelope.power_draw[ 7] = 170;
	//pwr_envelope.power_draw[ 8] = 180;
	//pwr_envelope.power_draw[ 9] = 190;
	//pwr_envelope.power_draw[10] = 210;
	//pwr_envelope.power_draw[11] = 211;
	//pwr_envelope.power_draw[12] = 212;
	//pwr_envelope.power_draw[13] = 213;
	//pwr_envelope.power_draw[14] = 214;
	//pwr_envelope.power_draw[15] = 215;
	//pwr_envelope.power_draw[16] = 216;
	//pwr_envelope.power_draw[17] = 217;
	//pwr_envelope.power_draw[18] = 218;
	//pwr_envelope.power_draw[19] = 219;
	//pwr_envelope.power_draw[20] = 310;

	ipmc_pwr_setup_power_envelope(pwr_envelope);  // Copy the envelope to the power manager

	// Here must be informed the Power Level desired by the payload. It must be a valid index of the power_draw array (1 up to num_of_levels).
	ipmc_pwr_set_desired_power_level(2); // Power Level 2 means 100 Watts, following what is specified in the power_draw array above.
}



void ipmc_pwr_switch_power_level_on_payload(uint8_t new_power_level)
{

	ipmc_ios_printf("Change Power Level from %d to %d\r\n", ipc_pwr_get_current_power_level(), new_power_level);
	/*
	 * Do whatever is needed to set to the requested power level on the payload
	 */

	/*
	 * TODO: improve example with 12V_enable and reading of current power level
	 */

	return;
}

#endif
